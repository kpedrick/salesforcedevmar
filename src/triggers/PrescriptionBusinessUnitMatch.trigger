trigger PrescriptionBusinessUnitMatch on Opportunity (after insert, after update) {
	new PrescriptionBusinessUnitMatch(trigger.old,trigger.new).execute();
}