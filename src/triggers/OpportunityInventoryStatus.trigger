trigger OpportunityInventoryStatus on Opportunity (after update, before delete) {
	
	new OpportunityInventoryStatusTrigger(trigger.old, trigger.new).execute();
	
}