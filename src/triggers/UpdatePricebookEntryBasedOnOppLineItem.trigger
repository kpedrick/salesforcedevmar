trigger UpdatePricebookEntryBasedOnOppLineItem on OpportunityLineItem (after insert) {
 new UpdatePricebookEntryBasedOnOppLineItem(trigger.old,trigger.new).execute();
}