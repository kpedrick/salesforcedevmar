/*******************************************************************************
Name              : LinvioCreateOpportunityContactRoleTrigger
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       09/04/2014           Peter Clifford       Create SFDC Objects to support Linvio Quotes and Invoices https://na5.salesforce.com/a0P7000000Czop0
*******************************************************************************/
trigger LinvioCreateOpportunityContactRoleTrigger on Opportunity (after insert) {
/*
  Set<Id> accountIds = new Set<Id>();
  
  for (Opportunity opportunity :trigger.new) {
    
    accountIds.add(opportunity.AccountId);
  }

  Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id, PersonContactId FROM Account WHERE Id IN :accountIds]);
  
  OpportunityContactRole[] roles = new OpportunityContactRole[] {};

  for (Opportunity opportunity :trigger.new) {
  
    if (accounts.containsKey(opportunity.AccountId)) {
  
      Account account = accounts.get(opportunity.AccountId);
  
      OpportunityContactRole role = new OpportunityContactRole();
      role.ContactId = account.PersonContactId;
      role.OpportunityId = opportunity.Id;
      role.IsPrimary = true;
      role.Role = 'Business User';
      
      roles.add(role);
    }
  }

  if (!roles.isEmpty()) {
    insert roles;
  }
  */

}