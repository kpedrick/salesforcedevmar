/*******************************************************************************
Name              : RFOpportunityShippingUpdateTrigger
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       10/10/2014           Peter Clifford          
*******************************************************************************/
trigger RFOpportunityShippingUpdateTrigger on Opportunity (after update) {

  Id[] opportunityIds = new Id[] {};
  
  for (Opportunity opportunity :trigger.new) {
  
    Opportunity oldOpportunity = trigger.oldMap.get(opportunity.Id);
    
    if ((opportunity.CloseDate != null && opportunity.Status__c == 'Shipped') &&
      ((opportunity.Status__c != oldOpportunity.Status__c) || (opportunity.CloseDate != oldOpportunity.CloseDate))) {
       
      opportunityIds.add(opportunity.Id);
    }
  }
  

  OpportunityLineItem[] items = [SELECT 
                                     Id, 
                                     Shipping_Datetime__c, 
                                     Opportunity.CloseDate,
                                     Opportunity.Status__c,
                                     Product2Id
                                  FROM OpportunityLineItem 
                                  WHERE OpportunityId IN :opportunityIds];
                                  
  Set<Id> oppLineItemsIdsShipped = new set<Id>();

  
  for (OpportunityLineItem item :items) 
  {
	  	if(item.Opportunity.Status__c == 'Shipped')
	  	{
		  	 item.Shipping_Datetime__c = Datetime.newInstance(item.Opportunity.CloseDate, Datetime.now().time());
		     oppLineItemsIdsShipped.add(item.Id);
	  	}

  }
  
  if (!items.isEmpty()) {
    Database.update(items, false);
  }
  
}