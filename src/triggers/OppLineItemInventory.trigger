trigger OppLineItemInventory on OpportunityLineItem (after delete, after insert, after undelete, after update) {
	
	new OppLineItemInventoryTrigger(trigger.old, trigger.new).execute();
}