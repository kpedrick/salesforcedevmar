trigger UniqueIdSynchronizer on Account (after insert, before update) {
	new UniqueIdSynchronizer(trigger.old,trigger.new).execute();
}