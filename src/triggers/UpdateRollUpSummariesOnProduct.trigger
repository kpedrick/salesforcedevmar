trigger UpdateRollUpSummariesOnProduct on Inventory__c (after insert, before update) {
	
if(Trigger.isUpdate){
	for(Inventory__c inv : trigger.new){
		if(inv.Internal_Unlock__c != true){
			//inv.addError('Invenotries cannot be updated. Only internally');
		}else{
			inv.Internal_Unlock__c = false;
		}
	}
}
}