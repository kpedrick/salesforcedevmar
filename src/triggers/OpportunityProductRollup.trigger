trigger OpportunityProductRollup on OpportunityLineItem (after delete, after insert, after undelete, after update) {
	
	new OpportunityProductRollupTrigger(trigger.old, trigger.new).execute();
}