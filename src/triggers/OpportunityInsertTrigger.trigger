/*
Name              : OpportunityInsertTrigger
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by              Reference             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/07/2014          Peter Clifford            https://na5.salesforce.com/a0P7000000CzZM8EAN
*/
trigger OpportunityInsertTrigger on Opportunity (before insert) {

    //OpportunityInsertTriggerController.createOpportunityCases(trigger.new);
    
    Map<Id, RecordType> rt = new Map<Id, RecordType>([SELECT Id, Name 
		                                                    FROM RecordType 
		                                                    WHERE SobjectType = 'Opportunity']);
    
    Map<String, Pricebook2> pricebooks = new Map<String, Pricebook2>();
    
    for (Pricebook2 pb :[SELECT Id, Name FROM Pricebook2]) {
      pricebooks.put(pb.Name, pb);
    }
    
    for (Opportunity opportunity :trigger.new) {
    
      if (rt.containsKey(opportunity.RecordTypeId)) {
    
	      String rtName = rt.get(opportunity.RecordTypeId).Name;
	      
	      if (pricebooks.containsKey(rtName)) {
          opportunity.Pricebook2Id = pricebooks.get(rtName).Id;
	      }
      }
    }
    
    
}