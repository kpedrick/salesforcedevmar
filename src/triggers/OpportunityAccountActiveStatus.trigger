trigger OpportunityAccountActiveStatus on Opportunity (after delete, after insert, after undelete, after update) {
	
	new OpportunityAccountActiveStatusTrigger(trigger.old,trigger.new).execute();

}