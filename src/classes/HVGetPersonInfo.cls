public with sharing class HVGetPersonInfo  extends HVRequest {
	/*  <?xml version="1.0" encoding="utf-8"?>
		<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">
		<auth>
		<hmac-data algName="HMACSHA1">vsPrKLO154VS1jF/5kIMnQk3kBQ=</hmac-data>
		</auth>
		<header>
		<method>GetPersonInfo</method>
		<method-version>1</method-version>
		<auth-session>
		<auth-token>ASAAAKh.....TmufbdVxHmmU8PsVk=
		</auth-token>
		<user-auth-token>ASAAANe...oCZjxOA==
		</user-auth-token>
		</auth-session>
		<language>en</language>
		<country>US</country>
		<msg-time>2010-09-17T10:21:46Z</msg-time>
		<msg-ttl>1800</msg-ttl>
		<version>0.9.1712.2902</version>
		<info-hash>
		<hash-data algName="SHA1">+FS0rMnT//A9dC7u3XviYXiUM24=</hash-data>
		</info-hash>
		</header>
		<info/>
		</wc-request:request>
	*/
	
    public HVGetPersonInfo(String accountId, String token) {

        this.header = new HVRequestHeader('CreateAuthenticatedSessionToken','2', token);
        this.info = new HVRequestInfo();
    }
    
    public override String getRequestBody() {
    
        String result = '';
        result += REQUEST_BEGIN;
        result += header.getBasicHeader();
        result += info.getInfo(getAuthInfo());
        result += REQUEST_END;
        
        return result;
    }
	
    private String getAuthInfo() {
    
        String content = getContent();
    
        String result = '';
        result += '<auth-info>';
        result += '<app-id>' + APP_ID + '</app-id>';
        result += '<credential>';
        result += '<appserver2>';
        
        result += '<hmacSig algName="HMACSHA256">' + getHMACSHA256Encrypt(content) + '</hmacSig>';
        
        result += content;
        
        result += '</appserver2>';
        result += '</credential>';
        result += '</auth-info>';
        return result;
    }
    
    public String getContent() {
    
        String result = '';
        result += '<content>';
        result += '<app-id>' + APP_ID + '</app-id>';
        result += '<hmac>HMACSHA256</hmac>';
        result += '<signing-time>' + HVRequest.getDatetimeFormat() + '</signing-time>';
        result += '</content>';    
        
        System.debug('CONTENT: ' + result);
        return result;
    }
    
    private String getHMACSHA256Encrypt(String body) {
    	
    	String localSharedSecret = Test.isRunningTest() ? '1234567890' : ''; 
    	
    	Blob ss = Encodingutil.base64Decode(localSharedSecret);
        String result = Encodingutil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(body), ss));
        
        return result;
    }

}