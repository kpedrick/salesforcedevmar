/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public virtual with sharing class HVResponse {
	
	protected Dom.Document xml;
	private String body;
	
	public HVResponse(String body) {
        System.debug('BODY: ' + body);
        this.body = body;
        (this.xml = new Dom.Document()).load(body);   
	}
	
	public String getBody() {
		
	   return body;
	}
	
	public String getTagValue(String tag) {
		
	   return getTagValue(xml.getRootElement(), tag);
	}

    protected String getTagValue(Dom.Xmlnode root, String tag) {

        if (tag.equals(root.getName())) {
            return root.getText();
        }
        
        for (Dom.Xmlnode node :root.getChildElements()) {
            String result = getTagValue(node, tag); 
            if (result != null) {
                return result;
            }
        }    	
        
        return null;
    }
    
    public String getAttributeValue(String tag, String attribute) {
        
       return getAttributeValue(xml.getRootElement(), tag, attribute);
    }    
    
    protected String getAttributeValue(Dom.Xmlnode root, String tag, String attribute) {

        if (tag.equals(root.getName())) {
            return root.getAttributeValue(attribute, null);
        }
        
        for (Dom.Xmlnode node :root.getChildElements()) {
            String result = getAttributeValue(node, tag, attribute); 
            if (result != null) {
                return result;
            }
        }       
        
        return null;
    }    
    
    
    
}