public class OpportunityInventoryStatusTrigger extends SObjectTrigger {
	
	public OpportunityInventoryStatusTrigger(sObject[] sObjectOldList, sObject[] sObjectNewList){
		super(sObjectOldList,sObjectNewList);
	}
    
    public override boolean executable(sObject sObjectOld, sObject sObjectNew){
		return true;
	}

	public override void execute(sObject[] sObjectList, Boolean forceUpdate){
		
		Set<Id> opportunityIds = new Set<Id>();
		set<Id> opportunityLineItemIds = new set<Id>();
		Map<Id, Inventory__c> oliIdInventoryMap = new Map<Id, Inventory__c>();
		List<Inventory__c> inventoryUpdateList = new List<Inventory__c>();
		List<Inventory__c> inventoryDeleteList = new List<Inventory__c>();
		
		for (sObject obj :sObjectList){
			opportunityIds.add((Id)obj.get('Id'));
		}
		
		List<Opportunity> opportunityList = [SELECT Id, 
													Status__c,
													(SELECT Id FROM OpportunityLineItems)
											 FROM Opportunity
											 WHERE Id IN :opportunityIds];
											 
		for (Opportunity opp :opportunityList){
			for (OpportunityLineItem oli : opp.OpportunityLineItems){
				opportunityLineItemIds.add(oli.Id);
			}
		}									 
											 
											 
		List<Inventory__c> inventoryList = [SELECT Id,
												   Status__c,
												   Prescription_Line_Item_Id__c
											FROM Inventory__c
											WHERE Prescription_Line_Item_Id__c IN :opportunityLineItemIds];
		
		
		for (Inventory__c inv :inventoryList){
			if (inv.Prescription_Line_Item_Id__c == null)
				continue;
				
			oliIdInventoryMap.put(inv.Prescription_Line_Item_Id__c, inv);
		}
		
		for (Opportunity opp :opportunityList){
			for (OpportunityLineItem oli : opp.OpportunityLineItems){
				
				if (oliIdInventoryMap.containsKey(oli.Id) == false)
					continue;
					
				Inventory__c inv = oliIdInventoryMap.get(oli.Id);
				
				if (inv.Status__c == OpportunityInventoryStatusUtil.getInventoryStatus(opp.Status__c))
					continue;
					
				inv.Status__c = OpportunityInventoryStatusUtil.getInventoryStatus(opp.Status__c);
				inv.Internal_Unlock__c = true;
				inventoryUpdateList.add(inv);	
			}
		}
		
		if (Trigger.isDelete == true){
			inventoryDeleteList.addAll(inventoryList);
		}
		
		if (inventoryUpdateList.isEmpty() == false){
			system.debug('inventoryUpdateList.isEmpty() -> '+ inventoryUpdateList.isEmpty());
			update inventoryUpdateList;
		}
		
		if (inventoryDeleteList.isEmpty() == false){
			delete inventoryDeleteList;
		}
	}
}