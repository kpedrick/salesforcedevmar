public with sharing class PrescriptionBusinessUnitMatch extends TriggerSupport{
	
	public static Boolean executed = false;
	
	public PrescriptionBusinessUnitMatch(sObject[] sObjectOldList, sObject[] sObjectNewList) {
       super(sObjectOldList, sObjectNewList);
    }
    
    public override Boolean executable(sObject sObjectOld, sObject sObjectNew){
    	if(sObjectOld == null) return true;
    	
    	if(executed) return false;
    	
    	if(Schema.Account.getSObjectType() == sObjectNew.getSObjectType()) return false;
    	
    	return getisDifferent(sObjectOld, sObjectNew,Opportunity.Business_Unit__c);
    	
    }
    
    public override void execute(sObject[] sObjectList, Boolean forceUpdate){
    	executed = true;
    	Map<String,Pricebook2> pricebooks = new Map<String,Pricebook2>(
    		[select 
    				Name,
    				(select id,Product2Id,Pricebook2Id from PriceBookEntries )
    		  from PriceBook2
    		  ]);
    	for(Pricebook2 pb : pricebooks.values()){
    		pricebooks.put(pb.Name, pb);
    	}
    	OpportunityLineItem[] itemsToDelete = new OpportunityLineItem[]{};
    	OpportunityLineItem[] itemsToInsert = new OpportunityLineItem[]{};
    	 
    	Opportunity[] opportunities = [select
    									Name,
    									Business_Unit__c,
    									PriceBook2.Name,
    									PriceBook2Id,
    									AccountId,
    									(select id,
    											PricebookEntry.Product2Id,
    											Quantity,
    											Description,
    											Discount,
    											ListPrice,
    											OpportunityId,
    											PricebookEntryId,
    											UnitPrice
    									   from OpportunityLineItems)
    							   from Opportunity
    							   		where id = :sObjectList
    							   		and status__c != 'Shipped'
    								];
    	Account acc = null;
    	
    	for(Opportunity o : opportunities){
    		Id originalPricebookId = o.Pricebook2Id;
    		for(Sobject so : sObjectNewList){
    			if(o.AccountId == so.get('Id')) acc = (Account) so;
    		}
    		PriceBook2 pb = null;
    		system.debug('iterating: ' + o);
    		system.debug('pricebook: ' + o.PriceBook2Id);
    		system.debug('business unit: ' + o.Business_unit__c);
    		
			if(pricebooks.containsKey(o.Business_Unit__c)){
				pb = pricebooks.get(o.Business_Unit__c);
				o.PriceBook2Id = pb.id;
				system.debug('setting pricebook ' + pb.id);
			}
    		
    		system.debug('selected pricebook: ' + pb);
    		if(pb == null) continue;
    		
    		system.debug('line items found: ' + o.OpportunityLineItems.size());
    		system.debug(originalPricebookId + '/' + pb.id);
    		
    		
    		if(o.OpportunityLineItems.size() != 0 && originalPricebookId != pb.id){
    			itemsToDelete.addAll(o.OpportunityLineItems);
    			OpportunityLineItem[] toInsert = o.OpportunityLineItems.deepClone();
    			for(OpportunityLineItem oli : toInsert){
    				Boolean found = false;
    				system.debug('product item : ' + oli);
    				for(PriceBookEntry pbe : pb.priceBookEntries){
    					if(oli.PricebookEntry.Product2Id == pbe.Product2Id){
    						found = true;
    						oli.PricebookEntryId = pbe.id;
    						
    					}
    				}
    				
    				if(found){
    					system.debug('inserting product: ' + oli);
    					itemsToInsert.add(oli);
    				}
    				else if(acc != null){
    					acc.addError('You must delete the related product(s) to Opportunity  <a href="/' + o.id + '">' + o.name + '</a>',false);
    				}
    			}
    		}
    	}
    	
    	system.debug('items to update: ' + itemsToInsert.size());
    	
    	delete itemsToDelete;
    	
    	update opportunities;
    	
    	insert itemsToInsert;
    	
    }
}