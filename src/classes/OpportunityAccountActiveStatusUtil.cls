public class OpportunityAccountActiveStatusUtil {
	
	public Map<Id,Account> accountMap;
	public Map<Id, Opportunity> opportunityMap;
	public List<Account> accountUpdateList;
	public Date dateToday;
	
	public OpportunityAccountActiveStatusUtil(Map<Id,Account> acctMap, Date dateToday){
		this.accountMap = acctMap;
		this.dateToday = dateToday;
	}
	
	
	public List<Account> createUpdateList(){
		
		this.accountUpdateList = new list<Account>();
		
		this.opportunityMap = this.createOpptyMap();
		
		for (Account acct : this.accountMap.values()){
			AccountActiveStatusWrapper acctWrapper = new AccountActiveStatusWrapper(acct, this.opportunityMap, this.dateToday);
			acctWrapper.calculateActiveStatus();
			
			if (acct.Active__c == acctWrapper.activeStatus)
				continue;
			
			
			acct.Active__c = acctWrapper.activeStatus;
			
			this.accountUpdateList.add(acct);
		}
		
		
		return this.accountUpdateList;
	}
	
	public Map<Id, Opportunity> createOpptyMap(){
		
		Set<Id> opportunityIds = new Set<Id>();
		
		for (Account acct : this.accountMap.values()){
			for (Opportunity opp : acct.Opportunities){
				opportunityIds.add(opp.Id);
			}
		}
		
		this.opportunityMap = new Map<Id,Opportunity>( [SELECT Id,
															   Status__c,
															   LastModifiedDate,
															   CreatedDate,
															   CloseDate,
															   (SELECT Id, 
															   		   LastModifiedDate 
															    FROM Feeds 
															    ORDER BY LastModifiedDate DESC 
															    LIMIT 1),		
															   (SELECT Id,
																		Shipping_Datetime__c,
																		Supply_Ending_Date__c
																FROM OpportunityLineItems)
														FROM Opportunity
														WHERE Id = :opportunityIds
														]);
		

		return this.opportunityMap;
		
	}
	
}