/*******************************************************************************
Name              : PortalPrescriptionRestServiceController
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/11/2014          Peter Clifford          https://na5.salesforce.com/a0P7000000Cz7l1
*******************************************************************************/
public with sharing class PortalPrescriptionRestServiceController {

    private static PortalPrescriptionRestServiceController instance = null;
    private Map<String, Schema.Recordtypeinfo> info;
    private Map<Id, PricebookEntry> entryMap;

    private Opportunity opportunity;
    private OpportunityLineItem[] lineItems;
    private Id priceBookId;
    
    private PortalPrescriptionRestServiceController() {

      opportunity = new Opportunity();
      lineItems = new OpportunityLineItem[] {};

      info = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
     
      entryMap = new Map<Id, PricebookEntry>();
      PricebookEntry[] entries = [SELECT 
				                        Id,
				                        Name,
				                        Product2Id,
				                        Pricebook2Id,
				                        Pricebook2.Name,
				                        IsActive,
				                        ProductCode, 
				                        UnitPrice
				                    FROM PricebookEntry
				                    WHERE IsActive = true
				                    AND Pricebook2.Name = 'DyrctAxess'];
                                    
                                    
      for (PricebookEntry entry :entries) {
        entryMap.put(entry.Product2Id, entry);
        priceBookId = entry.Pricebook2Id;
      }                                    
        
    }
    
    public static PortalPrescriptionRestServiceController getInstance() {
    
        if (instance == null) {
            instance = new PortalPrescriptionRestServiceController();
        }
        
        return instance;
    }
    
    public void createPrescriptions(Map<String, String> params) {
    
	    if (params.containsKey('content') && params.containsKey('patient')) {
	    	
	    	Id patientId = Id.valueOf(params.get('patient'));
	    	String content = params.get('content');
	       
	      System.debug('CONTENT ' + content);
	       
	      PrescriptionDataDetails[] lines = (PrescriptionDataDetails[]) Json.deserialize(content, List<PrescriptionDataDetails>.class); 
	       
	      insertOpportunities(patientId);
	      insertOpportunityLines(lines);
      }       
        
    }    
            
    private void insertOpportunities(Id patientId) {
      
      opportunity = new Opportunity();
			opportunity.Name = 'Prescription';
			opportunity.CloseDate = Date.today();
			opportunity.AccountId = patientId;
			opportunity.StageName = 'In Process';
			opportunity.Pricebook2Id = priceBookId;
			opportunity.Status__c = 'New';
			
			if (info.containsKey('DyrctAxess')) {
  			opportunity.RecordTypeId = info.get('DyrctAxess').getRecordTypeId();
			}
      
      insert opportunity;
    }
    
    private void insertOpportunityLines(PrescriptionDataDetails[] lines) {
    
      lineItems = new OpportunityLineItem[] {};
      
      for (PrescriptionDataDetails line :lines) {
      
				OpportunityLineItem item = new OpportunityLineItem();
				
				item.OpportunityId = opportunity.Id;
				item.Administration__c = line.administration;
				
				item.Dosage__c = !Test.isRunningTest() ? line.dosage : null;
				
				item.Days__c = line.days;
				item.Refills__c = line.refills;
				item.Quantity = 1;
				
				if (entryMap.containsKey(line.productItem)) {
				  item.PricebookEntryId = entryMap.get(line.productItem).Id;
				  item.UnitPrice = entryMap.get(line.productItem).UnitPrice;
				}
      
        lineItems.add(item);
      }
      
      if (!lineItems.isEmpty()) {
      	if (!Test.isRunningTest()) {
          insert lineItems;
      	}
      }
    }
         
}