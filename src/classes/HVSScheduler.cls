/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
global class HVSScheduler implements System.Schedulable {

    global void execute (System.Schedulablecontext sc) {
    
        Database.executeBatch(new HVSPatientBatch(), 1);
    }
}