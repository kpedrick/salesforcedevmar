/*
Name              : OpportunityAddProductsController
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by              Reference             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/07/2014          Peter Clifford            https://na5.salesforce.com/a0P7000000CzZM8EAN
*/
public with sharing class OpportunityAddProductsController {

  private Apexpages.Standardcontroller std;
  private Opportunity opportunity;
  
  public OpportunityLineItem[] items {get; set;}
  public Map<Id, Selectoption[]> productDosages {get; set;}
  public Map<Id, PricebookEntry> entries {get; set;}

  public String searchBox {get; set;}  
  public String itemId {get; set;}
  public String entryId {get; set;}

  public OpportunityAddProductsController(Apexpages.Standardcontroller std) {
  
    this.std = std;
    
    refresh();

  }

  public Pagereference searchProducts() {
  
    PricebookEntry[] priceEntries = new PricebookEntry[] {};
    entries = new Map<Id,PricebookEntry>();
    
    priceEntries = (searchBox != null) ? getPricebookEntries(searchBox) : getPricebookEntries();
    
    Set<Id> entryIds = new Set<Id>();
    
    for (OpportunityLineItem item :items) {
      entryIds.add(item.PricebookEntryId);
    }    
    
      for (PricebookEntry priceEntry :priceEntries) {
      	
	      if (!entryIds.contains(priceEntry.Id)) {
	        entries.put(priceEntry.Id, priceEntry);
	      }
      }
      
      return null;
  }

  public Pagereference saveItems() {
    
    if (validate()) {
      update items;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Save Success'));
      refresh();    
    }
    
    return null;
  }
  
  public Pagereference addItem() {
  
    OpportunityLineItem elem = null;
    
    if (entryId != null) {
    
      elem = new OpportunityLineItem();
      elem.OpportunityId = std.getId();
      elem.Quantity = 1;
      elem.PricebookEntryId = entryId;
      
      if (entries.containsKey(Id.valueOf(entryId))) {
      
        elem.UnitPrice = entries.get(Id.valueOf(entryId)).UnitPrice;
      
        insert elem;     
      }
    } 
    
    refresh();
    return null;
  }

  public Pagereference removeItem() {
  
    OpportunityLineItem elem = null;
  
    for (OpportunityLineItem item :items) {
    
      if (item.Id == Id.valueOf(itemId)) {
        elem = item;
      }
    }
  
    if (elem != null) {
      delete elem;
    }
  
    refresh();
    return null;
  }

  private void refresh() {
  
    searchBox = null;
    loadOpportunity();
    loadItems();
    loadProducts();
  }


  private void loadItems() {
  
    items = new OpportunityLineItem[] {};
    
    items = [SELECT 
                    Id, 
                    Administration__c, 
                    Days__c, 
                    Dosage__c,
                    Is_Opportunity_Prescription__c, 
                    Opportunity_Date__c, 
                    Patient_Name__c, 
                    Product_Name__c,  
                    Product2Id,
                    Refills__c,
                    UnitPrice,
                    PricebookEntryId
                FROM OpportunityLineItem
                WHERE OpportunityId = :std.getId()];
  }
  
  private void loadProducts() {
    
    productDosages = new Map<Id, Selectoption[]>();
    entries = new Map<Id,PricebookEntry>();
    
    Set<Id> entryIds = new Set<Id>();
    
    for (OpportunityLineItem item :items) {
      entryIds.add(item.PricebookEntryId);
    }
  
    PricebookEntry[] priceEntries = getPricebookEntries();
    
    for (PricebookEntry priceEntry :priceEntries) {
    
      if (!entryIds.contains(priceEntry.Id)) {
        entries.put(priceEntry.Id, priceEntry);
      }
    }
    
    
    Set<Id> productIds = new Set<Id>();
    
    for (PricebookEntry entry :priceEntries) {
      productIds.add(entry.Product2Id);
    }
    
  
    Product2[] products = [SELECT 
                              ProductCode, 
                              Name, 
                              Description,
                              Id, 
                              
                                  (SELECT
                                         Name, 
                                         Product__c, 
                                         Value__c, 
                                         Unit__c 
                                     FROM Dosages__r
                                     ORDER BY Value__c)
                                      
                             FROM Product2
                           WHERE Id IN :productIds];
                                           
    for (Product2 product :products) {
      
      productDosages.put(product.Id, new Selectoption[] {});
      
      productDosages.get(product.Id).add(new Selectoption('', '--None--'));
      
      for (Dosage__c dosage :product.Dosages__r) {
        
        Decimal dValue = dosage.Value__c.stripTrailingZeros();
        
        String value = dValue.toPlainString() + ' ' + dosage.Unit__c; 
        productDosages.get(product.Id).add(new Selectoption(dosage.Id, value));
      }
      
    }                                          
                                           
  }

  private void loadOpportunity() {
  
    opportunity = [SELECT Id, 
                          Name, 
                          Pricebook2Id 
                      FROM Opportunity 
                      WHERE Id = :std.getId()];
  }


  private PricebookEntry[] getPricebookEntries() {
  
    return getPricebookEntries(null);
  }
  
  private PricebookEntry[] getPricebookEntries(String searchTerm) {
  
    PricebookEntry[] result = new PricebookEntry[] {}; 
  
    PricebookEntry[] priceEntries = [SELECT 
                                          UseStandardPrice, 
                                          UnitPrice, 
                                          ProductCode, 
                                          Product2.IsActive, 
                                          Product2.Description, 
                                          Product2.ProductCode, 
                                          Product2.Name, 
                                          Product2Id
                                      FROM PricebookEntry
                                      WHERE Pricebook2Id = :opportunity.Pricebook2Id
                                      AND Product2.IsActive = true];    
    
    for (PricebookEntry priceEntry :priceEntries) {
    
      String productCode = priceEntry.Product2.ProductCode != null ? priceEntry.Product2.ProductCode.toUpperCase() : '';
      String productName = priceEntry.Product2.Name != null ? priceEntry.Product2.Name.toUpperCase() : '';
      String description = priceEntry.Product2.Description != null ? priceEntry.Product2.Description.toUpperCase() : '';
      
      if (searchTerm == null) {
        result.add(priceEntry);
      }
      else { 
      
        searchTerm = searchTerm.toUpperCase();
        
	      if (productCode.contains(searchTerm) || productName.contains(searchTerm) || description.contains(searchTerm)) {  
	        result.add(priceEntry);
	      }
      }
    }
   
    return result;
  }  
  
  private Boolean validate() {
  
    Boolean result = true;
  
    for (OpportunityLineItem item :items) {
    
      System.debug(item.Dosage__c);
    
      if (item.Dosage__c == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Dosage must have a value'));
        result = false;
      }
      else if (item.Administration__c == null) {
        item.Administration__c.addError('You must enter a value');
        result = false;
      }
    
    }
  
    return result;
  }
  
}