/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@isTest
public class HVTest {

    
    private static void createDyrctAxessCustomSettings() {
    
        DyrctAxess_Settings__c settings = new DyrctAxess_Settings__c();
        
        settings.Interval__c = 2;
        settings.Health_Vault_Endpoint__c = 'http://hv.com';
        settings.Portal_Base_URL__c = 'http://portal.com';
        settings.Application_Id__c = '1234567890';
        
        insert settings; 
        
        Health_Vault_Types__c[] hvt = new Health_Vault_Types__c[] {};
        hvt.add(new Health_Vault_Types__c(Value__c = 'ca3c57f4-f4c1-4e15-be67-0a3caf5414ed', Name = 'Blood Pressure Measurement'));
        
        insert hvt;  
    }

    static testMethod void createApplicationTest() {
    	
    	createDyrctAxessCustomSettings();

	    HVNewApplicationCreationInfo newApplicationCreationInfo = new HVNewApplicationCreationInfo();
	    HVResponse hvr = HVConnection.sendMethodRequest(newApplicationCreationInfo.getRequestBody());
	    
	    if (hvr != null) {
	        
	        String appToken = hvr.getTagValue('app-token');
	        String appId = hvr.getTagValue('app-id');
	        String sharedSecret = hvr.getTagValue('shared-secret');
	    
	        HVCreateConnectRequest createConnectRequest = new HVCreateConnectRequest(); 
	        String url = createConnectRequest.getShellRedirectURL(appToken);
	        System.debug('URI ' + url);
	        System.debug('APP TOKEN ' + appToken);
	        System.debug('APP ID ' + appId);
	        System.debug('SHARED SECRET ' + sharedSecret);
	        
	    }    
    }

    static testMethod void createAuthenticationSessionTest() {
    
        createDyrctAxessCustomSettings();
    
        String privateKey = 'BoXC8X32UnIsUAs+wd/R8ZRiWDnSK1scwjfCHuqhFq8=';
        String appId = 'd31c3f38-d3a3-4c7d-9d9a-92b539300e39';        
        
        HVCreateAuthenticatedSessionToken createAuthenticatedSessionToken = new HVCreateAuthenticatedSessionToken(appId, privateKey);
        HVConnection.sendMethodRequest(createAuthenticatedSessionToken.getRequestBody());    
    }

    static testMethod void getAuthorizedPeopleTest() {
    
        createDyrctAxessCustomSettings();
        
        String appId = '68bcc708-4c78-4b13-8e74-4c33591d1f73';
        String sharedSecret = 'cCLhXh9S26U0cuX3BGXQh0Oj5DKkICaXtMxiYahsISgNZpeXYZaif9nmbBULTIOwDiJ7HWRchlCjVTInGcN11w==';
        String token = 'AiAAAKsyGGR8oj5Mhusq+tJbMYZdGVz9hpy8mPnpHKYjA0zQzh2OhoGHhdXxlymbXyTiVMAAAACFmJK6K7N4o4Ss7tTphJMJlkozO4Xi1bmfIeDxmRrV/46RWmgjtLsHDbGz3OpvdHEECRz37+jtKR5jhVgZoXyzAnEx7vM0MCW9+IIeit3uBFQYR7PEUc2/grRa4mrhPKLB954lMENtJxeA5123M64PXQLWJKfW3tahjDVStHFHLVUL3K97u6DIGb0WHP9ndO4pVkHhFpjWB7rHFPfsK52o6IsD+l2qY0OodjlwYIlG36CNjapxINM5T3Ri17r6X7QgAAAADwFq6FEqa+YRJLD1nSXmnAS4bumai8DkM2IL0jDbp5ggAAAADwFq6FEqa+YRJLD1nSXmnAS4bumai8DkM2IL0jDbp5g=';
        HVGetAuthorizedPeople getAuthorizedPeople = new HVGetAuthorizedPeople(appId, sharedSecret, Token);
        HVResponse hvr = HVConnection.sendMethodRequest(getAuthorizedPeople.getRequestBody());    
    }

    static testMethod void connectionWebServiceTest() {
    
        createDyrctAxessCustomSettings();
        Account account = TestUtils.createAccount('patient');
        
        String url = HVSConnectionWebService.execute(account.Id);    
    }
    
    static testMethod void sessionWebServiceTest() {
    
        createDyrctAxessCustomSettings();
        Account account = TestUtils.createAccount('patient');
        
        //String isSuccess = HVSSessionWebService.execute(account.Id);    
    }
    
    static testMethod void synchronizeWebServiceTest() {

        createDyrctAxessCustomSettings();
        Account account = TestUtils.createAccount('patient');
        Health_Vault_Credential__c credential = TestUtils.createHealthVaultCredentials(account.Id);    
    
        Test.startTest();
    
        HVSDyrctAxessController c = new HVSDyrctAxessController();
        c.synchronize(HVSPatientBatchController.getInstance().getPatientById(account.Id));
        
        Test.stopTest();    
    }

    static testMethod void hvResponseTest() {

        

    }

    static testMethod void hvRequestTest() {

        

    }


}