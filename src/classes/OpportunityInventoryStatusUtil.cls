public class OpportunityInventoryStatusUtil {

	public static String getInventoryStatus(String oppStatus){
		String newStatus = 'Pending';
		
		if (oppStatus == 'Shipped'){
			newStatus = 'Complete';
		}else if(oppStatus == 'Canceled-Call Center' || oppStatus == 'Canceled-Pharmacy'){
			newStatus = 'Cancelled';
		}
		
		return newStatus;
	}
}