/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class PortalPasswordRestServiceController {

    private static PortalPasswordRestServiceController instance = null;

    private PortalPasswordRestServiceController() {}
    
    public static PortalPasswordRestServiceController getInstance() {
    
        if (instance == null) {
            instance = new PortalPasswordRestServiceController();
        }
        
        return instance;
    }


    public String changePhysicianPassword(String npi) {
    
        String password = getRandomPassword();
        String result = null;
    
        Account[] accounts = [SELECT Id, Name, 
                                    (SELECT 
                                        Id, 
                                        Name, 
                                        Username__c,
                                        Password__c
                                    FROM Portal_Credentials__r)
                                FROM Account
                                WHERE Physician_NPI_DEA__c = :npi];
                                
        if (!accounts.isEmpty()) {
            
            if (!accounts[0].Portal_Credentials__r.isEmpty()) {
            
                Portal_Credential__c credential = accounts[0].Portal_Credentials__r[0];
                credential.Password__c = password;
                update credential;
                result = password;
                PortalEmailController.sendResetPasswordEmail(accounts[0].Id, password);
            }
        }                         
        
        return result;       
    }
    
    public String changePatientPassword(String email) {
    
        String password = getRandomPassword();
        String result = null;
    
        Account[] accounts = [SELECT Id, Name, 
                                    (SELECT 
                                        Id, 
                                        Name, 
                                        Username__c,
                                        Password__c
                                    FROM Portal_Credentials__r)
                                FROM Account
                                WHERE PersonEmail = :email];
                                
        if (!accounts.isEmpty()) {
            
            if (!accounts[0].Portal_Credentials__r.isEmpty()) {
            
                Portal_Credential__c credential = accounts[0].Portal_Credentials__r[0];
                credential.Password__c = password;
                update credential;
                result = password;
                PortalEmailController.sendResetPasswordEmail(accounts[0].Id, password);
            }
        }                         
        
        return result;      
    }
    
    private String getRandomPassword() {
            
        Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0, len);    
        
        return pwd;
    }    

}