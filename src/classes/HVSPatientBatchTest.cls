@isTest
public with sharing class HVSPatientBatchTest {

    static testMethod void batchTest() {
    
        Account account = TestUtils.createAccount('patient');
        Health_Vault_Credential__c credential = TestUtils.createHealthVaultCredentials(account.Id);
        TestUtils.createDyrctAxessCustomSettings();
        
        Test.startTest();
        
        Database.executeBatch(new HVSPatientBatch());
        
        Test.stopTest();
    
    }

}