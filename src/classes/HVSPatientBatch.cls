/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
global class HVSPatientBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    private final Integer MAX_SCOPE = 1;

    public HVSPatientBatch() {}
    
    global Iterable<sObject> start(Database.Batchablecontext bc) {
        return HVSPatientBatchController.getInstance().getHealthVaultPatients();
    }

    global void execute(Database.Batchablecontext bc, sObject[] scope) {
        
        if (scope.size() <= MAX_SCOPE) {
    	
            Account[] patients = (Account[]) scope;
            HVInterface hv = new HVSDyrctAxessController();
    	   
            for (Account patient :patients) {
                
                try {
                    hv.synchronize(patient);
                }
                catch (Exception e) {}
            }
        }
    }
    
    global void finish(Database.Batchablecontext bc) {
    
        DyrctAxess_Settings__c settings = DyrctAxess_Settings__c.getOrgDefaults();
        
        if (settings.Enable_Process__c) {
            try {
                Integer interval = (settings.Interval__c != null && settings.Interval__c > 1) 
                                       ? Integer.valueOf(settings.Interval__c) 
                                       : 2;
                                       
                settings.Apex_Job__c = System.scheduleBatch(new HVSPatientBatch(), 'Health Vault API Process', interval, 1);
                update settings;
            }
            catch (Exception e) {}
        }
    }

}