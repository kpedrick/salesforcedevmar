public with sharing class TotalSalesDollarsToDate {
	public class Amount{
		public Object bu {get;set;}
		public Object amount {get;set;}
		public Amount(Object bu, Object amount){
			this.bu = bu;
			this.amount = '$ ' + String.valueOf(amount);
		}
	}
	public Amount[] amounts {get;set;}
	
	public TotalSalesDollarsToDate(){
		User[] users = [select profile.name from User where id = :userinfo.getUserId()];
		User u = null;
		if(!users.isEmpty()){
			u = users.get(0);
		}
		amounts = new Amount[]{};
		AggregateResult[] groupedResults = [SELECT SUM(Amount) sum, Business_Unit__c
      										  FROM Opportunity
      										 where status__c = 'Shipped'
      									  GROUP BY Business_Unit__c];
      	if(!groupedResults.isEmpty()){
			for(AggregateResult ar : groupedResults){
				String bu = String.valueOf(ar.get('Business_Unit__c')).toLowerCase();
				if(u == null){
					amounts.add(new Amount(ar.get('Business_Unit__c'),ar.get('sum')));
				}
				else if(u.profile.name.contains('Vaya')){
					if(bu.contains('vaya')){
						amounts.add(new Amount(ar.get('Business_Unit__c'),ar.get('sum')));
					}
				}
				else if(u.profile.name.contains('Dyrect')){
					if(bu.contains('dyrect')){
						amounts.add(new Amount(ar.get('Business_Unit__c'),ar.get('sum')));
					}
				}
				else{
					amounts.add(new Amount(ar.get('Business_Unit__c'),ar.get('sum')));
				}
			}
      	}
      	
	}
}