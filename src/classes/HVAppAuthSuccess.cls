@RestResource(urlMapping='/HealthVault/AppAuthSuccess/*')
global class HVAppAuthSuccess {

    @HttpGet
    global static SessionWebServiceResponse getHealthVaultUrl() {
    
        RestRequest req = RestContext.request;
        String accountId = req.params.get('accountid');
        String token = req.params.get('token');
        system.debug('request info: ' + accountId + ' : ' + token);
        try {
        	
            SessionWebServiceResponse result = new SessionWebServiceResponse(execute(accountId,token));
            return result;
        }
        catch (Exception e) {}
    
        return new SessionWebServiceResponse('false');
    } 
 
    public static String execute(String contactId,String token) {
    	
        HVGetPersonInfo info = new HVGetPersonInfo(Id.valueOf(contactId),token);
        system.debug(info);
        //HVResponse hvr = HVConnection.sendMethodRequest(controller.getRequestBody());
        return info.toString();
    }
 
    global class SessionWebServiceResponse {
    
        private String success;
        
        public SessionWebServiceResponse(String success) {
            this.success = success;
        }
    }

}