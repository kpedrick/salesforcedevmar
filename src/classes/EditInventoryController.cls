public with sharing class EditInventoryController {


public String invId {get;set;}
public Inventory__c inventory {get;set;}

public Inventory__c inventoryToCompare;

public EditInventoryController(){
    
    
    this.invId = (String) ApexPages.currentPage().getParameters().get('invId');
    
    if(this.invId != null && this.invId != '')
    this.inventory = [Select Id, 
                             Debit__c,
                             Credit__c,
                             Dosage__c,
                             //days__c,
                             Type__c,
                             Ledger__c,
                             Status__c,
                             Product_Id__c,
                             Physician__c,
                             Patient__c,
                             Prescription__c,
                             Prescription_Line_Item_Id__c,
                             Product_Id__r.Name,
                             Name   
                      From Inventory__c
                      Where Id = :this.invId];
    
    //track fields which might be updated   
    this.inventoryToCompare = new Inventory__c();             
    this.inventoryToCompare.Debit__c = this.inventory.debit__c;
    this.inventoryToCompare.Credit__c = this.inventory.credit__c;
    this.inventoryToCompare.Status__c = this.inventory.Status__c;
}

public PageReference cancel(){
    
    return new PageReference('/apex/InventoryManagement?id=' + this.inventory.Product_Id__c);
}

public PageReference save(){
    
    try{
        
        system.debug('inventory: ' +this.inventory);
        system.debug('inventory saved: '+ this.inventoryToCompare);
        if(this.inventoryToCompare.Status__c != this.inventory.Status__c
           || this.inventoryToCompare.Credit__c != this.inventory.Credit__c
           || this.inventoryToCompare.Debit__c != this.inventory.Debit__c){
            
            //Save inventory record, will create a new one - or will be updated the existent
            Inventory__c inv = new Inventory__c();
            inv.Product_ID__c =this.inventory.Product_Id__c;
            inv.Status__c = this.inventory.Status__c;
            inv.Type__c = this.inventory.Type__c;
            inv.Physician__c = this.inventory.Physician__c;
            inv.Patient__c = this.inventory.Patient__c;
            inv.Credit__c = this.inventory.Credit__c;
            inv.Debit__c =  this.inventory.Debit__c;
            inv.Dosage__c = this.inventory.Dosage__c;
            //inv.Days__c = this.inventory.Days__c;
            inv.Prescription__c = this.inventory.Prescription__c;
            inv.Prescription_Line_Item_Id__c = this.inventory.Prescription_Line_Item_Id__c;
            inv.Adjustment__c = true;
            insert inv;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Save success'));
            return new PageReference('/apex/InventoryManagement?id=' + this.inventory.Product_Id__c);
                
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Info, 'Credit/debit or status must to be edited to create an adjust inventory'));
            return null;
        }
        
    }catch(Exception e){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,e.getMessage()));
        return null;
    }
    
}


}