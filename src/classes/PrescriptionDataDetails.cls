/*******************************************************************************
Name              : PrescriptionDataDetails
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/11/2014          Peter Clifford          https://na5.salesforce.com/a0P7000000Cz7l1
*******************************************************************************/
public with sharing class PrescriptionDataDetails {

  public String selected {get;set;}
  public String days {get;set;}
  public String dosage {get;set;}
  public String administration {get;set;}
  public String refills {get;set;}
  public String productItem {get; set;}

  public PrescriptionDataDetails() {}

}