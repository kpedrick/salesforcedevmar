@isTest
private class OpportunityProductRollupTest {
	
	public class BatchContext implements Database.BatchableContext {
		public Id getChildJobId(){
			return null;
		}
		public Id getJobId(){
			return null;
		}
	}

    static testMethod void testProductNameRollUp() {

        TestFactory factory = new TestFactory();
        
        
        Boolean doInsert = true;

		factory.setPricebookStandard();
        factory.createPriceBook('VAYA', doInsert);
        factory.createAccount(doInsert, 'VAYA');
        factory.createOpportunity(doInsert, factory.account.Id, factory.pricebook.Id);
        factory.createProduct(doInsert);
        factory.createStandardPricebookEntry(doInsert, factory.product.Id, factory.standardId);
        factory.createPriceBookEntry(doInsert, factory.product.Id, factory.pricebook.Id);
        
        Test.startTest();
        
        factory.createOpportunityLineItem(doInsert, factory.opportunity.Id, factory.pricebookEntry.Id);
        
        Test.stopTest();
        
        Opportunity oppty = [SELECT Product_Name_s__c FROM Opportunity WHERE Id =: factory.opportunity.Id];
       
        system.assertEquals(factory.product.Name, oppty.Product_Name_s__c);
    }
    /*
    static testMethod void testRefillCountRollUp() {

        TestFactory factory = new TestFactory();
        
        Boolean doInsert = true;

		factory.setPricebookStandard();
        factory.createPriceBook();
        factory.createAccount(doInsert);
        factory.createOpportunity(doInsert);
        factory.createProduct(doInsert);
        factory.createPriceBookEntry(doInsert);
        
        factory.createOpportunityLineItem(false);
        factory.opportunityLineItem.Refills__c = '3';
        
        insert factory.opportunityLineItem;
        
        Opportunity oppty = [SELECT Refill_Count__c FROM Opportunity WHERE Id =: factory.opportunity.Id];
       
        system.assertEquals(oppty.Refill_Count__c, null);
        
        
        Opportunity[] result = [SELECT 
		                            Id, 
		                            Name,
		                            Type,
		                            PriceBook2Id,
		                            RecordTypeId,
		                            AccountId,
		                            CloseDate,
		                            Status__c,
		                            StageName,
		                            Business_Unit__c,
		                            First_Opportunity__c,
	                              (SELECT 
	                                 Id,
	                                 Name,
                                   	 Quantity,
                                   	 UnitPrice,                              
	                                 Product2Id,
	                                 PriceBookEntryId,
	                                 Administration__c,
	                                 Total_Refills__c,
	                                 Refills__c,
	                                 Product_Unit__c,
	                                 Days__c,
	                                 Product_Dose__c,
	                                 Dosage__c
	                                FROM OpportunityLineItems)		                              
		                          FROM Opportunity 
		                          WHERE Id = :factory.opportunity.Id];
		                          
		                          
		RFOpportunityRefillBatch batch = new RFOpportunityRefillBatch();
		
		batch.execute(new BatchContext(), result);
		
		
		Opportunity oppty2 = [SELECT Refill_Count__c FROM Opportunity WHERE Id != :factory.opportunity.Id];
		
		system.assertEquals(oppty2.Refill_Count__c, '1 of 3');                          
        
        OpportunityLineItem oli = [SELECT Id, Supply_Refill_Processing_Date__c FROM OpportunityLineItem WHERE Id = :factory.opportunityLineItem.Id ];
        
        system.debug(oli.Supply_Refill_Processing_Date__c);
        
        
        
        
        
    }
    
    static testMethod void testBatchNeverEnding(){
    	
    	
    	TestFactory factory = new TestFactory();
        
        Boolean doInsert = true;

		factory.setPricebookStandard();
        factory.createPriceBook();
        factory.createAccount(doInsert);
        factory.createOpportunity(doInsert);
        factory.createProduct(doInsert);
        factory.createPriceBookEntry(doInsert);
        
        factory.createOpportunityLineItem(false);
        factory.opportunityLineItem.Refills__c = '3';
        insert factory.opportunityLineItem;
        
        OpportunityLineItem[] items = [SELECT Id, OpportunityId FROM OpportunityLineItem ];
  
		Set<Id> opportunityIds = new Set<Id>();
    	for (OpportunityLineItem item :items) {
			opportunityIds.add(item.OpportunityId);
		} 
    	
    	
    	Opportunity[] result = [SELECT 
		                            Id, 
		                            Name,
		                            Type,
		                            PriceBook2Id,
		                            RecordTypeId,
		                            AccountId,
		                            CloseDate,
		                            Status__c,
		                            StageName,
		                            Business_Unit__c,
		                            First_Opportunity__c,
	                              (SELECT 
	                                 Id,
	                                 Name,
                                   	 Quantity,
                                   	 UnitPrice,                              
	                                 Product2Id,
	                                 PriceBookEntryId,
	                                 Administration__c,
	                                 Total_Refills__c,
	                                 Refills__c,
	                                 Product_Unit__c,
	                                 Days__c,
	                                 Product_Dose__c,
	                                 Dosage__c
	                                FROM OpportunityLineItems)
		                          FROM Opportunity 
		                          WHERE Id IN :opportunityIds];
		                          
		RFOpportunityRefillBatch batch = new RFOpportunityRefillBatch();
		
		batch.execute(new BatchContext(), result);
		                    
		List<Opportunity> opptyList = [SELECT Id FROM Opportunity];
		
		system.assertEquals(1, opptyList.size());
                     
    }
    */
}