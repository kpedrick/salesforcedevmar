public class AccountActiveStatusWrapper {
	
	public Account account;
	public Map<Id, Opportunity> opportunityMap;
	
	public Boolean hasActiveStatus = false;
	public Boolean hasNoContactStatus = false;
	public Boolean hasOpportunity = false;
	public Boolean hasShippedStatus = false;
	public Date lastActivityDate;
	public String activeStatus;
	
	public Date dateToday;
	
	public AccountActiveStatusWrapper(Account acct, Map<Id, Opportunity> opptyMap, Date dateToday){
		this.account = acct;
		this.opportunitymap = opptyMap;
		this.dateToday = dateToday;
	}
	
	public String calculateActiveStatus(){
		
		this.calculateHasOpportunity();
		this.calculateHasNoContactStatus();
		this.calculateHasActiveStatus();
		this.calculateLastActivityDate();
		this.calculateHasShippedStatus();
		
		if (this.hasOpportunity == false){
			this.activeStatus = '';
			return this.activeStatus;
		}
		
		if (hasShippedStatus == false && hasNoContactStatus == true){
			this.activeStatus = 'No Response';
			return this.activeStatus;
		}
		
		if (hasActiveStatus == true && this.lastActivityDate > this.dateToday){
			this.activeStatus = 'Active';
		}else{
			this.activeStatus = 'Pending';
		}
		
		if (this.hasShippedStatus == true && this.lastActivityDate < this.dateToday){
			this.activeStatus = 'Inactive';
		}
		
		return this.activeStatus;
		
	}
	
	public Boolean calculateHasOpportunity(){
		
		if (this.account.Opportunities.isEmpty() == false){
			this.hasOpportunity = true;
		}
		
		return this.hasOpportunity;
	}
	
	public Boolean calculateHasShippedStatus(){
		for (Opportunity opp : this.account.Opportunities){

			if (opp.Status__c != 'Shipped')
				continue;
					
			this.hasShippedStatus = true;
		}
		return this.hasShippedStatus;
	}
	
	
	public Boolean calculateHasNoContactStatus(){
		
		for (Opportunity opp : this.account.Opportunities){

			if (opp.Status__c != 'No Contact' && opp.Status__c != 'Canceled-Pharmacy' && opp.Status__c != 'Canceled-Call Center')
				continue;
					
			this.hasNoContactStatus = true;
		}
		return this.hasNoContactStatus;
	}
	
	public Boolean calculateHasActiveStatus(){
		
		for (Opportunity opp : this.account.Opportunities){

			if (opp.Status__c == null)
				continue;
					
			if (opp.Status__c == 'New' || opp.Status__c == 'No Contact' || opp.Status__c == 'Canceled-Pharmacy' || opp.Status__c == 'Canceled-Call Center')
				continue;
					
			this.hasActiveStatus = true;
		}
		
		return this.hasActiveStatus;
	}
	
	public Date calculateLastActivityDate(){
		
		for (Opportunity opp : this.account.Opportunities){
			
			Opportunity oppty = this.opportunityMap.get(opp.Id);
			
			for (OpportunityFeed opptyFeed :oppty.Feeds){
				
				if (this.lastActivityDate == null || this.lastActivityDate < opptyFeed.lastModifiedDate){
					this.lastActivityDate = Date.valueOf(opptyFeed.lastModifiedDate);
				}
			}
			
			for (OpportunityLineItem oli : oppty.OpportunityLineItems){
				
				if (oli.Shipping_Datetime__c == null)
					continue;
					
				if (oli.Supply_Ending_Date__c == null)
					continue;
				
				if (this.lastActivityDate == null || this.lastActivityDate < oli.Supply_Ending_Date__c){
					this.lastActivityDate = oli.Supply_Ending_Date__c;
				}
			}
		}
		
		// if there is no activity in chatter and no shipping date
		if (this.lastActivityDate == null){
			for (Opportunity opp : this.account.Opportunities){
				if (this.lastActivityDate == null || this.lastActivityDate < opp.LastModifiedDate){
					this.lastActivityDate = Date.valueOf(opp.LastModifiedDate);
				}
			}
		}
		
		if (this.lastActivityDate != null){
			this.lastActivityDate = this.lastActivityDate.addDays(45);	
		}
		
		return this.lastActivityDate;
	}
	

}