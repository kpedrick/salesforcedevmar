/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class PortalPatientRestServiceController {

    private static PortalPatientRestServiceController instance = null;
    private Map<String, Schema.Recordtypeinfo> info;
    
    private PortalPatientRestServiceController() {
        info = Schema.SObjectType.Account.getRecordTypeInfosByName();
    }
    
    public static PortalPatientRestServiceController getInstance() {
    
        if (instance == null) {
            instance = new PortalPatientRestServiceController();
        }
        
        return instance;
    }
    
    public String createPatientRegistration(Map<String, String> params){
    	
    	system.debug('paramas ' + params);
        for (String key :params.keySet()) {
        
            System.debug ('Key ' + key);
            System.debug ('Value ' + params.get(key));
        }
        
        
        if (this.validateRegistrationParams(params)) {
        
            Id accountId = this.insertPatientRegistration(params);
            if (accountId != null) {
            	if (this.insertRegistrationCredential(accountId, params) != null){
            		PortalEmailController.sendPatientRegistrationEmail(accountId, params.get('args[Password__c]'));
            		
                	return accountId;
            	}
            }
        }
        
        
        return null;
    }
    
    private Id insertRegistrationCredential(Id accountId, Map<String, String> params) {
    
        Portal_Credential__c credential = new Portal_Credential__c();
        credential.Application__c = 'DA';
        credential.Email__c = params.get('args[Email]');
        credential.Account__c = accountId;
        credential.Password__c = params.get('args[Password__c]');
	
	    insert credential;
	    return credential.Id;       
    }
    
    public Map<String, String> createPatient(Map<String, String> params) {
    
    
        system.debug('paramas ' + params);
        for (String key :params.keySet()) {
        
            System.debug ('Key ' + key);
            System.debug ('Value ' + params.get(key));
        }
    
        if (validateParams(params)) {
        
            Id accountId = insertPatient(params);
            if (accountId != null) {
            	Map<String, String> result = insertCredential(accountId, params);
                PortalEmailController.sendWelcomeEmail(accountId, result.get('password'));
                return result;                
            }
        }
        
        return null;
    }
    
    private Id insertPatientRegistration(Map<String, String> params){
    	
    	
    	Account account = new Account();

    	account.FirstName = params.get('args[FirstName]');
        account.LastName = params.get('args[LastName]');
        account.PersonEmail = params.get('args[Email]');
        account.Physician_Registration__c = params.get('args[Physician_Registration__c]');
        account.PersonMobilePhone = params.get('args[Phone]');
        account.RecordTypeId = info.get('Patient').getRecordTypeId();
        account.Business_Unit__c = 'DyrctAxess';
        
        insert account;
        
        return account.Id;
    }
    
    private Id insertPatient(Map<String, String> params) {
    
        Account account = new Account();
        account.FirstName = params.get('FirstName');
        account.LastName = params.get('LastName');
        account.PersonEmail = params.get('PersonEmail');
        
        account.Gender__c = params.get('Gender');
        account.PersonMobilePhone = params.get('PersonMobilePhone');
        account.PersonHomePhone = params.get('PersonHomePhone');
        
        account.Patient_Birthday_Year__c = params.get('Patient_Birthday_Year__c');
        account.Patient_Birthday_Month__c = params.get('Patient_Birthday_Month__c');
        account.Patient_Birthday_Day__c = params.get('Patient_Birthday_Day__c');
        
        String birthdate = params.get('Patient_Birthday_Year__c') + '-' + getNumberMonth(params.get('Patient_Birthday_Month__c')) + '-' + params.get('Patient_Birthday_Day__c'); 
        
        account.PersonBirthdate = Date.valueOf(birthdate);
        
        account.ShippingCity = params.get('BillingCity'); 
        account.ShippingCountry = params.get('BillingCountry');
        account.ShippingState = params.get('BillingState');
        account.ShippingStreet = params.get('BillingStreet');
        account.ShippingPostalCode = params.get('BillingPostalCode');
        account.Business_Unit__c = 'DyrctAxess';
        
        account.Physician__c = params.get('Physician__c');
        
        account.RecordTypeId = info.get('Patient').getRecordTypeId();
        
        insert account;
        
        return account.Id;
    }
    
    private Map<String, String> insertCredential(Id accountId, Map<String, String> params) {
    
        Map<String, String> result = new Map<String, String>(); 
        String password = getRandomPassword();
    
        Portal_Credential__c credential = new Portal_Credential__c();
        credential.Application__c = 'DA';
        credential.Email__c = params.get('PersonEmail');
        credential.Account__c = accountId;
        credential.Password__c = password;
    
        insert credential;
        
        result.put('id', credential.Id);
        result.put('password', password);
        
        return result;
    }
        
    private Boolean validateParams(Map<String, String> params) {
    
        Boolean result = true;
        
        result &= params.containsKey('FirstName') && (params.get('FirstName') != null || params.get('FirstName') != '');
        result &= params.containsKey('LastName') && (params.get('LastName') != null || params.get('LastName') != '');
        result &= params.containsKey('PersonEmail') && (params.get('PersonEmail') != null || params.get('PersonEmail') != '');
        
        return result;
    }
    
    private boolean validateRegistrationParams(Map<String, String> params){
    	
    	Boolean result = true;
        
        for (String value :params.keySet()) {
            system.debug(value + ' ** ' + params.get(value));
        }
        
        result &= params.containsKey('args[FirstName]') && (params.get('args[FirstName]') != null || params.get('args[FirstName]') != '');
        result &= params.containsKey('args[LastName]') && (params.get('args[LastName]') != null || params.get('args[LastName]') != '');
        result &= params.containsKey('args[Email]') && (params.get('args[Email]') != null || params.get('args[Email]') != '');
        //result &= params.containsKey('args[Phone]') && (params.get('args[Phone]') != null || params.get('args[Phone]') != '');
        //result &= params.containsKey('args[Physician_NPI_DEA__c]') && (params.get('args[Physician_NPI_DEA__c]') != null || params.get('args[Physician_NPI_DEA__c]') != '');
        result &= params.containsKey('args[Password__c]') && (params.get('args[Password__c]') != null || params.get('args[Password__c]') != '');    
    
        System.debug('Validate Parameters ' + result);
    
        return result;
    }
    
    private String getRandomPassword() {
		    
		Integer len = 10;
		Blob blobKey = crypto.generateAesKey(128);
		String key = EncodingUtil.convertToHex(blobKey);
		String pwd = key.substring(0, len);    
        
        return pwd;
    }
    
    private String getNumberMonth(String month) {
    
        String result = '01';

        if ('February'.equalsIgnoreCase(month)) {
            result = '02';     
        }        
        if ('March'.equalsIgnoreCase(month)) {
            result = '03';
        }        
        if ('April'.equalsIgnoreCase(month)) {
            result = '04';
        }        
        if ('May'.equalsIgnoreCase(month)) {
            result = '05';
        }        
        if ('June'.equalsIgnoreCase(month)) {
            result = '06';
        }        
        if ('July'.equalsIgnoreCase(month)) {
            result = '07';
        }        
        if ('August'.equalsIgnoreCase(month)) {
            result = '08';
        }        
        if ('September'.equalsIgnoreCase(month)) {
            result = '09';
        }        
        if ('October'.equalsIgnoreCase(month)) {
            result = '10';
        }        
        if ('November'.equalsIgnoreCase(month)) {
            result = '11';
        }        
        if ('December'.equalsIgnoreCase(month)) {
            result = '12';
        }        
    
        return result;
    }
    

}