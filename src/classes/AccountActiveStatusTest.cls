@isTest
private class AccountActiveStatusTest {

    static testMethod void testBatch() {
    	
    	TestFactory factory = new TestFactory();
    	factory.createAll(true);
    	
    	Test.startTest();
    	
    	AccountActiveStatusBatch job = new AccountActiveStatusBatch();
		Id processId = Database.executeBatch(job, 1);
		
        Test.stopTest();
    }
    
    static testMethod void testBatchFuture() {
    	
    	TestFactory factory = new TestFactory();
    	factory.createAll(true);
    	
    	factory.opportunity.Status__c = 'Shipped';
    	update factory.opportunity;
    	
    	Test.startTest();
    	
     	AccountActiveStatusBatch job = new AccountActiveStatusBatch();
		job.execute([SELECT Id FROM Account WHERE RecordType.Name = 'Patient'], Date.today().addDays(100));
		
		Test.stopTest();
    }
    
    static testMethod void testTrigger() {
    	
    	TestFactory factory = new TestFactory();
    	factory.createAll(true);
    	
    	
    	factory.opportunity.Status__c = 'On Hold-Call Center';
    	
    	update factory.opportunity;
    	
    }
    
    static testMethod void testAccountActiveStatusSchedulable(){
    	OpportunityRefillTestFactory factory = new OpportunityRefillTestFactory();
    	factory.createAll(true);
    	
    	Test.startTest();
    	String jobId = System.schedule('AccountActiveStatusSchedulableTest', '0 0 * * * ?', new AccountActiveStatusSchedulable());
    	Test.stopTest();
    }
   
}