public class OpportunityAccountActiveStatusTrigger extends  SObjectTrigger {

	public OpportunityAccountActiveStatusTrigger(sObject[] sObjectOldList, sObject[] sObjectNewList) {
		super(sObjectOldList, sObjectNewList);
	}
    
	public override Boolean executable(sObject sObjectOld, sObject sObjectNew){
		return true;
	}
    
	public override void execute(sObject[] sObjectList, Boolean forceUpdate){
		
		Set<Id> accountIds = new Set<Id>();
		List<Account> accountUpdateList = new List<Account>();
		
		for (SObject obj :sObjectList){
			accountIds.add((Id)obj.get(''+ Opportunity.AccountId));
			
			if (trigger.isUpdate == true){
				accountIds.add((Id)trigger.oldMap.get((Id)obj.get('Id')).get('AccountId'));
			}
		}
		accountIds.remove(null);
		
		
		Map<Id,Account> accountMap = new Map<Id,Account>(  	[SELECT Id,
																	Active__c,
																	(SELECT Id,
																			CreatedDate,
																			LastModifiedDate,
																			CloseDate,
																			Status__c
																	 FROM Opportunities)
															 FROM Account
															 WHERE Id IN :accountIds
															 AND RecordType.Name = 'Patient'
															 ]);
		
		OpportunityAccountActiveStatusUtil oaas = new OpportunityAccountActiveStatusUtil(accountMap, Date.today());
		
		accountUpdateList.addAll(oaas.createUpdateList());
		
		if (accountUpdateList.isEmpty() == true)
			return;
			
	
		try{
			update accountUpdateList;
		}catch(Exception e){
			system.debug(e);
		}
	}
}