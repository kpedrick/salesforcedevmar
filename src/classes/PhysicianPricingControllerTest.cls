@isTest
private class PhysicianPricingControllerTest {
	
	
	
	

    @isTest(seeAllData=true) static void physicianPricingControllerTestWithoutPricebook() {
     
        Account physician = TestUtils.createAccount('physician');
        Opportunity opportunity = TestUtils.createPrescription(physician.Id);
        
        Test.startTest();
        
        ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(physician);
        PhysicianPricingController controller = new PhysicianPricingController(stdCon);
        //Test new pricebook is created
        controller.newPricebook.Name = 'New Pricebook';
        controller.saveNewPb();
        //check if not null
        system.assertEquals(true,controller.newPricebook != null);
        List<PricebookEntry> lstPbe = new List<PricebookEntry>();
       	List<PricebookEntry> listPbeStandard = [Select Id,Product2Id,UnitPrice From PricebookEntry Where Pricebook2Id = :controller.newPricebook.Id];
        for(PricebookEntry pbe: listPbeStandard){
       	  pbe.UnitPrice = 0.5;
        }
        //test prices lower
        try{
         update listPbeStandard;
        }catch(System.dmlException dml){
        	system.assertEquals(true,'Price cannot be less than standard' == dml.getDmlMessage(0));
        }
        
        Test.stopTest();
     
    }
    
     @isTest(seeAllData=true) static void physicianPricingControllerTestWithPricebook() {
       Account physician = TestUtils.createAccount('physician');
       Opportunity opportunity = TestUtils.createPrescription(physician.Id);
      
       Test.startTest();
       Pricebook2 pb = new Pricebook2(Name='phy test',Physician__c = physician.Id,isActive=true);
       insert pb;
       List<PricebookEntry> lstPbe = new List<PricebookEntry>();
       List<PricebookEntry> listPbeStandard = [Select Id,Product2Id,UnitPrice From PricebookEntry Where Pricebook2.IsStandard = true];
       for(PricebookEntry pbe: listPbeStandard){
       	PricebookEntry p = new PricebookEntry(Product2Id = pbe.Product2Id, Pricebook2Id = pb.Id,isActive = true,unitPrice = pbe.UnitPrice);
       	lstPbe.add(p);
       }
       insert lstPbe;
       
       
        ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(physician);
        PhysicianPricingController controller = new PhysicianPricingController(stdCon);
        //Test % markup on pricebook entry values
        PricebookEntry pbEntry = new PricebookEntry();
        for(PricebookEntry pbe : controller.listPbe){
        	pbe.UnitPrice = 30;
        	pbEntry = pbe;
        	break;
        }
        controller.save();
        //test markup generated
        pbEntry = [Select Id,Markup__c From PricebookEntry where id = :pbEntry.Id limit 1];
        system.assertEquals(true,pbEntry.Markup__c != null);
         //Test price lower 
        for(PricebookEntry pbe : controller.listPbe){
        	pbe.UnitPrice = 0;
        	pbEntry = pbe;
        	break;
        }
        try{
         //save new prices
         controller.save();
        }catch(System.dmlException dml){
        	system.assertEquals(true,'Price cannot be less than standard' == dml.getDmlMessage(0));
        }
        Test.stopTest();
     }
}