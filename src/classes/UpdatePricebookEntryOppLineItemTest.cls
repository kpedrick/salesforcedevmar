@isTest
private class UpdatePricebookEntryOppLineItemTest {

    @isTest(seeAllData=true) static void UpdatePricebookEntryOppLineItemTest() {
    	
        Account physician = TestUtils.createAccount('physician');
        Opportunity opportunity = TestUtils.createPrescription(physician.Id);
        
        Test.startTest();
        //Create product and values for pricebook entries
        Product2 prod = TestUtils.createProduct('prod 1');
         //Standard Price book
	 	Pricebook2 stdPricebook = [Select Id 
 	 									  From Pricebook2
 	 									  Where IsStandard = true];
	 	PricebookEntry stdPbe = new PricebookEntry(IsActive = true,Pricebook2Id= stdPricebook.Id, Product2Id = prod.Id,UnitPrice=2);
	    insert stdPbe;
        //Create new pricebook with entries
        Pricebook2 pb = new Pricebook2(Name='phy test',Physician__c = physician.Id,isActive=true);
        insert pb;
        List<PricebookEntry> lstPbe = new List<PricebookEntry>();
        List<PricebookEntry> listPbeStandard = [Select Id,Product2Id,UnitPrice From PricebookEntry Where Pricebook2.IsStandard = true];
        for(PricebookEntry pbe2: listPbeStandard){
       	 PricebookEntry p = new PricebookEntry(Product2Id = pbe2.Product2Id, Pricebook2Id = pb.Id,isActive = true,unitPrice = pbe2.UnitPrice);
       	 lstPbe.add(p);
        }
        insert lstPbe;
        //update pricebook to opp
	     opportunity.Pricebook2Id = pb.Id;
         update opportunity;
        //Update pricebook entry price to the product inserted
        PricebookEntry pbe3 =[Select Id,UnitPrice,Markup__c From PricebookEntry Where Pricebook2Id = :pb.Id AND Product2Id = :prod.Id];
        pbe3.UnitPrice = 30;
        pbe3.Markup__c = 200; 
        update pbe3;
        //Change value on the standard pricebooks: It could be : dyrctacces, vaya or standard itself   - This is to detect if the price went up 
        Map<Id,Pricebook2> mapPriceBooks = new Map<Id,Pricebook2>([Select Id,
        																  Name,	
        																  (Select Id,UnitPrice,Product2Id From PricebookEntries) 
        													       From Pricebook2 
        													       Where Name = :physician.Business_Unit__c]);
        List<PricebookEntry> listpbe = new List<pricebookentry>();													       
        for(Pricebook2 pbook : mapPricebooks.values()){
        	for(PricebookEntry p : pbook.pricebookentries){
        		if(p.Product2Id == prod.Id){
        			p.UnitPrice = 40;
        			listpbe.add(p);
        			break;
        		}
        	}
        } 
        update listPbe;											       
        //Finally, create the opp line item
	 	OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opportunity.Id,PricebookEntryId= pbe3.Id,TotalPrice= 300,Quantity = 1);
	 	insert oli;
      /*  pbe3 =[Select Id,UnitPrice,Markup__c From PricebookEntry Where Pricebook2Id = :pb.Id AND Product2Id = :prod.Id];
        system.assertEquals(true,pbe3.UnitPrice == (40 * pbe3.markup__c / 100));*/
       
       
       
       Test.stopTest();
       
    }
}