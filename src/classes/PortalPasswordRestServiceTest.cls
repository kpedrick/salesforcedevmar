/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@isTest(seeAllData=true)
public with sharing class PortalPasswordRestServiceTest {

    static testMethod void testPasswordRestServicePatient() {
 
        executeTestPasswordRestService('patient');
    }
    
    static testMethod void testPasswordRestServicePhysician() {
 
        executeTestPasswordRestService('physician');
    }    
    
    private static void executeTestPasswordRestService(String accountType) {

        Account account = TestUtils.createAccount(accountType);
        Portal_Credential__c credential = TestUtils.createCredential(account.Id);
        
        PortalPasswordRestService.PortalPasswordRestServiceData result = null;
        
        Test.startTest();
        
        Restrequest req = new Restrequest();
        req.params.put('username', credential.Username__c);
        req.requestURI = '/' + accountType;
        
        RestContext.request = req;
        result = PortalPasswordRestService.doGet(); 
        
        Test.stopTest();
        
        System.debug(result.password);
        
        System.assert(result.password != null);
        System.assertNotEquals(credential.Password__c, result.password);
    }
    
}