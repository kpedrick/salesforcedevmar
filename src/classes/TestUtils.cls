/*
Name              : TestUtils
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by              Reference             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/07/2014          Peter Clifford            https://na5.salesforce.com/a0P7000000CzZM8EAN
*/
public with sharing class TestUtils {

    public static Account createAccount(String type) {
    
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rt = d.getRecordTypeInfosByName();
        
        Account account = new Account();
        account.FirstName = 'Test Name';
        account.LastName = 'Test Name';
        account.PersonEmail = 'test@test.com';
        account.Physician_NPI_DEA__c = 'test@test.com';
        account.Business_Unit__c = 'DyrctAxess';
        
        account.RecordTypeId = rt.get('Patient').getRecordTypeId();
        
        if ('physician'.equals(type)) {
            account.RecordTypeId = rt.get('Physician').getRecordTypeId();
        }
        
        insert account;
        
        return [SELECT Id,Business_Unit__c, PersonEmail, Physician_NPI_DEA__c, RecordType.Name FROM Account WHERE Id = :account.Id];
    }
    
    public static Portal_Credential__c createCredential(Id accountId) {
    
        Portal_Credential__c credential = new Portal_Credential__c();
        credential.Account__c = accountId;
        credential.Email__c = 'test@test.com';
        credential.Password__c = 'testtest';
    
        insert credential;
       
        return [SELECT Id, Account__c, Email__c, Password__c, Username__c FROM Portal_Credential__c WHERE Id = :credential.Id];
    }
    
    public static Health_Vault_Credential__c createHealthVaultCredentials(Id accountId) {
    
        Health_Vault_Credential__c credential = new Health_Vault_Credential__c();
        credential.Account__c = accountId;
        credential.Application_Id__c = '1234567890';
        credential.Application_Shared_Secret__c = 'shared_token';
        credential.Application_Token__c = '1234567890';
        
        insert credential;
        
        return [SELECT Id, Account__c, Application_Id__c, Application_Token__c, Application_Shared_Secret__c FROM Health_Vault_Credential__c WHERE Id = :credential.Id];
    }
    
    public static Health_Activity__c createHealthActivity(Id accountId) {
    
        Health_Activity__c hv = new Health_Activity__c();
        hv.Account__c = accountId;
        hv.Blood_Pressure_Diastolic__c = 12;
        hv.Blood_Pressure_Systolic__c = 20;
        hv.Blood_Pressure_Pulse__c = 10;
        hv.Date__c = Date.today().addDays(1);
        
        insert hv;
        return  [SELECT Id, Date__c, Blood_Pressure_Diastolic__c, Blood_Pressure_Systolic__c, Blood_Pressure_Pulse__c FROM Health_Activity__c WHERE Id = :hv.Id];
    }
    
    public static Restrequest getPatientParamsMap(String physicianId) {
    
        Restrequest result = new Restrequest();
        
        result.addParameter('FirstName', 'TestName');
        result.addParameter('LastName', 'TestName');
        result.addParameter('PersonEmail', 'test@test.com');
        result.addParameter('Gender', 'Male');
        result.addParameter('PersonMobilePhone', '(555) 345-6574');
        result.addParameter('PersonHomePhone', '(555) 345-6574');
        
        result.addParameter('Patient_Birthday_Year__c', '2000');
        result.addParameter('Patient_Birthday_Month__c', '12');
        result.addParameter('Patient_Birthday_Day__c', '01');        
    
        result.addParameter('BillingCity', 'Test City');
        result.addParameter('BillingCountry', 'Test Country');
        result.addParameter('BillingState', 'Test State');
        result.addParameter('BillingStreet', 'Test Street');
        result.addParameter('BillingPostalCode', 'Test Code');    
    
        result.addParameter('Physician__c', String.valueOf(physicianId));
        
        return result;
    }    
    
    public static Restrequest getPhysicianParamsMap() {
    
        Restrequest result = new Restrequest();
        
        result.addParameter('args[FirstName]', 'TestName');
        result.addParameter('args[LastName]', 'TestName');
        result.addParameter('args[Email]', 'test@test.com');
        result.addParameter('args[Physician_NPI_DEA__c]', 'Test NPI');    
        result.addParameter('args[Password__c]', 'Test Password');
        result.addParameter('args[Phone]', '2638726');
        
        return result;
    }    
    
    public static Restrequest getPrescriptionParamsMap(Id accountId) {
    
        Restrequest result = new Restrequest();
        
        result.addParameter('content', '[{"selected":"true","days":"30","dosage":"a00e0000005mmb9AAA","administration":"QD","refills":"0","productItem":"01t400000045z2XAAQ"}]');
        result.addParameter('patient', accountId);
        
        return result;        	
    }
    
    public static void createDyrctAxessCustomSettings() {
    
        DyrctAxess_Settings__c settings = new DyrctAxess_Settings__c();
        
        settings.Interval__c = 2;
        settings.Health_Vault_Endpoint__c = 'http://hv.com';
        settings.Portal_Base_URL__c = 'http://portal.com';
        settings.Application_Id__c = '1234567890';
        settings.Enable_Process__c = true;
        
        insert settings; 
        
        
        Health_Vault_Types__c[] hvt = new Health_Vault_Types__c[] {};
        hvt.add(new Health_Vault_Types__c(Value__c = 'ca3c57f4-f4c1-4e15-be67-0a3caf5414ed', Name = 'Blood Pressure Measurement'));
        
        insert hvt;  
    }    
    
    public static Opportunity createPrescription(Id accountId) {
    
        Opportunity opportunity = new Opportunity();
        opportunity.StageName = 'Test Stage';
        opportunity.Name = 'Test';
        opportunity.AccountId = accountId;
        opportunity.CloseDate = Date.today();
        
        insert opportunity;    
        
        return opportunity;
    }
    
    public static Product2 createProduct(String name){
	 	Product2 prod = new Product2(Name = name,IsActive =true);
	 	insert prod;
	 	return prod;
 	}
 
	 /*
	 * @descp: This method retrieve standard pricebook to bind pricebookentry for the product given
	   All test classes which call this method must be "@isTest(seeAllData=true)" declared.
	   @author: mauricio diaz
	   @date: 01/05/2015
	 */
	 public static OpportunityLineItem createOppLineItem(Id oppId,Id prodId,Decimal quantity){
	 	 
	 	 //Standard Price book
	 	 Pricebook2 stdPricebook = [Select Id 
 	 									  From Pricebook2
 	 									  Where IsStandard = true];
	 	 PricebookEntry stdPbe = new PricebookEntry(IsActive = true,Pricebook2Id= stdPricebook.Id, Product2Id = prodId,UnitPrice=300);
	     insert stdPbe;
	     
	     //Custom pricebook which is related to Opp id 
	 	 Pricebook2 oppPricebook = [Select Id 
 	 									  From Pricebook2
 	 									  Where Id IN (Select Pricebook2Id 
 	 									  			   From Opportunity Where Id = :oppId )];
	 	 PricebookEntry pbe = new PricebookEntry(IsActive = true,Pricebook2Id= oppPricebook.Id, Product2Id = prodId,UnitPrice=300);
	     insert pbe;
	     
	     //Finally, create the opp line item
	 	 OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppId,PricebookEntryId= pbe.Id,TotalPrice= 300,Quantity = quantity);
	 	 insert oli;
	 	 return oli;
	 }
    
}