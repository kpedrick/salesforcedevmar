/*******************************************************************************
Name              : OpportunityReceiptItemizedController
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       09/04/2014           Peter Clifford       Create SFDC Objects to support Linvio Quotes and Invoices https://na5.salesforce.com/a0P7000000Czop0
*******************************************************************************/
public with sharing class OpportunityReceiptItemizedController {

  public Id opportunityId;
  public OpportunityLineItem[] items;
  public Opportunity opp {get; set;}

  public OpportunityReceiptItemizedController() {
    System.debug('OpportunityReceiptItemizedController::OpportunityReceiptItemizedController = ' + opportunityId);
  }
  
  public void setOpportunityId(Id value) {
    opportunityId = value;
    loadOpportunity();
  }
  
  public Id getOpportunityId() {
  	return opportunityId;
  }
  
  public void setItems(OpportunityLineItem[] value) {}
  
  public OpportunityLineItem[] getItems() {
  
    OpportunityLineItem[] result = new OpportunityLineItem[] {};
    
    result = [SELECT 
                  Id, 
                  Name, 
                  UnitPrice, 
                  Description, 
                  Product2Id, 
                  ProductCode, 
                  TotalPrice,
                  Quantity,
                  Dosage_Value__c,
                  Days__c,
                  PricebookEntry.Name
              FROM OpportunityLineItem
              WHERE OpportunityId = :opportunityId];
 
    return result;   
  }
 
  public void loadOpportunity() {
  
    opp = [SELECT 
	              Id, 
	              Name,
	              pymt__Total_Amount__c, 
	              pymt__Payments_Made__c, 
	              pymt__Balance__c 
	          FROM Opportunity 
	          WHERE Id = :opportunityId];
  
  }
  
}