/*******************************************************************************
Name              : RFOpportunityRefillBatch
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       10/10/2014           Peter Clifford          
*******************************************************************************/
global with sharing class RFOpportunityRefillBatch implements Database.Batchable<Opportunity>, Schedulable {

  public RFOpportunityRefillBatch() {}
  
  global void execute(SchedulableContext sc) {
  
    Database.executeBatch(new RFOpportunityRefillBatch());
  }
    
  global Iterable<Opportunity> start(Database.BatchableContext bc) {
  
    Date todayDate = Date.today();
    OpportunityLineItem[] items = [SELECT 
	                                    Id, 
	                                    OpportunityId 
                                    FROM OpportunityLineItem 
                                    WHERE Supply_Refill_Processing_Date__c = :todayDate];
  
    Set<Id> opportunityIds = new Set<Id>();
    
    for (OpportunityLineItem item :items) {
      opportunityIds.add(item.OpportunityId);
    } 
  
    Opportunity[] result = [SELECT 
		                            Id, 
		                            Name,
		                            Type,
		                            PriceBook2Id,
		                            RecordTypeId,
		                            AccountId,
		                            CloseDate,
		                            Status__c,
		                            StageName,
		                            Business_Unit__c,
		                            //First_Opportunity__c,
	                              (SELECT 
	                                 Id,
	                                 Name,
                                   	 Quantity,
                                   	 UnitPrice,                              
	                                 Product2Id,
	                                 PriceBookEntryId,
	                                 Administration__c,
	                                 Total_Refills__c,
	                                 Refills__c,
	                                 Product_Unit__c,
	                                 Days__c,
	                                 Product_Dose__c,
	                                 Dosage__c
	                                FROM OpportunityLineItems
	                                WHERE Supply_Refill_Processing_Date__c = :todayDate
	                                AND Refills__c != null)
		                              
		                          FROM Opportunity 
		                          WHERE Id IN :opportunityIds];
	                          
    return result;
  }
  
	global void execute(Database.BatchableContext bc, Opportunity[] scope) {
  
		Map<Integer, Opportunity> opportunities = new Map<Integer, Opportunity>();
		Map<Integer, OpportunityLineItem[]> items = new Map<Integer, OpportunityLineItem[]>();
		OpportunityLineItem[] linesToInsert = new OpportunityLineItem[] {};
    
		Integer counter = 0;
		for (Opportunity opportunity :scope) {
    
			Opportunity newOpportunity = opportunity.clone(false, true);
			newOpportunity.CloseDate = Date.today();
			newOpportunity.StageName = 'New';
			newOpportunity.Status__c = 'New';
			newOpportunity.Type = 'Refill Prescription';
      
			//if (newOpportunity.First_Opportunity__c == null){
			//	newOpportunity.First_Opportunity__c = opportunity.Id;
			//}
      
      		String refillCount = '';
			for (OpportunityLineItem item :opportunity.OpportunityLineItems) {
      
				OpportunityLineItem newItem = item.clone(false, true);
	        
				Integer newRefill = Integer.valueOf(item.Refills__c) - 1;
				newItem.Refills__c = String.valueOf(newRefill);
				
				
				if (newItem.Total_Refills__c != null){
	        		refillCount = String.valueOf(newItem.Total_Refills__c - newRefill) +' of '+ String.valueOf(newItem.Total_Refills__c) +', ';
	        	}
	        
				if (!items.containsKey(counter)) {
					items.put(counter, new OpportunityLineItem[] {});
				}
	        
				items.get(counter).add(newItem);
			}
			
			if (refillCount != ''){
				refillCount = refillCount.subString(0, refillCount.length() - 2);
	      	}
	      	if (refillCount.length() > 255){
				refillCount = refillCount.subString(0, 255);
		  	}
			
			newOpportunity.Refill_Count__c = refillCount;
			
			opportunities.put(counter, newOpportunity);

			counter ++;
		}
    
		if (!opportunities.isEmpty()) {
    	
			insert opportunities.values();
    
			for (Integer index :items.keySet()) {
	    
				Opportunity newOpportunity = opportunities.get(index);
				
				for (OpportunityLineItem newItem :items.get(index)) {
	      	
					newItem.OpportunityId = newOpportunity.Id;
					linesToInsert.add(newItem);
				}
			}
	  
			if (!linesToInsert.isEmpty()) {
				insert linesToInsert;
			}
		}
	}
  
  global void finish(Database.BatchableContext bc) {}
  
  
}