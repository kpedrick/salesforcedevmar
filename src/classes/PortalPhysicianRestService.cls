/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping = '/portal/physician/*')
global with sharing class PortalPhysicianRestService {

    public static Restrequest req = null;

    @httpGet
    global static PortalPhysicianRestServiceData doGet() {
    
        req = RestContext.request;
        
        String response = PortalPhysicianRestServiceController.getInstance().createPhysician(req.params);
        
        return new PortalPhysicianRestServiceData(response);
    }

    global class PortalPhysicianRestServiceData {
        
        public String id;
        
        public PortalPhysicianRestServiceData(String id) {
            this.id = id == null ? '' : id;
        }
    }

}