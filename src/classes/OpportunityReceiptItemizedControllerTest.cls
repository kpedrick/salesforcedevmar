/*******************************************************************************
Name              : OpportunityReceiptItemizedControllerTest
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       09/04/2014           Peter Clifford       Create SFDC Objects to support Linvio Quotes and Invoices https://na5.salesforce.com/a0P7000000Czop0
*******************************************************************************/
@isTest
public with sharing class OpportunityReceiptItemizedControllerTest {
	
	private static Account account = null;
	private static Opportunity opportunity = null;
	
  static testMethod void test() {
  
    OpportunityReceiptItemizedController controller = new OpportunityReceiptItemizedController();
    
    account = TestUtils.createAccount('Patient');
    opportunity = TestUtils.createPrescription(account.Id);
    
    Test.startTest();
    
    controller.setOpportunityId(opportunity.Id);
    
    Test.stopTest();
    
    OpportunityLineItem[] items = controller.getItems();
    System.assert(items.isEmpty());
    
    Id opportunityId = controller.getOpportunityId();
    System.assertEquals(opportunityId, opportunity.Id); 
    
  }

}