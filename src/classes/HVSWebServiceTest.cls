@isTest
public with sharing class HVSWebServiceTest {

    static testMethod void testConnectionWebService() {

        Account account = TestUtils.createAccount('patient');
        Health_Vault_Credential__c credential = TestUtils.createHealthVaultCredentials(account.Id);
        
        Restrequest request = new Restrequest();
        request.requestURI = 'http://url.com/' + String.valueOf(account.Id);
        HVSConnectionWebService.ConnectionWebServiceResponse result = null;
        
        Test.startTest();
        
        RestContext.request = request;
        result = HVSConnectionWebService.getHealthVaultUrl();
        
        Test.stopTest();
        
    }
    
    
    static testMethod void testSessionWebService() {

    
    }    
    
    
    static testMethod void testLogsRestService() {

        Account account = TestUtils.createAccount('patient');
        Health_Vault_Credential__c credential = TestUtils.createHealthVaultCredentials(account.Id);        
        Health_Activity__c activity = TestUtils.createHealthActivity(account.Id);
        
        HVSLogsRestService.HVSLogsResponse result = null;
        
        Test.startTest();
        
        Restrequest request = new Restrequest();
        request.requestURI = 'http://url.com/' + String.valueOf(account.Id);
       
        RestContext.request = request; 
        
        result = HVSLogsRestService.getHttp();
        
        Test.stopTest();
    
    }    
    
    

}