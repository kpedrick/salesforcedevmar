/*******************************************************************************
Name              : PortalEmailController
Description       : -
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/11/2014          Peter Clifford          https://na5.salesforce.com/a0P7000000Cz7l1
*******************************************************************************/
public with sharing class PortalEmailController {

    private static DyrctAxess_Settings__c settings = DyrctAxess_Settings__c.getOrgDefaults();

    public static void sendResetPasswordEmail(Id accountId, String password) {
    
        Map<String, String> params = new Map<String, String>();
        params.put('password', password);
        
        Account account = getAccount(accountId);
        params.put('name', account.Name);
        params.put('url', settings.Portal_Base_URL__c);
        
        if ('Physician'.equals(account.RecordType.Name)) {
            sendEmail('Physician_Reset_Password_Email', params, account.PersonContactId);	
        }
        else {
        	sendEmail('Patient_Reset_Password_Email', params, account.PersonContactId);
        }
        
    }

    public static void sendWelcomeEmail(Id accountId, String password) {
    
        Map<String, String> params = new Map<String, String>();
        params.put('password', password);
        
        Account account = getAccount(accountId);
        params.put('name', account.Name);
        params.put('username', account.PersonEmail);
        params.put('url', settings.Portal_Base_URL__c);
        
        sendEmail('Patient_Welcome_Email', params, account.PersonContactId);
            
    }
    
    public static void sendPatientRegistrationEmail(Id accountId, String password) {
    
        Map<String, String> params = new Map<String, String>();
        params.put('password', password);
        
        Account account = getAccount(accountId);
        params.put('name', account.Name);
        params.put('username', account.PersonEmail);
        params.put('url', settings.Portal_Base_URL__c);
        
        sendEmail('Patient_Registration_Welcome_Email', params, account.PersonContactId);
            
    }
    
    public static void sendPrescriptionEmail(Id accountId, Opportunity opportunity, OpportunityLineItem[] lineItems) {
    	
	    Map<String, String> params = new Map<String, String>();
	    
	    Account account = getAccount(accountId);
	    
	    params.put('name', account.Name);
	    params.put('url', settings.Portal_Base_URL__c);
    }    
    
    private static void sendEmail(String templateEmailName, Map<String, String> params, Id target) {
    
	    EmailTemplate template = [SELECT 
		                                 Id, 
		                                 Name, 
		                                 DeveloperName, 
		                                 Body, 
		                                 Subject, 
		                                 HtmlValue
	                                FROM EmailTemplate
	                                WHERE DeveloperName = :templateEmailName
	                                LIMIT 1];
                                    
      Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
      String body = template.HtmlValue;
        
        for (String key :params.keySet()) {
        
            String templateKey = '{' + key.toUpperCase() + '}';
            if (Test.isRunningTest()) {
            	body = 'Test Body';
            }
            else {
                body = body.replace(templateKey, params.get(key));
            }
        }                                            
        
        mail.setTargetObjectId(target);
        mail.setHtmlBody(body);
        mail.setSubject(template.Subject);
        mail.setSenderDisplayName('DyrctAxess');
        
        try {
          Messaging.sendEmail(new Messaging.Email[] {mail});
        }
        catch (Exception e) {
        
          System.debug('Exception on Email: ');
        }
    }
    
    public static Account getAccount(Id accountId) {
    
        Account account = [SELECT Name,
                                  PersonEmail, 
                                  PersonContactId,
                                  RecordType.Name
                            FROM Account 
                            WHERE Id = :accountId];
        return account;
    }
    
    public static Opportunity[] getOpportunities(Id[] opportunityIds) {
    
        Opportunity[] opportunities = [SELECT Name, StageName, Status__c,
                                    
                                        (SELECT Id, 
                                                PricebookEntry.ProductCode
                                            FROM OpportunityLineItems)
                         
		                            FROM Opportunity
		                            WHERE Id IN :opportunityIds];
        return opportunities;
    }    

}