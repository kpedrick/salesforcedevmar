/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public virtual class HVRequest {
	
	public final static String APP_ID = DyrctAxess_Settings__c.getOrgDefaults().Application_Id__c;
	
	protected final String REQUEST_BEGIN = '<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">';
	protected final String REQUEST_END = '</wc-request:request>';
	protected HVRequestHeader header;
	protected HVRequestInfo info;
	
	public override String toString(){
		return getRequestBody();
	}
	
	public void setHeader(HVRequestHeader header) {
	   this.header = header;
	}
	
    public void setInfo(HVRequestInfo info) {
       this.info = info;
    }	
    
    public virtual String getRequestBody() {
    	
        return getRequestBody('');
    }

    public virtual String getRequestBody(String authSection) {
            
        return getRequestBody(authSection, '');
    }

    public virtual String getRequestBody(String authSection, String extraHeader) {
            
        return getRequestBody(authSection, extraHeader, '');
    }
	
    public virtual String getRequestBody(String authSection, String extraHeader, String extraInfo) {
    
        String result = '';
        result += REQUEST_BEGIN;
        result += authSection;
        result += header.getBasicHeader(extraHeader);
        result += info.getInfo(extraInfo);
        result += REQUEST_END;
        system.debug('request body: ' + result);
        return result;
    }	
		
    public static String getDatetimeFormat() {
        
        Datetime result = Datetime.now();
        String dt = result.format('yyyy-MM-dd') + 'T' + result.format('HH:mm:ss');
        return dt;
    }		
		
	public class HVRequestHeader {
		
	    private String method;
	    private String version;
	    private String appId;
	    private String stamp;
	    
	    public HVRequestHeader(String method, String version) {
	       this.method = method;
	       this.version = version;
	       this.appid = APP_ID;
	    }
	    
        public HVRequestHeader(String method, String version, String appId) {
           this.method = method;
           this.version = version;
           this.appId = appId;
           system.debug('RequestHeader: ' + appId);
        }
        
        	
	    public String getBasicHeader() {
	        return getBasicHeader('');
	    }
	    
	    public virtual String getBasicHeader(String extra) {
	        return getBasicHeader(extra, null);
	    }	
	    
        public virtual String getBasicHeader(String extra, String authToken) {
            String result = '';
            result += '<header>';
            result += '<method>' + method + '</method>';
            result += '<method-version>' + version + '</method-version>';
            
            if (authToken != null) {
                result += authToken;
            }
            
            if (appId != null) {
               result += '<app-id>' + appId + '</app-id>';
            }
              
            result += '<language>en</language>';
            result += '<country>US</country>';
            result += '<msg-time>' + HVRequest.getDatetimeFormat() + '</msg-time>';
            result += '<msg-ttl>1800</msg-ttl>';
            result += '<version>2.0.0.0</version>';
            result += '{EXTRA}';
            result += '</header>';
            return result.replace('{EXTRA}', extra);
        }	    
	   	
	}
	
    public class HVRequestInfo {
        
	    public String getInfo() {
	       return getInfo('');
	    }
	    
	    public virtual String getInfo(String extra) {
	    
	       String result = '';
	       result += '<info>';
	       result += '{EXTRA}';
	       result += '</info>';
	       return result.replace('{EXTRA}', extra);
	    }
    
    }	

}