/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping='/HealthVault/Connection/*')
global class HVSConnectionWebService {

    @HttpGet
    global static ConnectionWebServiceResponse getHealthVaultUrl() {
    
        RestRequest req = RestContext.request;

        try {
            
            ConnectionWebServiceResponse result = new ConnectionWebServiceResponse(execute(req.requestURI.substringAfterLast('/')));
            system.debug('result: ' + result);
            return result;
        }
        catch (Exception e) {}
    
        return null;
    } 
 
    public static String execute(String contactId) {
        
        HVConnectionController controller = new HVConnectionController(Id.valueOf(contactId));
        return controller.execute();
    }
    
    global class ConnectionWebServiceResponse {
    
        public String url;
        
        public ConnectionWebServiceResponse(String url) {
            this.url = url;
        }
    }
    
}