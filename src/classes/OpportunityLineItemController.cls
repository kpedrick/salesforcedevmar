/*
Name              : OpportunityLineItemController
Revision History  :-
Created/Modified by       Created/Modified Date     Requested by              Reference             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/12/2014              Peter Clifford            https://na5.salesforce.com/a0P7000000CzZM8EAN
*/

public with sharing class OpportunityLineItemController {

  private Apexpages.Standardcontroller std;
  private OpportunityLineItem lineItem;
  private Id productId;
  
  

  public OpportunityLineItemController(Apexpages.Standardcontroller std) {
  
    this.std = std;
    lineItem = (OpportunityLineItem) std.getRecord();
    
    OpportunityLineItem aux = [SELECT 
                                  Product2Id 
                                FROM OpportunityLineItem 
                                WHERE Id = :lineItem.Id];
    
    productId = aux.Product2Id;
  }


  public Selectoption[] getOptions() {
  
    Selectoption[] result = new Selectoption[] {};
    
    result.add(new Selectoption('', '- none -'));
  
    Dosage__c[] dosages = [SELECT Id,
                                 Name, 
                                 Product__c, 
                                 Value__c, 
                                 Unit__c 
                             FROM Dosage__c
                             WHERE Product__c = :productId];
      
	  for (Dosage__c dosage :dosages) {
	    
	    String value = String.valueOf(dosage.Value__c) + ' ' + dosage.Unit__c;
	    
	    result.add(new Selectoption(dosage.Id, value));
	  }
      
    return result;
  }



}