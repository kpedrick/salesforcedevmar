@isTest
private class RFOpportunityShippingUpdateTriggerTest {
    
    @isTest(seeAllData=true)
    static void rfOpportunityShippingUpdateTriggerTest() {
    	
    	Account patient = TestUtils.createAccount('physician');
        Opportunity opportunity = TestUtils.createPrescription(patient.Id);
        
        Test.startTest();
        
        Product2 prod = TestUtils.createProduct('prod 1');
        OpportunityLineItem oli = TestUtils.createOppLineItem(opportunity.Id, prod.Id, 1);
  
        
        //Test complete invenotries when shipped
        opportunity.Status__c = 'Shipped';
        update opportunity;
        
        system.assertEquals(true,[Select Id,Status__c 
        						  From Inventory__c 
        						  Where Prescription_Line_Item_Id__c = : oli.Id limit 1].Status__c == 'Complete');
        
        //Test cancelled invenotries
        opportunity.Status__c = 'Canceled-Pharmacy';
        update opportunity;
        
        system.assertEquals(true,[Select Id,Status__c 
        						  From Inventory__c 
        						  Where Prescription_Line_Item_Id__c = : oli.Id limit 1].Status__c == 'Cancelled');
        Test.stopTest();
        
    	
    	
    }
}