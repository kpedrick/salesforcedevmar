@isTest
private class VirtualInventoryManagementControllerTest {

    @isTest(seeAllData=true) static void VirtualInventoryManagementControllerTest() {
        // TO DO: implement unit test
        
        Account physician = TestUtils.createAccount('physician');
        
        Account patient = TestUtils.createAccount('patient');
        patient.Physician__c = physician.Id;
        update patient;
        
        Opportunity opportunity = TestUtils.createPrescription(patient.Id);
        Product2 prod = TestUtils.createProduct('prod 1');
        OpportunityLineItem oli = TestUtils.createOppLineItem(opportunity.Id, prod.Id, 1);
        oli.Quantity = 4;
        oli.Days__c = '30';
        update oli;
        
        opportunity.Status__c = 'Shipped';
        update opportunity;
        
        test.startTest();
        ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(prod);
        VirtualInventoryManagementController controller = new VirtualInventoryManagementController(stdCon);
        
        test.stopTest();
    }
}