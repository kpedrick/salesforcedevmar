public class UniqueIdSynchronizer extends TriggerSupport {
	public recordtype patientType;
	static Boolean executed = false;
	public UniqueIdSynchronizer(sObject[] sObjectOldList, sObject[] sObjectNewList) {
       super(sObjectOldList, sObjectNewList);
       patientType = [select id  
                        from recordtype 
                       where SobjectType = 'Account' 
                        and  DeveloperName = 'Patient'];
    }
    
    public override Boolean executable(sObject sObjectOld, sObject sObjectNew){
    	if(executed){
    		return false;
    	}
    	
    	if(patientType.id != sObjectNew.get('RecordTypeId')){
    		return false;
    	}
    	
    	if(sObjectOld == null){
    		return true;
    	}
    	
    	
    	return sObjectOld.get('Salutation') != sObjectNew.get('Salutation') || 
    		   sObjectOld.get('FirstName') != sObjectNew.get('FirstName') || 
    		   sObjectOld.get('LastName') != sObjectNew.get('LastName');
    }
    
    public override void execute(sObject[] sObjectList, Boolean forceUpdate){
    	executed = true;
    	Account[] accounts = sObjectList;
    		
    	if(forceUpdate){
    	 	accounts = [select Salutation,
    								 FirstName,
    								 LastName,
    								 Unique_Identifier__c,
    								 Salutation__c,
    								 First_Name__c,
    								 Last_Name__c
    							from Account 
    						   where id = :sobjectList ];
    	}
    						   
    	for(Account o : accounts){
    		//shuffle
    		if( (o.Salutation != null && o.Salutation != '') && o.Salutation != o.Salutation__c){
    			o.Salutation__c = o.Salutation;	
    		}
    		if((o.FirstName != null && o.FirstName != '') && o.FirstName != o.First_Name__c){
    			o.First_Name__c = o.FirstName;	
    		}
    		if(o.LastName != o.Unique_Identifier__c){
    			o.Last_Name__c = o.LastName;
    			o.LastName = o.Unique_Identifier__c;
    		}
    		//reset
    		o.Salutation = '';
    		o.FirstName = '';
    	}
    	
    	if(forceUpdate){
    		update accounts;
    	}
    }
}