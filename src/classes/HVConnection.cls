/**************************************************\
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVConnection {

    public static String APP_ID = DyrctAxess_Settings__c.getOrgDefaults().Application_Id__c;//'831f505f-9bd7-4d96-a123-2e4c4a1e70e3';
    private static String WILDCAT_ENDPOINT = DyrctAxess_Settings__c.getOrgDefaults().Health_Vault_Endpoint__c; //'https://platform.healthvault-ppe.com/platform/wildcat.ashx';
    
    public static HVResponse sendMethodRequest(String body) {
        return sendMethodRequest(body, 'POST');
    }    
    
    public static HVResponse sendMethodRequest(String body, String method) {
    
        System.debug('REQUEST BODY ' + body);
    	System.debug('app id: ' + APP_ID);
        HVResponse hvr = null;
    
        Httprequest request = new Httprequest();
        request.setMethod(method.toUpperCase());
        request.setBody(body);
        request.setEndpoint(WILDCAT_ENDPOINT);
        
        HttpResponse response = null;
        
        response = Test.isRunningTest() ? getFakeResponse() : (new Http()).send(request);
        
        if (response.getStatusCode() == 200) {
            hvr = new HVResponse(response.getBody());
        }
        
        return hvr;
    }
    
    private static HttpResponse getFakeResponse() {
    
        Httpresponse response = new Httpresponse();
        response.setStatusCode(200);
        response.setBody('<?xml version="1.0" encoding="UTF-8"?><response><status><code>0</code></status><wc:info xmlns:wc="urn:com.microsoft.wc.methods.response.NewApplicationCreationInfo"><group max-full="1"><filter><type-id>30cafccc-047d-4288-94ef-643571f7919d</type-id><thing-state>Active</thing-state></filter><format><section>audits</section><section>core</section><type-version-format>30cafccc-047d-4288-94ef-643571f7919d</type-version-format></format></group><app-id>4417587e-46d1-49ee-a0fa-50dbfdcf932c</app-id><shared-secret>gBksYDAqcuAUXK+zyvfFW9F0sy8nchwnplJ6K7aKmAM=</shared-secret><app-token>AiAAANDl....A5b9fxVIlXEloouci6jGhY/A==</app-token></wc:info></response>');
        
        return response;
    }
}