/*************************************************************************
 * 
 * author: Eric Talley
 * company: Agate Group
 * purpose: Shuttle any changes from the account to related opportunities. The businessUnitMatch
 * class makes sure that the pricebook matches the business unit on the account.
 */
public class PrescriptionBusinessUnitAccount extends TriggerSupport{
	
	
	public PrescriptionBusinessUnitAccount(sObject[] sObjectOldList, sObject[] sObjectNewList) {
       super(sObjectOldList, sObjectNewList);
    }
    
    public override Boolean executable(sObject sObjectOld, sObject sObjectNew){
    	return getisDifferent(sObjectOld, sObjectNew, Schema.Account.Business_Unit__c);
    }
    
    public override void execute(sObject[] sObjectList, Boolean forceUpdate){
    	new PrescriptionBusinessUnitMatch(sObjectList,sObjectList).execute([select id from Opportunity where accountId = :sobjectList],true);
    }
}