/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@isTest(seeAllData=true)
public with sharing class PortalPhysicianRestServiceTest {

    static testMethod void testPhysicianRestServicePatient() {
    	
        PortalPhysicianRestService.PortalPhysicianRestServiceData result = null;
        
        Test.startTest();
        
        RestContext.request = TestUtils.getPhysicianParamsMap();
        result = PortalPhysicianRestService.doGet();
        
        Test.stopTest();
        
        System.assert(result.id != null);
        
        System.assertEquals(result.id, [SELECT Id FROM Account WHERE Id = :result.id].Id);    	
    }

}