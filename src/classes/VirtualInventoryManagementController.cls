/*
* @descp: Controller to manage virtual inventory per drug and physician
* @author: mauricio@theagategroup.com
* @version: 1.0
* @pre cond: Reports must to be created: 'Calculate Rate of Consumption Per Drug' - 'Inventories with Physician' - 'Inventories per Product Report'
*
*/


public with sharing class VirtualInventoryManagementController {
    
public Product2 product {get;set;}
public List<OpportunityLineItem> listOlis {get;set;}
public List<Inventory__c> listInv {get;set;}
public Boolean isInventorySum {get;set;}
public Inventory__c inventory {get;set;}

public List<ItemInv> listGeneralInventory {get;set;}
public List<ItemInv> listPhysicianInventory {get;set;}


//Map physician - resuppy date
public Map<Id,Date> mapPhysicianResupplyDate {get;set;}

public VirtualInventoryManagementController(ApexPages.StandardController stdCon){
 this.product = (Product2)stdCon.getRecord();
 this.isInventorySum = false;
 this.inventory = new Inventory__c();
 this.mapPhysicianResupplyDate = new Map<Id,Date>();
 
 this.listInv = [Select Id,
                        Product_Id__c,
                        Product_Id__r.Name,
                        Debit__c,
                        Credit__c,
                        Purchase_Date__c,
                        Status__c,
                        Prescription__c,
                        Type__c,
                        Dosage__c,
                        //days__c,
                        Physician__c,
                        Physician__r.Name,
                        Patient__r.Name
                From Inventory__c 
                Where Product_Id__c = :this.product.Id 
                AND Physician__c != null 
                And Patient__c != null 
                AND Status__c = 'Complete'
                //AND Supply_Ending_Date__c > TODAY
               order by Physician__r.Name];
               
 system.debug(this.listInv);
 
 this.listOlis = [Select Id,OpportunityId,Shipping_Datetime__c 
                  From OpportunityLineItem 
                  Where Product2Id = :this.product.Id];
 
 //Fill inner list based on Report for General and Physician inventory                
 this.listGeneralInventory = this.calculateInventoriesPerProduct();         
 
 //fill consumption rate for each drug based on (Calculate Rate of Consumption Per Drug) Report
 this.calculateRateOfConsumptionPerDrug(this.listGeneralInventory);
 
 
 //Calculate rate of consumption for each patient and estiamted date resupply for each physician  
 this.listPhysicianInventory = this.calculateConsumptionAndEstimatedDate(this.listInv); 
                  
 system.debug('this.listPhysicianInventory -> '+ this.listPhysicianInventory);
 
 
 
}


public PageReference backToProduct(){
    return new PageReference('/'+ this.product.Id);
}


public List<ItemInv> calculateConsumptionAndEstimatedDate(List<Inventory__c> listInventories){
    List<ItemInv> listItemInvPerPhysician = new List<ItemInv>();
    ItemInv inv;
    
    
    system.debug('listInventories -> '+ listInventories);
    
    for(Inventory__c inventory : listInventories){
       inv = new ItemInv();
       inv.inventoryId = inventory.Id;
       inv.dosage = inventory.Dosage__c;
       inv.physicianName = inventory.Physician__r.Name;
       inv.patientName = inventory.Patient__r.Name;
       inv.productName = inventory.Product_ID__r.Name;
       //inv.consumptionRate = ((inventory.Credit__c / inventory.Days__c) * 7).setScale(2);
       
       //calculate estimated resupply date per physician
       //Inventories with Physician - Re Supply end date
       if(this.mapPhysicianResupplyDate.get(inventory.Physician__c) != null){
         inv.reSupplyDate =this.mapPhysicianResupplyDate.get(inventory.Physician__c);
       }else{
         inv.reSupplyDate = this.calculateReSupplyDatePerPhysician(inventory.Physician__c,inventory.Dosage__c);
       }
       listItemInvPerPhysician.add(inv);
    }
    
    return listItemInvPerPhysician;
}

/**
* @descp: Method to pull data from 'Inventories with Physician' report
*
**/

public Date calculateReSupplyDatePerPhysician(String physicianId,String dosage){
    
     List<Report> lstReport = [Select Id From Report Where Name ='Inventories with Physician' limit 1];
     
     //Create all variables to a Report
     Reports.ReportResults res = Reports.ReportManager.runReport(lstReport.get(0).Id,true);
     Reports.Dimension groupingsDown = res.getGroupingsDown();
     Map<String, Reports.ReportFact> factMap = res.getFactMap(); 
     
      //Get Formula Columns values from the report given
     Integer formula1Idx,formula2Idx,rowCountIdx;
     List<String> aggColNames = res.getReportMetadata().getAggregates();
     for (Integer i = 0, cnt = aggColNames.size(); i < cnt; i++) {
        String aggColName = aggColNames.get(i);
        if (aggColName == 'FORMULA1') {
            formula1Idx = i;
        }else if (aggColName == 'FORMULA2') {
            formula2Idx = i;
        }else if (aggColName == 'RowCount') {
            rowCountIdx = i;
        }
     }
     
      //Pull Consumption rate per Dosage per product/ Based on each row from list of Inner items
     for (Reports.GroupingValue groupPhysicianName : groupingsDown.getGroupings()) {
        if((Id) groupPhysicianName.getvalue() == physicianId){
            for (Reports.GroupingValue groupDosage: groupPhysicianName.getGroupings()) {
                if(dosage == (String)groupDosage.getvalue()){
                    
                    Reports.ReportFact fact = factMap.get(groupDosage.getKey() + '!T');
                    Decimal generalInventoryPerPhysician =   (Decimal) fact.getAggregates().get(formula1Idx).getValue();
                    Decimal consumptionRatePerPhysician =   (Decimal) fact.getAggregates().get(formula2Idx).getValue();
                    
                    Integer resupplyDays = Integer.valueOf((generalInventoryPerPhysician / consumptionRatePerPhysician) * 7 );
                    Date resupplyDate = Date.today().addDays(resupplyDays);
                    
                    //Add to map and return date 
                    this.mapPhysicianResupplyDate.put(inventory.Physician__c,resupplyDate);
                    return resupplyDate;
                   /*Reports.ReportFact fact = factMap.get(groupDosage.getKey() + '!T');
                     Decimal generalInventoryPerPhysician =   (Decimal) fact.getAggregates().get(formula1Idx).getValue();
                    
                   //Iter on each inventory for the Product & Dosage related - we'll get values to count total Inventory for Physician 
                    Reports.ReportFactWithDetails factWithDetails = (Reports.ReportFactWithDetails) factMap.get(groupDosage.getKey() + '!T');
                    
                    //Calculate Physician inventory level of each dosage per product related (as report grouped)
                    //if physician field is not null will sum debit or credit related
                    for (Reports.ReportDetailRow row : factWithDetails.getRows()) {
                        Integer cont = 0;
                        isCredit = false;
                        for (Reports.ReportDataCell cell : row.getDataCells()) {
                            if(cont == 0){
                                if(cell.getValue() != null){
                                cont += 1;continue;
                                }else{
                                    break;
                                }
                            }
                            
                            if(cont == 3){
                                if(cell.getValue() != null){
                                        cont += 1;continue;
                                }
                            }
                        }
                     }*/
                 
                }
            }
        }
     }
    
    return null;
}


/**
* @descp: Method to pull data from 'Calculate Rate of Consumption Per Drug' report filtered by standard product.
*          
* @precond: ICalculate Rate of Consumption Per Drug report's must to be created on the org - with one filtered: Consumption rate per week
* Order of columns: Opportunity Name,Prescription Shipped Date, Quantity, Administration, Days, Supply end date
*
**/

public void calculateRateOfConsumptionPerDrug(List<ItemInv> listInv){
    
     List<Report> lstReport = [Select Id From Report Where Name ='Calculate Rate of Consumption Per Drug' limit 1];
     //Create all variables to a Report
     Reports.ReportMetadata options = getReportOptionsConsumptionRate(this.product.name);
     Reports.ReportResults res = Reports.ReportManager.runReport(lstReport.get(0).Id,options);
     Reports.Dimension groupingsDown = res.getGroupingsDown();
     Map<String, Reports.ReportFact> factMap = res.getFactMap(); 
     
      //Get Formula Columns values from the report given
     Integer formula1Idx,rowCountIdx;
     List<String> aggColNames = res.getReportMetadata().getAggregates();
     for (Integer i = 0, cnt = aggColNames.size(); i < cnt; i++) {
        String aggColName = aggColNames.get(i);
        if (aggColName == 'FORMULA1') {
            formula1Idx = i;
        }else if (aggColName == 'RowCount') {
            rowCountIdx = i;
        }
     }
     
     //Pull Consumption rate per Dosage per product/ Based on each row from list of Inner items
     for (Reports.GroupingValue groupProductName : groupingsDown.getGroupings()) {
        for(ItemInv inv : listInv){
            for (Reports.GroupingValue groupDosage: groupProductName.getGroupings()) {
                    if(inv.dosage == (String)groupDosage.getvalue()){
                        Reports.ReportFact fact = factMap.get(groupDosage.getKey() + '!T');
                        inv.consumptionRate =  ((Decimal) fact.getAggregates().get(formula1Idx).getValue()).setScale(2);
                        break;
                    }
            }
            
            //calculate estimated re supply date based on Consumption Rate per Drug
            if(inv.consumptionRate != 0 && inv.consumptionRate != null ){
                if(inv.PhysicianInventory > 0){
                    inv.reSupplyDate = date.today().addDays(Integer.valueOf((inv.physicianInventory / inv.consumptionRate) * 7)) ;
                }
            }
        }
     }
    
}


/**
* @descp: Method to pull data from 'Inventories per product report' filtered by standard product.
          It will pull general inventory (sum of total inventory for the product) and Physician inventory (inventory that is related to a physician)
*          
* @precond: Inventories per product report's must to be created on the org - With 2 filteres inventory level & inventory dosage level 
* Order of columns: Status,Physician,Prescription,Debit,Credit,Inventory Level per dosage, Inventory Level
*
* @comments: Maybe FORMULA2 and FORMULA3 have to be changed on Production to Formula1 and Formula2
**/

public List<ItemInv> calculateInventoriesPerProduct(){
    
     List<Report> lstReport = [Select Id From Report Where Name ='Inventories per Product Report' limit 1];
     //Create all variables to a Report
     Reports.ReportMetadata options = getReportOptionsInventoriesPerProduct(this.product.id);
     Reports.ReportResults res = Reports.ReportManager.runReport(lstReport.get(0).Id,options,true);
     Reports.Dimension groupingsDown = res.getGroupingsDown();
     Map<String, Reports.ReportFact> factMap = res.getFactMap(); 
     
     //Get Formula Columns values from the report given
     Integer formula2Idx,formula3Idx, rowCountIdx;
     List<String> aggColNames = res.getReportMetadata().getAggregates();
     for (Integer i = 0, cnt = aggColNames.size(); i < cnt; i++) {
        String aggColName = aggColNames.get(i);
        if (aggColName == 'FORMULA2') {
            formula2Idx = i;
        }else if (aggColName == 'FORMULA3') {
            formula3Idx = i;
        }else if (aggColName == 'RowCount') {
            rowCountIdx = i;
        }
     }
     
     //List of inventory general to show
     List<ItemInv> listGeneralInv = new List<ItemInv>();
     //Iter on Agregattion groups then rows of each dosage 
      for (Reports.GroupingValue groupProductName : groupingsDown.getGroupings()) {
            String productName = (String) groupProductName.getLabel();
            ItemInv itemInv;
            for (Reports.GroupingValue groupDosage: groupProductName.getGroupings()) {
                
                //populate each inventory with dosage
                itemInv = new ItemInv();
                itemInv.productName = productName;
                itemInv.dosage = (String)groupDosage.getvalue();
                
                Reports.ReportFact fact = factMap.get(groupDosage.getKey() + '!T');
                itemInv.generalInventory =   (Decimal) fact.getAggregates().get(formula2Idx).getValue();
                
               //Iter on each inventory for the Product & Dosage related - we'll get values to count total Inventory for Physician 
                Reports.ReportFactWithDetails factWithDetails = (Reports.ReportFactWithDetails) factMap.get(groupDosage.getKey() + '!T');
                Boolean isCredit = false;
                
                //Calculate Physician inventory level of each dosage per product related (as report grouped)
                //if physician field is not null will sum debit or credit related
                for (Reports.ReportDetailRow row : factWithDetails.getRows()) {
                    Integer cont = 0;
                    isCredit = false;
                    for (Reports.ReportDataCell cell : row.getDataCells()) {
                        if(cont == 1){
                            if(cell.getValue() != null){
                            cont += 1;continue;
                            }else{
                                break;
                            }
                        }
                        if(cont == 2 && cell.getValue() != null){
                                cont += 1;
                                isCredit = true; continue;
                            
                        }
                        if(cont == 3 && cell.getValue() != null && !isCredit ){
                                itemInv.physicianInventory += (Double) cell.getValue();
                                break;
                        }
                        if(cont == 4 && isCredit && cell.getValue() != null){
                                itemInv.physicianInventory -= (Double) cell.getValue();
                                break;
                        }
                       cont += 1;   
                    }
                } 
                listGeneralInv.add(itemInv);
            }
      }
      system.debug(listGeneralInv);
    return listGeneralInv;
}


/**
* @descp: method to add filters to the report on System.ReportsManager for Calculate Rate of Consumption Per Drug
*  There is a filter on Supply end Date greater than today .
*
**/
 public static Reports.ReportMetadata getReportOptionsConsumptionRate(String productName) {
        List<Reports.ReportFilter> reportFilters = new List<Reports.ReportFilter>();
        Reports.ReportFilter productFilter = new Reports.ReportFilter();
        
        productFilter.setColumn('NAME');
        productFilter.setOperator('equals');
        productFilter.setValue(productName);
        reportFilters.add(productFilter);
        
      /*  Reports.ReportFilter dosageFilter = new Reports.ReportFilter();
        dosageFilter.setColumn('OpportunityLineItem.Dosage_Value__c');
        dosageFilter.setOperator('equals');
        dosageFilter.setValue(dosageValue);
        reportFilters.add(dosageFilter);*/
        
        Reports.ReportFilter supplyDateFilter = new Reports.ReportFilter();
        supplyDateFilter.setColumn('OpportunityLineItem.Supply_Ending_Date__c');
        supplyDateFilter.setOperator('greaterThan');
        supplyDateFilter.setValue(string.valueOf(date.today()));
        reportFilters.add(supplyDateFilter);
        
        Reports.ReportMetadata opts = new Reports.ReportMetadata();
        opts.setReportFilters(reportFilters);
        return opts;
    }




/**
* @descp: method to add filters to the report on System.ReportsManager for Inventories per product
*
**/
 public static Reports.ReportMetadata getReportOptionsInventoriesPerProduct(String productId) {
        List<Reports.ReportFilter> reportFilters = new List<Reports.ReportFilter>();
        Reports.ReportFilter productFilter = new Reports.ReportFilter();
        productFilter.setColumn('FK_PROD_ID');
        productFilter.setOperator('equals');
        productFilter.setValue(productId);
        reportFilters.add(productFilter);
        
        Reports.ReportFilter stageFilter = new Reports.ReportFilter();
        stageFilter.setColumn('Inventory__c.Status__c');
        stageFilter.setOperator('equals');
        stageFilter.setValue('Complete');
        reportFilters.add(stageFilter);
        
        Reports.ReportMetadata opts = new Reports.ReportMetadata();
        opts.setReportFilters(reportFilters);
        return opts;
    }
    

  
  public class ItemInv {
    
    public String inventoryId {get;set;}
    public String physicianName {get;set;}
    public String patientName {get;set;}
    public String productName {get;set;}
    public String dosage {get;set;}
    public Double generalInventory {get;set;}
    public Double physicianInventory {get;set;}
    public Date reSupplyDate {get;set;}
    public Decimal consumptionRate {get;set;}
    
    
    public ItemInv(){
        reSupplyDate = date.today();
        consumptionRate = 0;
        physicianInventory = 0;
    }
  
  }

}