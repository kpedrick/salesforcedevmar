/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVCreateAuthenticatedSessionToken extends HVRequest {
	
	private String childAppId;
	private String sharedSecret;
	
    public HVCreateAuthenticatedSessionToken(String childAppId, String sharedSecret) {

        this.header = new HVRequestHeader('CreateAuthenticatedSessionToken','2', childAppId);
        this.info = new HVRequestInfo();
        this.childAppId = childAppId;
        this.sharedSecret = sharedSecret;
    }
    
    public override String getRequestBody() {
    
        String result = '';
        result += REQUEST_BEGIN;
        result += header.getBasicHeader();
        result += info.getInfo(getAuthInfo());
        result += REQUEST_END;
        
        return result;
    }
	
    private String getAuthInfo() {
    
        String content = getContent();
    
        String result = '';
        result += '<auth-info>';
        result += '<app-id>' + childAppId + '</app-id>';
        result += '<credential>';
        result += '<appserver2>';
        
        result += '<hmacSig algName="HMACSHA256">' + getHMACSHA256Encrypt(content) + '</hmacSig>';
        
        result += content;
        
        result += '</appserver2>';
        result += '</credential>';
        result += '</auth-info>';
        return result;
    }
    
    public String getContent() {
    
        String result = '';
        result += '<content>';
        result += '<app-id>' + childAppId + '</app-id>';
        result += '<hmac>HMACSHA256</hmac>';
        result += '<signing-time>' + HVRequest.getDatetimeFormat() + '</signing-time>';
        result += '</content>';    
        
        System.debug('CONTENT: ' + result);
        return result;
    }
    
    private String getHMACSHA256Encrypt(String body) {
    	
    	String localSharedSecret = Test.isRunningTest() ? '1234567890' : sharedSecret; 
    	
    	Blob ss = Encodingutil.base64Decode(localSharedSecret);
        String result = Encodingutil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(body), ss));
        
        return result;
    }

}