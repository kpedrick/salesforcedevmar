/*************************************************************************
 * 
 * author: Eric Talley
 * company: Agate Group
 * purpose: provides a baseline approach for dealing with trigger logic
 */
public abstract class TriggerSupport {
    public sObject[] sObjectOldList { get; set; }
    public sObject[] sObjectNewList { get; set; }
    
    public Map<Id, sObject> sObjectOldListMap = new Map<Id, sObject>();
    public Map<Id, sObject> sObjectNewListMap = new Map<Id, sObject>();
     
    public TriggerSupport(sObject[] sObjectOldList, sObject[] sObjectNewList) {
        this.sObjectOldList = sObjectOldList == null ? new sObject[] {} : sObjectOldList;
        this.sObjectNewList = sObjectNewList == null ? new sObject[] {} : sObjectNewList;
        
        initializeMap(this.sObjectOldList, sObjectOldListMap);
        initializeMap(this.sObjectNewList, sObjectNewListMap);
    }
    
    public void initializeMap(sObject[] sObjectList, Map<Id, sObject> sObjectListMap) {
        for(sObject sObjectRecord : sObjectList) {
            if (sObjectRecord.Id != null) {
                sObjectListMap.put(sObjectRecord.Id, sObjectRecord);
            }
        }
    }
    
    public abstract Boolean executable(sObject sObjectOld, sObject sObjectNew);
    
    public virtual void execute() {        
        sObject[] sObjectUpdateableList = new sObject[] {};      
     
        for(sObject sObjectNew : sObjectNewList) {
            sObject sObjectOld = sObjectOldListMap.get(sObjectNew.Id);
            sObjectOld = sObjectOld == null ? null : sObjectOld;	            
            if (executable(sObjectOld, sObjectNew)) {
                sObjectUpdateableList.add(sObjectNew);
            }
        }       
        if (sObjectUpdateableList.size() != 0) {
            execute(sObjectUpdateableList, trigger.IsAfter == true);
        }

    }
    
    public virtual void execute(sObject[] sObjectList, Boolean forceUpdate) {}
    
    public Boolean getIsDifferent(sObject sObjectOld, sObject sObjectNew, Schema.SobjectField sobjectField) {
    	if(sObjectOld == null || sObjectNew == null)
    		return true;
    	String field = sobjectField.getDescribe().getName();
        return sObjectOld.get(field) != sObjectNew.get(field);
    }
    
    
}