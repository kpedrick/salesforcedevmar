public class OppLineItemInventoryTrigger extends SObjectTrigger {
	
	public OppLineItemInventoryTrigger(sObject[] sObjectOldList, sObject[] sObjectNewList){
		super(sObjectOldList,sObjectNewList);
	}
    
    public override boolean executable(sObject sObjectOld, sObject sObjectNew){
		return true;
	}

	public override void execute(sObject[] sObjectList, Boolean forceUpdate){
		
		set<Id> opportunityLineItemIds = new set<Id>();
		Map<Id, Inventory__c> oliIdInventoryMap = new Map<Id, Inventory__c>();
		
		List<Inventory__c> inventoryInsertList = new List<Inventory__c>();
		List<Inventory__c> inventoryUpdateList = new List<Inventory__c>();
		List<Inventory__c> inventoryDeleteList = new List<Inventory__c>();
		
		for (sObject obj :sObjectList){
			opportunityLineItemIds.add((Id)obj.get('Id'));
		}
		
		List<OpportunityLineItem> opportunityLineItemList = [SELECT Id,
																	Product2Id,
																	OpportunityId,
																	Opportunity.Account.Physician__c,
																	Opportunity.AccountId,
																	Opportunity.Status__c,
																	Quantity,
																	Days__c,
																	Dosage__r.Display_Value__c,
																	Shipping_Datetime__c
															 FROM OpportunityLineItem
															 WHERE Id IN :opportunityLineItemIds];

		List<Inventory__c> inventoryList = [SELECT Id,
												   Credit__c,
												   Product_ID__c,
												   Status__c,
												   Opportunity__c,
												   Physician__c,
												   Patient__c,
												   Dosage__c,
												   Days__c,
												   Prescription__c,
												   Purchase_Date__c,
												   Prescription_Line_Item_Id__c
											FROM Inventory__c
											WHERE Prescription_Line_Item_Id__c IN :opportunityLineItemIds];
		
		
		for (Inventory__c inv :inventoryList){
			if (inv.Prescription_Line_Item_Id__c == null)
				continue;
				
			oliIdInventoryMap.put(inv.Prescription_Line_Item_Id__c, inv);
		}
		
		for (OpportunityLineItem oli : opportunityLineItemList){
			
			Date tempdate = (oli.Shipping_Datetime__c == null)? null : oli.Shipping_Datetime__c.date();
			
			if (oliIdInventoryMap.containsKey(oli.Id) == false){
				
				inventoryInsertList.add(new Inventory__c(
														Product_ID__c = oli.Product2Id,
														Status__c = OpportunityInventoryStatusUtil.getInventoryStatus(oli.Opportunity.Status__c),
														Opportunity__c = oli.OpportunityId,
														Physician__c = oli.Opportunity.Account.Physician__c,
														Patient__c = oli.Opportunity.AccountId,
														Credit__c = oli.Quantity,
														Dosage__c = oli.Dosage__r.Display_Value__c,
														Days__c = oli.Days__c,														
														Prescription__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + oli.Id,
														Prescription_Line_Item_Id__c = oli.Id,
														Purchase_Date__c = tempdate
														));
			}else{
				
				Inventory__c inv = oliIdInventoryMap.get(oli.Id);
				
				if (inv.Product_ID__c != oli.Product2Id ||
					inv.Status__c != OpportunityInventoryStatusUtil.getInventoryStatus(oli.Opportunity.Status__c) ||
					inv.Opportunity__c != oli.OpportunityId ||
					inv.Physician__c != oli.Opportunity.Account.Physician__c ||
					inv.Patient__c != oli.Opportunity.AccountId ||
					inv.Credit__c != oli.Quantity ||
					inv.Dosage__c != oli.Dosage__r.Display_Value__c ||
					inv.Days__c != oli.Days__c ||
					inv.Prescription__c != URL.getSalesforceBaseUrl().toExternalForm() + '/' + oli.Id ||
					inv.Purchase_Date__c != tempdate
					){
						inv.Product_ID__c = oli.Product2Id;
						inv.Status__c = OpportunityInventoryStatusUtil.getInventoryStatus(oli.Opportunity.Status__c);
						inv.Opportunity__c = oli.OpportunityId;
						inv.Physician__c = oli.Opportunity.Account.Physician__c;
						inv.Patient__c = oli.Opportunity.AccountId;
						inv.Credit__c = oli.Quantity;
						inv.Dosage__c = oli.Dosage__r.Display_Value__c;
						inv.Days__c = oli.Days__c;
						inv.Prescription__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + oli.Id;
						inv.Purchase_Date__c = tempdate;
						inv.Internal_Unlock__c = true;
						inventoryUpdateList.add(inv);
					}
			}
		}
		
		if (Trigger.isDelete == true){
			inventoryDeleteList.addAll(inventoryList);
		}
		
		if (inventoryInsertList.isEmpty() == false){
			insert inventoryInsertList;
		}
		
		if (inventoryUpdateList.isEmpty() == false){
			update inventoryUpdateList;
		}
		
		if (inventoryDeleteList.isEmpty() == false){
			delete inventoryDeleteList;
		}
		
	}
	
}