@isTest
private class InsertInventoriesBasedOnOppLineItemTest {
    
    @isTest(seeAllData=true)
    static void InsertInventoriesBasedOnOppLineItemTest() {
            
        Account patient = TestUtils.createAccount('physician');
        Opportunity opportunity = TestUtils.createPrescription(patient.Id);
        
        Test.startTest();
        
        Product2 prod = TestUtils.createProduct('prod 1');
        OpportunityLineItem oli = TestUtils.createOppLineItem(opportunity.Id, prod.Id, 1);
        //test inventory created
        system.assertEquals(true, [Select Id From Inventory__c Where Prescription_Line_Item_Id__c = :oli.Id limit 1] != null); 
        
        //Test update quantity on Opp line item it will creat an inventory with the difference related 
        try{
        oli.Quantity = 4;
        update oli;
        system.assertEquals(true, [Select Id From Inventory__c Where Prescription_Line_Item_Id__c = :oli.Id AND Credit__c = 3 limit 1] != null);
        }catch(Exception e){
        }
        Test.stopTest();
      
    }
}