public class OpportunityRefill {
	
	public Opportunity orriginalOpp;
	
	public Integer refillNumber;
	
	public DyrctAxess_Settings__c dyrctAxessSettings;
	
	public Opportunity oppty;
	
	public List<OpportunityLineItem> opptyLines;
	
	
	public OpportunityRefill(Opportunity orriginalOpportunity, Integer refillNumber, DyrctAxess_Settings__c dyrctAxessSettings){
		this.dyrctAxessSettings = dyrctAxessSettings;
		this.refillNumber = refillNumber;
		this.orriginalOpp = orriginalOpportunity;
		this.opptyLines = new List<OpportunityLineItem>();
	}
	
	public void cloneOpportunity(Opportunity opp){
		
		this.oppty = opp.clone(false, true);
		this.oppty.CloseDate = Date.today();
		this.oppty.StageName = 'New';
		this.oppty.Status__c = 'Refill';
		this.oppty.Type = 'Refill Prescription';
		
		this.oppty.Refill_Number__c = this.refillNumber;
		
		this.oppty.Prescription_Opportunity__c = this.orriginalOpp.Id;
		
		// fields that should not be cloned
		this.oppty.Prescription_Shipped_Date__c = null;
		
	}
	
	public void cloneLines(List<OpportunityLineItem> lines, Date dateToday, DateTime ShippingDate){
		
		String refillCount = '';
		for (OpportunityLineItem line: lines){
			
			if (line.Days__c == null)
				continue;
				
			if (line.Days__c.isNumeric() == false)
				continue;
				
			Date tempDate = Date.newInstance(ShippingDate.year(), ShippingDate.month(), ShippingDate.day());
					
			Date processingDate = tempDate.addDays(Integer.valueOf(line.Days__c) - Integer.valueOf(dyrctAxessSettings.Notification_Email_Days__c));
			
			system.debug('processingDate -> '+ processingDate);
			
			
			if (processingDate > dateToday)
				continue;
			
			if (line.Refills__c.isNumeric() == false)
				continue;
				
			Integer refill = Integer.valueOf(line.Refills__c) - this.refillNumber;
			
			if (refill < 0)
				continue;
				
			refillCount = String.valueOf(this.refillNumber) +' of '+ line.Refills__c +', ';
			
			OpportunityLineItem newItem = line.clone(false, true);
			newItem.Total_Refills__c = Integer.valueOf(line.Refills__c);
			newItem.Refills__c = String.valueOf(refill);
			
			this.opptyLines.add(newItem);
		}
		
		if (refillCount != ''){
			refillCount = refillCount.subString(0, refillCount.length() - 2);
      	}
      	if (refillCount.length() > 255){
			refillCount = refillCount.subString(0, 255);
	  	}
	  	
	  	this.oppty.Refill_Count__c = refillCount;
		
	}
	
	public void addOppId(){
		for(OpportunityLineItem line : this.opptyLines){
			line.OpportunityId = this.oppty.Id;
		}
	}

}