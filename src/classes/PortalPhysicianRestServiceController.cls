/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class PortalPhysicianRestServiceController {

    private static PortalPhysicianRestServiceController instance = null;
    private Map<String, Schema.Recordtypeinfo> info;
    
    private PortalPhysicianRestServiceController() {
        info = Schema.SObjectType.Account.getRecordTypeInfosByName();
    }
    
    public static PortalPhysicianRestServiceController getInstance() {
    
        if (instance == null) {
            instance = new PortalPhysicianRestServiceController();
        }
        
        return instance;
    }
    
    public String createPhysician(Map<String, String> params) {
    
        if (validateParams(params)) {
        
            Id accountId = insertPhysician(params);
            if (accountId != null) {
                if (insertCredential(accountId, params) != null) {
                    return accountId;
                }                
            }
        }
        
        return null;
    }
    
    private Id insertPhysician(Map<String, String> params) {
    
        Account account = new Account();
        account.FirstName = params.get('args[FirstName]');
        account.LastName = params.get('args[LastName]');
        account.PersonEmail = params.get('args[Email]');
        account.Physician_NPI_DEA__c = params.get('args[Physician_NPI_DEA__c]');
        account.PersonMobilePhone = params.get('args[Phone]');
        account.RecordTypeId = info.get('Physician').getRecordTypeId();
        
        insert account;
        
        return account.Id;
    }
    
    private Id insertCredential(Id accountId, Map<String, String> params) {
    
        Portal_Credential__c credential = new Portal_Credential__c();
        credential.Application__c = 'DA';
        credential.Email__c = params.get('args[Email]');
        credential.Account__c = accountId;
        credential.Password__c = params.get('args[Password__c]');
	
	    insert credential;
	    return credential.Id;       
    }
        
    private Boolean validateParams(Map<String, String> params) {
    
        Boolean result = true;
        
        for (String value :params.keySet()) {
            system.debug(value + ' ** ' + params.get(value));
        }
        
        result &= params.containsKey('args[FirstName]') && (params.get('args[FirstName]') != null || params.get('args[FirstName]') != '');
        result &= params.containsKey('args[LastName]') && (params.get('args[LastName]') != null || params.get('args[LastName]') != '');
        result &= params.containsKey('args[Email]') && (params.get('args[Email]') != null || params.get('args[Email]') != '');
        //result &= params.containsKey('args[Phone]') && (params.get('args[Phone]') != null || params.get('args[Phone]') != '');
        result &= params.containsKey('args[Physician_NPI_DEA__c]') && (params.get('args[Physician_NPI_DEA__c]') != null || params.get('args[Physician_NPI_DEA__c]') != '');
        result &= params.containsKey('args[Password__c]') && (params.get('args[Password__c]') != null || params.get('args[Password__c]') != '');    
    
        System.debug('Validate Parameters ' + result);
    
        return result;
    }

}