/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVGetAuthorizedPeople extends HVRequest {

    private String childAppId;
    private String sharedSecret;
    private String token;

    public HVGetAuthorizedPeople(String childAppId, String sharedSecret, String token) {
    
        this.header = new HVRequestHeader('GetAuthorizedPeople','1', null);
        this.info = new HVRequestInfo();
        this.childAppId = childAppId;
        this.sharedSecret = sharedSecret;
        this.token = token;
    }
    
    public override String getRequestBody() {
    
        String headerValue = header.getBasicHeader(getInfoHash(), getAuthSession());
    
        String result = '';
        result += REQUEST_BEGIN;
        result += getAuth(headerValue);
        result += headerValue;
        result += info.getInfo(getContent());
        result += REQUEST_END;
        
        return result;
    }    
    
    private String getAuth(String headerValue) {
    
        String result = '';
        result += '<auth>';
        result += '<hmac-data algName="HMACSHA256">';
        result += getHMACSHA256Encrypt(headerValue);
        result += '</hmac-data>';
        result += '</auth>'; 
    
        return result;
    }
    
    private String getInfoHash() {
    
        String result = '';
        result += '<info-hash>';
        result += '<hash-data algName="SHA256">';
        result += getSHA256Hash(getInfoContent());
        result += '</hash-data>';
        result += '</info-hash>';
        
        return result;
    }
    
    private String getAuthSession() {
    
        String result = '';
        result += '<auth-session>';
        result += '<auth-token>';
        result += token;
        result += '</auth-token>';
        result += '</auth-session>';
            
        return result;
    }

    private String getInfoContent() {
        return '<info>' + getContent() + '</info>';    
    }
    
    private String getContent() {
        return '<parameters></parameters>';    
    }    
    
    private String getSHA256Hash(String body) {
        
        String result = Encodingutil.base64Encode(Crypto.generateDigest('SHA-256', Blob.valueOf(body)));
        
        return result;
    }        
    
    private String getHMACSHA256Encrypt(String body) {
        
        Blob ss = Encodingutil.base64Decode(sharedSecret);
        String result = Encodingutil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(body), ss));
        
        return result;
    }    

}