/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping = '/portal/prescription/*')
global with sharing class PortalPrescriptionRestService {

    public static Restrequest req = null;

    @httpGet
    global static PortalPrescriptionRestServiceData doGet() {
    
        req = RestContext.request;
        
        System.debug('REQSM ' + req.params);
        
        PortalPrescriptionRestServiceController.getInstance().createPrescriptions(req.params);
        
        return new PortalPrescriptionRestServiceData();
    }

    global class PortalPrescriptionRestServiceData {
        
        public Boolean status = true;
    }
 
}