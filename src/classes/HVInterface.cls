/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public interface HVInterface {

    Boolean synchronize(Account contact);
    
}