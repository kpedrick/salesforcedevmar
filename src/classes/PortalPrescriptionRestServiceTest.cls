/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@isTest(seeAllData=true)
public with sharing class PortalPrescriptionRestServiceTest {

    static testMethod void testPortalPrescriptionRestService() {
    
        Account account = TestUtils.createAccount('patient');
        
        RestContext.request = TestUtils.getPrescriptionParamsMap(account.Id);        
        PortalPrescriptionRestService.PortalPrescriptionRestServiceData result = PortalPrescriptionRestService.doGet();
        
        System.assert(result.status);
    }

}