@isTest
public with sharing class HVSSchedulerPageUIControllerTest {

    static testMethod void enabledPageControllerTest() {
    
        TestUtils.createDyrctAxessCustomSettings();
        DyrctAxess_Settings__c settings = DyrctAxess_Settings__c.getOrgDefaults();
        settings.Enable_Process__c = true;
        update settings;
        
        Test.startTest();
        
        HVSchedulerPageUIController controller = new HVSchedulerPageUIController();
        controller.start();
        controller.save();
        
        Test.stopTest();
    }

    static testMethod void disabledPageControllerTest() {
    
        TestUtils.createDyrctAxessCustomSettings();
        DyrctAxess_Settings__c settings = DyrctAxess_Settings__c.getOrgDefaults();
        settings.Enable_Process__c = false;
        settings.Apex_Job__c = 'fakeid';
        update settings;
        
        Test.startTest();
        
        HVSchedulerPageUIController controller = new HVSchedulerPageUIController();
        controller.start();
        controller.save();
        
        Test.stopTest();
    }

}