/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVGetThingsResponse extends HVResponse {

    private Health_Activity__c[] activities;
    private Id accountId;
    private final Map<String, Schema.Recordtypeinfo> rtName = Schema.SObjectType.Health_Activity__c.getRecordTypeInfosByName();

    public HVGetThingsResponse(String body, Id accountId) {
        super(body);
        activities = new Health_Activity__c[] {};
        this.accountId = accountId;
    }    

    public Health_Activity__c[] getActivities() {
        return activities;
    }

    public void loadElements() {
    
        loadElements(this.xml.getRootElement());
    }
    
    private void loadElements(Dom.Xmlnode root) {
    	
    	Dom.Xmlnode[] elements = getTagChildren(root, 'group');
    	
    	for (Dom.Xmlnode element :elements) {
    	
    	   String thingName = getAttributeValue(element, 'type-id', 'name');
    	   
    	   if ('Blood Pressure Measurement'.equalsIgnoreCase(thingName)) {
    	       loadElementBloodPressure(element);
    	   }
    	}
    }
    
    private Dom.Xmlnode getTagChild(Dom.Xmlnode root, String tag) {
    	
    	return getTagChildren(root, tag) != null ? getTagChildren(root, tag)[0] : null; 
    }
    
    private Dom.Xmlnode[] getTagChildren(Dom.Xmlnode root, String tag) {

        if (tag.equals(root.getName())) {
            return root.getChildren();
        }
        
        for (Dom.Xmlnode node :root.getChildElements()) {
            Dom.Xmlnode[] result = getTagChildren(node, tag); 
            if (result != null) {
                return result;
            }
        }
               
        return null;
    } 

    private void loadElementBloodPressure(Dom.Xmlnode root) {
    
        Dom.Xmlnode bloodPressure = getTagChild(root, 'data-xml');
        String dateXml = getTagValue(root, 'eff-date');
        String thingId = getTagValue(root, 'thing-id');
        
        String systolicValue = getTagValue(bloodPressure, 'systolic');
        String diastolicValue = getTagValue(bloodPressure, 'diastolic');
        String pulseValue = getTagValue(bloodPressure, 'pulse');
        String isIrregularHeartbeatValue = getTagValue(bloodPressure, 'irregular-heartbeat');
        
        systolicValue = systolicValue != null ? systolicValue : '0';
        diastolicValue = diastolicValue != null ? diastolicValue : '0';
        pulseValue = pulseValue != null ? pulseValue : '0';
        isIrregularHeartbeatValue = isIrregularHeartbeatValue != null ? isIrregularHeartbeatValue : 'false';
        
        addBloodPressureActivity(thingId, Integer.valueOf(diastolicValue), Integer.valueOf(systolicValue), Integer.valueOf(pulseValue), Boolean.valueOf(isIrregularHeartbeatValue), Datetime.valueOf(dateXml.replace('T', ' ')));
    }
    
    private void loadElementHeartRate(Dom.Xmlnode root) {
    
        Dom.Xmlnode heartRate = getTagChild(root, 'data-xml');
        String dateXml = getTagValue(root, 'eff-date');
        String thingId = getTagValue(root, 'thing-id');
        
        String rateValue = getTagValue(heartRate, 'value');
        
        Dom.Xmlnode method = getTagChild(heartRate, 'measurement-method');
        Dom.Xmlnode conditions = getTagChild(heartRate, 'measurement-conditions');
        
        String methodValue = getTagValue(method, 'value');
        String conditionsValue = getTagValue(conditions, 'value');
        
        rateValue = rateValue != null ? rateValue : '0';
        
        
        addHeartRateActivity(thingId, Integer.valueOf(rateValue), methodValue, conditionsValue, Datetime.valueOf(dateXml.replace('T', ' ')));
    }    
    
    private void addHeartRateActivity(String thingId, Integer value, String method, String conditions, Datetime dt) {
    
        Health_Activity__c ha = new Health_Activity__c();
        ha.Thing_Id__c = thingId;
        ha.Heart_Rate_Conditions__c = conditions;
        ha.Heart_Rate_Method__c = method;
        ha.Heart_Rate_Value__c = value;
        ha.Date__c = dt;
        ha.Account__c = accountId;
        ha.RecordTypeId = rtName.get('Heart Rate').getRecordTypeId(); 
        activities.add(ha);
    }

    private void addBloodPressureActivity(String thingId, Integer diastolic, Integer systolic, Integer pulse, Boolean isIrregularHeartbeat, Datetime dt) {
    
        Health_Activity__c bp = new Health_Activity__c();
        bp.Thing_Id__c = thingId;
        bp.Blood_Pressure_Diastolic__c = diastolic;
        bp.Blood_Pressure_Systolic__c = systolic;
        bp.Blood_Pressure_Pulse__c = pulse;
        bp.Blood_Pressure_Irregular_Heartbeat__c = isIrregularHeartbeat;
        bp.Date__c = dt;
        bp.Account__c = accountId;
        bp.RecordTypeId = rtName.get('Blood Pressure').getRecordTypeId(); 
        activities.add(bp);
    }
    
}