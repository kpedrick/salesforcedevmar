/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVSPatientBatchController {

    private static HVSPatientBatchController instance = null;
    
    public static HVSPatientBatchController getInstance() {
        
        if (instance == null) {
            return new HVSPatientBatchController();
        }
        return instance;
    }

    public Account[] getHealthVaultPatients() {
    
        Account[] result = new Account[] {};
        result = [SELECT 
						Website, 
						Type, 
						SystemModstamp, 
						SicDesc, 
						ShippingStreet, 
						ShippingState, 
						ShippingPostalCode, 
						ShippingLongitude, 
						ShippingLatitude, 
						ShippingCountry, 
						ShippingCity, 
						Salutation, 
						RecordTypeId, 
						Physician__c, 
						Physician_NPI_DEA__c, 
						Phone, 
						PersonTitle, 
						PersonOtherStreet, 
						PersonOtherState, 
						PersonOtherPostalCode, 
						PersonOtherPhone, 
						PersonOtherLongitude, 
						PersonOtherLatitude, 
						PersonOtherCountry, 
						PersonOtherCity, 
						PersonMobilePhone, 
						PersonMailingStreet, 
						PersonMailingState, 
						PersonMailingPostalCode, 
						PersonMailingLongitude, 
						PersonMailingLatitude, 
						PersonMailingCountry, 
						PersonMailingCity, 
						PersonLeadSource, 
						PersonLastCUUpdateDate, 
						PersonLastCURequestDate, 
						PersonHomePhone, 
						PersonEmailBouncedReason, 
						PersonEmailBouncedDate, 
						PersonEmail, 
						PersonDepartment, 
						PersonContactId, 
						PersonBirthdate, 
						PersonAssistantPhone, 
						PersonAssistantName, 
						Patient_Provider_Phone__c, 
						Patient_Member_Name__c, 
						Patient_Member_Id_Number__c, 
						Patient_Insurance_Provider__c, 
						Patient_Group_Number__c, 
						Patient_Credit_Card_Security_Number__c, 
						Patient_Credit_Card_Number__c, 
						Patient_Credit_Card_Name__c, 
						Patient_Credit_Card_Holder__c, 
						Patient_Charge_Credit_Card__c, 
						ParentId, 
						OwnerId, 
						NumberOfEmployees, 
						Name, 
						MasterRecordId, 
						LastViewedDate, 
						LastReferencedDate, 
						LastName, 
						LastModifiedDate, 
						LastModifiedById, 
						LastActivityDate, 
						JigsawCompanyId, 
						Jigsaw, 
						IsPersonAccount, 
						IsDeleted, 
						Industry, 
						Id, 
						Gender__c, 
						FirstName, 
						Fax, 
						Description, 
						CreatedDate, 
						CreatedById,
						BillingStreet, 
						BillingState, 
						BillingPostalCode, 
						BillingLongitude, 
						BillingLatitude, 
						BillingCountry, 
						BillingCity,
						AnnualRevenue,
					    
					    (SELECT Id,
								 IsDeleted,
								 Name,
								 CreatedDate,
								 CreatedById,
								 LastModifiedDate,
								 LastModifiedById,
								 SystemModstamp,
								 Application_Id__c,
								 Authorization_Shared_Secret__c,
								 Application_Shared_Secret__c,
								 Person_Id__c, 
                                 Record_Id__c,
								 Application_Token__c,
                                 Authorization_URL__c, 			
                                 Is_First_Run__c,					 
								 Account__c
							 FROM Health_Vault_Credentials__r
							 ORDER BY CreatedDate DESC)
					    
					FROM Account
					WHERE RecordType.Name = 'Patient'];
					//AND Health_Vault_Credentials_Count__c > 0]; 
    
        return result;
    }

    public Account getPatientById(Id accountId) {
    
        Account[] result = new Account[] {};
        result = [SELECT 
                        Website, 
                        Type, 
                        SystemModstamp, 
                        SicDesc, 
                        ShippingStreet, 
                        ShippingState, 
                        ShippingPostalCode, 
                        ShippingLongitude, 
                        ShippingLatitude, 
                        ShippingCountry, 
                        ShippingCity, 
                        Salutation, 
                        RecordTypeId, 
                        Physician__c, 
                        Physician_NPI_DEA__c, 
                        Phone, 
                        PersonTitle, 
                        PersonOtherStreet, 
                        PersonOtherState, 
                        PersonOtherPostalCode, 
                        PersonOtherPhone, 
                        PersonOtherLongitude, 
                        PersonOtherLatitude, 
                        PersonOtherCountry, 
                        PersonOtherCity, 
                        PersonMobilePhone, 
                        PersonMailingStreet, 
                        PersonMailingState, 
                        PersonMailingPostalCode, 
                        PersonMailingLongitude, 
                        PersonMailingLatitude, 
                        PersonMailingCountry, 
                        PersonMailingCity, 
                        PersonLeadSource, 
                        PersonLastCUUpdateDate, 
                        PersonLastCURequestDate, 
                        PersonHomePhone, 
                        PersonEmailBouncedReason, 
                        PersonEmailBouncedDate, 
                        PersonEmail, 
                        PersonDepartment, 
                        PersonContactId, 
                        PersonBirthdate, 
                        PersonAssistantPhone, 
                        PersonAssistantName, 
                        Patient_Provider_Phone__c, 
                        Patient_Member_Name__c, 
                        Patient_Member_Id_Number__c, 
                        Patient_Insurance_Provider__c, 
                        Patient_Group_Number__c, 
                        Patient_Credit_Card_Security_Number__c, 
                        Patient_Credit_Card_Number__c, 
                        Patient_Credit_Card_Name__c, 
                        Patient_Credit_Card_Holder__c,  
                        Patient_Charge_Credit_Card__c, 
                        ParentId, 
                        OwnerId, 
                        NumberOfEmployees, 
                        Name, 
                        MasterRecordId, 
                        LastViewedDate, 
                        LastReferencedDate, 
                        LastName, 
                        LastModifiedDate, 
                        LastModifiedById, 
                        LastActivityDate, 
                        JigsawCompanyId, 
                        Jigsaw, 
                        IsPersonAccount, 
                        IsDeleted, 
                        Industry, 
                        Id, 
                        Gender__c, 
                        FirstName, 
                        Fax, 
                        Description, 
                        CreatedDate, 
                        CreatedById,
                        BillingStreet, 
                        BillingState, 
                        BillingPostalCode, 
                        BillingLongitude, 
                        BillingLatitude, 
                        BillingCountry, 
                        BillingCity,
                        AnnualRevenue,
                        
                        (SELECT Id,
                                 IsDeleted,
                                 Name,
                                 CreatedDate,
                                 CreatedById,
                                 LastModifiedDate,
                                 LastModifiedById,
                                 SystemModstamp,
			                     Person_Id__c, 
			                     Record_Id__c,                                 
                                 Application_Id__c,
                                 Is_First_Run__c,
                                 Authorization_Shared_Secret__c,
                                 Application_Shared_Secret__c,
                                 Application_Token__c,
                                 Authorization_URL__c,
                                 Account__c 
                             FROM Health_Vault_Credentials__r
                             ORDER BY CreatedDate DESC)
                                                     
                    FROM Account
                    WHERE Id = :accountId]; 
    
        if (!result.isEmpty()) {
            return result[0];
        }
    
        return null;
    }
    
    public Health_Vault_Credential__c[] getCredentialsByContactId(Id accountId) {
    
        Health_Vault_Credential__c[] result = new Health_Vault_Credential__c[] {};
    
        result = [Select 
	                    SystemModstamp, 
	                    Name, 
	                    LastModifiedDate, 
	                    LastModifiedById, 
	                    IsDeleted, 
	                    Id, 
	                    CreatedDate, 
	                    CreatedById, 
	                    Account__c, 
	                    Person_Id__c, 
	                    Is_First_Run__c,
	                    Record_Id__c,
	                    Authorization_URL__c, 
	                    Authorization_Shared_Secret__c, 
	                    Application_Token__c, 
	                    Application_Shared_Secret__c, 
	                    Application_Id__c 
                    FROM Health_Vault_Credential__c
                    WHERE Account__c = :accountId
                    ORDER BY CreatedDate DESC];    
    
        return result;
    
    }

}