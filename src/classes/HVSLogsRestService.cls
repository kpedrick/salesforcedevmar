/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping = '/hvs/logs/*')
global class HVSLogsRestService {

    @httpGet
    global static HVSLogsResponse getHttp() {
        
        Restrequest req = RestContext.request;
        system.debug(req.requestURI);
        return getLogs(req.requestURI.substringAfterLast('/'));
    }
    /*
    @HttpPost
    global static BloodPressure saveType(String accountId, BloodPressure bp) {
        system.debug(accountId);
        system.debug(bp);
        Date dt = bp.getDate();
        if(dt == null){
        	return bp;
        }
        
        Health_Activity__c[] has = [SELECT 
                                            Id, 
                                            Date__c, 
                                            Blood_Pressure_Diastolic__c, 
                                            Blood_Pressure_Systolic__c, 
                                            Blood_Pressure_Pulse__c
                                             
                                        FROM Health_Activity__c
                                        WHERE Account__c = :accountId
                                        AND Date__c = :dt
                                        AND Blood_Pressure_Diastolic__c = :bp.diastolic
                                        AND Blood_Pressure_Systolic__c = :bp.systolic
                                        AND Blood_Pressure_Pulse__c = :bp.pulse
                                        ];
        if(has.isEmpty()){
        	insert bp.getHealthActivity(accountId);
        }
        
        return bp;
    }
    */
    
    @HttpPost
    global static List<BloodPressure> saveType(String accountId, List<BloodPressure> bpList) {
    	
    	system.debug('bpList -> '+ bpList);
    	
    	List<Health_Activity__c> haList = new List<Health_Activity__c>();
    	
    	for (BloodPressure bp :bpList){
    		haList.add(bp.getHealthActivity(accountId));
    	}
    	
    	try{
    		upsert haList Thing_Id__c;
    	}catch(Exception e){
    		system.debug(e);
    	}
    	
    	return bpList;

    }
    
    global class BloodPressure {
        global Integer year;
        global Integer month;
        global Integer day;
        global Integer hour;
        global Integer minute;
        global Integer sec;
        global Integer systolic;
        global Integer diastolic;
        global Integer pulse;
        global String thingId;
        global String irregularHeartBeat;
        
        
        global Datetime getDate(List<Account> accounts){
        	
        	Datetime myDatetime;
        	
        	TimeZone tz = UserInfo.getTimeZone();

        	if (year != null && month != null && day != null && hour != null && minute != null){
        		myDatetime = Datetime.newInstanceGMT(year, month, day, hour, minute, 0);
        		
        	}else if(year != null && month != null && day != null){
        		myDatetime =  Datetime.newInstance(year,month, day, 0, 0, 0);
        	}
        	
        	Integer systemOffset = tz.getOffset(myDatetime)/ 3600000;
        	
        	if (accounts.isEmpty() == false && accounts.get(0).UTC_Offset__c != null){
        		
        		myDatetime = myDatetime.addHours(Integer.valueOf(accounts.get(0).UTC_Offset__c) * -1);
        	}
        	 
        	return myDatetime;
        }
        
        global String getMinute(){
        	if (minute != null){
        		if (minute >= 0 && minute < 10){
        			return '0'+ String.valueOf(minute);
        		}else if (minute >= 10 && minute <= 60){
        			return String.valueOf(minute);
        		}else{
        			return null;
        		}
        	}
        	return null;
        }
        
        global String getHours(){
        	if (hour != null){
        		if (hour == 0){
        			return '12';
        		}else if (hour > 12 && hour < 24){
        			return String.valueOf(hour - 12);
        		}else if (hour > 0 && hour <= 12){
        			return String.valueOf(hour);
        		}else{
        			return null;
        		}
        	}
        	return null;
        }
        global String getAMPM(){
        	if (hour != null){
        		if (hour == 0){
        			return 'AM';
        		}else if (hour >= 12 && hour < 24){
        			return 'PM';
        		}else if (hour > 0 && hour < 12){
        			return 'AM';
        		}else{
        			return null;
        		}
        	}
        	return null;
        }
        
        global Health_Activity__c getHealthActivity(Id accountId){
        	
        	Account[] accounts = [SELECT UTC_Offset__c FROM Account WHERE Id = :accountId];
        	
        	return new Health_Activity__c(
        		Account__c = accountid,
        		Date__c = getDate(accounts),
        		Blood_Pressure_Diastolic__c = diastolic,
        		Blood_Pressure_Systolic__c = systolic,
        		Blood_Pressure_Pulse__c = pulse,
        		Thing_Id__c = thingId,
        		Date_Minutes__c = getMinute(),
        		Date_Hour__c = getHours(),
        		Date_AMPM__c = getAMPM(),
        		Blood_Pressure_Irregular_Heartbeat__c = (irregularHeartBeat == 'true')? true:false
        		
        	);
        }
    }
    
    global class HVSLogsResponse {
        Map<String, String> systolic;
        Map<String, String> diastolic;
        
        public HVSLogsResponse() {
            systolic = new Map<String, String>();
            diastolic = new Map<String, String>();
        }
    }
    
    /*
    public static HVSLogsResponse getFakeResponse() {
    
        HVSLogsResponse result = new HVSLogsResponse();
        result.systolic.put('year','[Date.UTC(1970,9,27),0],[Date.UTC(1970,10,10),0.6],[Date.UTC(1970,10,18),0.7],[Date.UTC(1970,11,2),0.8],[Date.UTC(1970,11,9),0.6],[Date.UTC(1970,11,16),0.6],[Date.UTC(1970,11,28),0.67],[Date.UTC(1971,0,1),0.81],[Date.UTC(1971,0,8),0.78],[Date.UTC(1971,0,12),0.98],[Date.UTC(1971,0,27),1.84],[Date.UTC(1971,1,10),1.80],[Date.UTC(1971,1,18),1.80],[Date.UTC(1971,1,24),1.92],[Date.UTC(1971,2,4),2.49],[Date.UTC(1971,2,11),2.79],[Date.UTC(1971,2,15),2.73],[Date.UTC(1971,2,25),2.61],[Date.UTC(1971,3,2),2.76],[Date.UTC(1971,3,6),2.82],[Date.UTC(1971,3,13),2.8],[Date.UTC(1971,4,3),2.1],[Date.UTC(1971,4,26),1.1],[Date.UTC(1971,5,9),0.25],[Date.UTC(1971,5,12),0]'); 
        result.diastolic.put('year','[Date.UTC(1970,9,18),0],[Date.UTC(1970,9,26),0.2],[Date.UTC(1970,11,1),0.47],[Date.UTC(1970,11,11),0.55],[Date.UTC(1970,11,25),1.38],[Date.UTC(1971,0,8),1.38],[Date.UTC(1971,0,15),1.38],[Date.UTC(1971,1,1),1.38],[Date.UTC(1971,1,8),1.48],[Date.UTC(1971,1,21),1.5],[Date.UTC(1971,2,12),1.89],[Date.UTC(1971,2,25),2.0],[Date.UTC(1971,3,4),1.94],[Date.UTC(1971,3,9),1.91],[Date.UTC(1971,3,13),1.75],[Date.UTC(1971,3,19),1.6],[Date.UTC(1971,4,25),0.6],[Date.UTC(1971,4,31),0.35],[Date.UTC(1971,5,7),0]'); 
        return result;
    }
    */
    
    global static HVSLogsResponse getLogs(String accountId) {
        if(accountId == null || accountId == '')
            return new HVSLogsResponse();
        Datetime dateLimitNow = Datetime.now();
        Datetime dateLimit = dateLimitNow.addYears(-1);
    
        HVSLogsResponse result = new HVSLogsResponse();
        
        Account[] accounts = [SELECT UTC_Offset__c FROM Account WHERE Id = :accountId];
        Health_Activity__c[] has = [SELECT 
                                            Id, 
                                            Date__c, 
                                            Blood_Pressure_Diastolic__c, 
                                            Blood_Pressure_Systolic__c, 
                                            Blood_Pressure_Pulse__c
                                             
                                        FROM Health_Activity__c
                                        WHERE Account__c = :accountId
                                        AND Date__c >= :dateLimit
                                        AND Date__c != null
                                        ORDER BY Date__c];

        String yearDiastolic = '';
        String semesterDiastolic = '';
        String quarterDiastolic = '';
        String monthlyDiastolic = '';
        String weeklyDiastolic = '';
        String yearSystolic = '';
        String semesterSystolic = '';
        String quarterSystolic = '';
        String monthlySystolic = '';
        String weeklySystolic = '';        
        
                    
        for (Health_Activity__c ha :has) {
    
            String d = '';
            String s = '';
            String p = '';
            
            Datetime mydateGMT;
            TimeZone tz = UserInfo.getTimeZone();
            Integer systemOffset = tz.getOffset(ha.Date__c)/ 3600000;
            
            if (accounts.isEmpty() == false && accounts.get(0).UTC_Offset__c != null){
            	//mydateGMT = ha.Date__c.addHours(systemOffset * (-1));
            	mydateGMT = ha.Date__c.addHours(Integer.valueOf(accounts.get(0).UTC_Offset__c));
            }
        
            if (ha.Blood_Pressure_Diastolic__c != null) {
            	if (mydateGMT != null){
            		d = '[' + mydateGMT.getTime() + ',' + ha.Blood_Pressure_Diastolic__c + '],';
            	}else{
            		d = '[' + ha.Date__c.getTime() + ',' + ha.Blood_Pressure_Diastolic__c + '],';
            	}
            }                
            
            if (ha.Blood_Pressure_Systolic__c != null) {
            	if (mydateGMT != null){
            		s = '[' + mydateGMT.getTime() + ',' + ha.Blood_Pressure_Systolic__c + '],';
            	}else{
                	s = '[' + ha.Date__c.getTime() + ',' + ha.Blood_Pressure_Systolic__c + '],';
            	}
            }                            
            
            if (ha.Blood_Pressure_Pulse__c != null) {
            	if (mydateGMT != null){
            		p += '[' + mydateGMT.getTime() + ',' + ha.Blood_Pressure_Pulse__c + '],';
            	}else{
                	p += '[' + ha.Date__c.getTime() + ',' + ha.Blood_Pressure_Pulse__c + '],';
            	}
            }
            
            if (ha.Date__c >= dateLimitNow.addDays(-7)) {
                weeklyDiastolic += d;
                weeklySystolic += s;     
            }
            
            if (ha.Date__c >= dateLimitNow.addMonths(-1)) {
                monthlyDiastolic += d;
                monthlySystolic += s;     
            }
            
            if (ha.Date__c >= dateLimitNow.addMonths(-3)) {
                quarterDiastolic += d;
                quarterSystolic += s;     
            }
            
            if (ha.Date__c >= dateLimitNow.addMonths(-6)) {
                semesterDiastolic += d;
                semesterSystolic += s;     
            }                              
        
            yearDiastolic += d;
            yearSystolic += s;                       
                                                            
        }                                        
        
        result.systolic.put('year', sanitize(yearSystolic));
        result.systolic.put('semester', sanitize(semesterSystolic));
        result.systolic.put('quarter', sanitize(quarterSystolic));
        result.systolic.put('month', sanitize(monthlySystolic));
        result.systolic.put('week', sanitize(weeklySystolic));
        
        result.diastolic.put('year', sanitize(yearDiastolic));
        result.diastolic.put('semester', sanitize(semesterDiastolic));
        result.diastolic.put('quarter', sanitize(quarterDiastolic));
        result.diastolic.put('month', sanitize(monthlyDiastolic));
        result.diastolic.put('week', sanitize(weeklyDiastolic));        
        
        return result;
    }
    
    public static String sanitize(String json) {
        
        if (json == '') {
            return json;
        }        
        return json.substring(0, json.length() - 1);
    }
    

}