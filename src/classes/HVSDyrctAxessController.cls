/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVSDyrctAxessController implements HVInterface {

    public Boolean synchronize(Account account) {
    
    
    
        /*****************************
        First Callout : Refresh Token
        *****************************/
        HVSessionController session = new HVSessionController(account.Id);
        session.execute(false);
    
        Health_Vault_Credential__c credential = session.getCredential();
        
        
        Boolean isFirstRun = credential.Is_First_Run__c;
        if (isFirstRun) {
        	credential.Is_First_Run__c = false;
        }
        
        HVGetThings getThings = new HVGetThings(credential.Record_Id__c, credential.Person_Id__c, credential.Application_Shared_Secret__c, credential.Application_Token__c, isFirstRun);
    
        Health_Activity__c[] records = new Health_Activity__c[] {};
        
        
        /*****************************************
        Second and Third Callout : Upsert Things
        ******************************************/        
        for (String thingName :HVGetThings.TYPES.keySet()) {
        
            getThings.setTypeName(thingName);
            HVResponse hvr =  HVConnection.sendMethodRequest(getThings.getRequestBody());
            
            HVGetThingsResponse response = new HVGetThingsResponse(hvr.getBody(), account.Id);
            response.loadElements();
            
            records.addAll(response.getActivities());
        }
    
        update credential;
    
        if (!records.isEmpty()) {
        	try {
        	   upsert records Thing_Id__c;
        	}
        	catch (Exception e) {
        	   System.debug('ERROR ON UPSERTING ' + e.getMessage());
        	}
        }
        
        return true;
    }

}