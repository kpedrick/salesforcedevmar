public with sharing class CaseController {

    public CaseController() {}

    public Case createCase(Account account, Id opportunityId) {
    
        Case result = new Case();
        
        result.Status = 'New';
        result.Origin = 'Web';
        result.Priority = 'High';
        result.Subject = 'Patient lacks Billing Information';
        result.AccountId = account.Id;
        result.ContactId = account.PersonContactId;
        result.Opportunity__c = opportunityId;
        
        return result;
    }
    
}