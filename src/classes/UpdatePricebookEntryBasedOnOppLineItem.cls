public class UpdatePricebookEntryBasedOnOppLineItem extends SObjectTrigger {
	
	public static Boolean executed = false;
	
	public UpdatePricebookEntryBasedOnOppLineItem(sObject[] sObjectOldList, sObject[] sObjectNewList) {
       super(sObjectOldList, sObjectNewList);
    }
    
    public override Boolean executable(sObject sObjectOld, sObject sObjectNew){
    	//if(executed) return false;
    	return true;
    }
    
    public override void execute(sObject[] sObjectList, Boolean forceUpdate){
       
       Set<Id> oppLineItemIds = new Set<Id>();
       for(sObject sObj : sObjectList){
       	  oppLineItemIds.add((Id)sObj.get(''+OpportunityLineItem.Id));
       }
       oppLineItemIds.remove(null);
       
       Map<Id,OpportunityLineItem> mapOli = new Map<Id,OpportunityLineItem>([Select Id,
       																				UnitPrice,
       																				Opportunity.Pricebook2.Physician__r.Business_Unit__c,
       																				PricebookEntry.Markup__c,
       																				Product2Id 
							      											 From OpportunityLineItem 
							      											 where id IN :oppLineItemIds
							      											 And Opportunity.Pricebook2.Physician__c != null]);		
       String[] businessUnit = new String[]{};
       
       //check if the unit price for the standard pricebook associated to the custom pricebook went up 
       //based on % mark up for that product
       for(OpportunityLineItem oli : mapOli.values()){
        if(oli.Opportunity.Pricebook2.Physician__r.Business_Unit__c != null){
        	businessUnit.add(oli.Opportunity.Pricebook2.Physician__r.Business_Unit__c);
        }
       }
       
        Map<Id,Pricebook2> mapPriceBooks = new Map<Id,Pricebook2>([Select Id,
        																  Name,	
        																  (Select Id,UnitPrice,Product2Id From PricebookEntries) 
        													       From Pricebook2 
        													       Where Name IN :businessUNit]);
        Map<String,List<PricebookEntry>> mapNamePricebookEntries = new Map<String,List<PricebookEntry>>();
        for(Pricebook2 pb : mapPricebooks.values()){
        	mapNamePricebookEntries.put(pb.Name,pb.PricebookEntries);
        }        													       
       	
       	List<OpportunityLineItem> listOliToUpdate = new List<OpportunityLineItem>();
       	List<PricebookEntry> listPbeToUpdate = new List<PricebookEntry>();
       	
       	for(OpportunityLineItem o : mapOli.values()){
       		
       		String busUnit = o.Opportunity.Pricebook2.Physician__r.Business_Unit__c;
       		
       		if(mapNamePricebookEntries.get(busUnit) != null){
       			//Check if the price went up of the RElated standard pricebook
       			for(PricebookEntry pbe : mapNamePricebookEntries.get(busUnit)){
       				if(pbe.Product2Id == o.Product2Id){
       					
       					system.debug('o.UnitPrice -> '+ o.UnitPrice);
       					system.debug('pbe.UnitPrice -> '+ pbe.UnitPrice);
       					system.debug('o.PricebookEntry.Markup__c -> '+ o.PricebookEntry.Markup__c);
       					
       					if(!(o.UnitPrice == ( pbe.UnitPrice * o.PricebookEntry.Markup__c / o.UnitPrice)) ){
       						//Increase value for the custom pricebook and opp line item
       						o.UnitPrice = pbe.UnitPrice * o.PricebookEntry.Markup__c / 100 ;
       						o.PricebookEntry.UnitPrice = pbe.UnitPrice * o.PricebookEntry.Markup__c / 100;
       						listPbeToUpdate.add(o.PricebookEntry);
       						listOliToUpdate.add(o);
       					}
       					break;
       				}
       			}
       		}
       	}
       	
       	if(!listOliToUpdate.isEmpty()) update listOliToUpdate;
       	if(!listPbeToUpdate.isEmpty()) update listPbeToUpdate;
      
             
    }
}