/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping = '/portal/patient/*')
global with sharing class PortalPatientRestService {

    public static Restrequest req = null;

    @httpGet
    global static PortalPatientRestServiceData doGet() {
    
        req = RestContext.request;
        
        Map<String, String> response = PortalPatientRestServiceController.getInstance().createPatient(req.params);
        
        return new PortalPatientRestServiceData(response);
    }

    global class PortalPatientRestServiceData {
        
        public String id;
        public String password;
        
        public PortalPatientRestServiceData(Map<String, String> params) {
            this.id = params.get('id');
            this.password = params.get('password');
        }
    }

}