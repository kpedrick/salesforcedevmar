/*
	
			AccountActiveStatusBatch job = new AccountActiveStatusBatch();
			Id processId = Database.executeBatch(job, 1);
			
			AccountActiveStatusBatch job = new AccountActiveStatusBatch();
			job.execute([SELECT Id FROM Account WHERE RecordType.Name = 'Patient'], date.parse('12/27/2015'));
*/

global class AccountActiveStatusBatch implements Database.Batchable<SObject> {
	
	public class ApexBatchJobException extends Exception {}
	
	public static String QUERY_CONSTANT = 'SELECT Id FROM Account WHERE RecordType.Name = \'Patient\'';
	private String query;
	
	public AccountActiveStatusBatch() {
		this(QUERY_CONSTANT);
	}
	
	public AccountActiveStatusBatch(String queryString) {
		
		this.query = queryString;
		
		//Check if the job is running already
        ApexClass ac = [select Name from ApexClass where Name = 'AccountActiveStatusBatch'];
        
        List<AsyncApexJob> aajs = [select Status from AsyncApexJob where ApexClassId = :ac.Id and 
        							JobType = 'batchApex' and Status in ('Queued', 'Processing')];
        							
        if (!aajs.isEmpty()) {
        	throw new ApexBatchJobException(aajs.size() + ' AccountActiveStatusBatch job(s) already running or processing.');
		}
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext ctx) {
	 	Database.QueryLocator qLocator = null;
        try {
            qLocator = Database.getQueryLocator(this.query);
        } catch (Exception e) {
            throw e;
        } 
        return qLocator;
	}
	
	global void execute(Database.BatchableContext ctx, List<SObject> sobjects) {
		this.execute(sobjects, Date.today());
	}
	
	public void execute(List<SObject> sobjects, Date dateToday){
		
		Set<Id> accountIds = new Set<Id>();
		List<Account> accountUpdateList = new List<Account>();
		
		for (SObject obj :sobjects){
			accountIds.add((Id)obj.get('Id'));
		}
		accountIds.remove(null);
		
		
		Map<Id,Account> accountMap = new Map<Id,Account>(  	[SELECT Id,
																	Active__c,
																	(SELECT Id,
																			CreatedDate,
																			LastModifiedDate,
																			CloseDate,
																			Status__c
																	 FROM Opportunities)
															 FROM Account
															 WHERE Id IN :accountIds
															 AND RecordType.Name = 'Patient'
															 ]);
		
		OpportunityAccountActiveStatusUtil oaas = new OpportunityAccountActiveStatusUtil(accountMap, dateToday);
		
		accountUpdateList.addAll(oaas.createUpdateList());
		
		if (accountUpdateList.isEmpty() == true)
			return;
			
	
		try{
			update accountUpdateList;
		}catch(Exception e){
			system.debug(e);
		}
	}
	
	global void finish(Database.BatchableContext ctx) {}

}