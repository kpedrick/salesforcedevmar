@isTest
public class TestFactory {
	
 	public Id standardId;
    public Account account;
    public Opportunity opportunity;
    public Pricebook2 pricebook;
    public Product2 product;
    public PricebookEntry standardPricebookEntry;
    public PricebookEntry pricebookEntry;
    
    public OpportunityLineItem opportunityLineItem;
    
    
    public Account patient;
    public Account physician;
    
    public RecordType patientRecordType;
    public RecordType physicianRecordType;
    
    
    public TestFactory(){}
    
    public void createPackageTimeSheetTest(Boolean doInsert){
        this.setPricebookStandard();
        this.createPriceBook('VAYA', doInsert);
        this.createAccount(doInsert, 'VAYA');
        this.createOpportunity(doInsert, this.account.Id, this.pricebook.Id);
        this.createProduct(doInsert);
        this.createStandardPricebookEntry(doInsert, this.product.Id, this.standardId);
        this.createPriceBookEntry(doInsert, this.product.Id, this.pricebook.Id);
        this.createOpportunityLineItem(doInsert, this.opportunity.Id, this.pricebookEntry.Id);
    }
    
    public void createAll(Boolean doInsert){
    	
    	this.patientRecordType = [SELECT Id FROM RecordType WHERE Name = 'Patient' AND SobjectType = 'Account'];
    	this.physicianRecordType = [SELECT Id FROM RecordType WHERE Name = 'Physician' AND SobjectType = 'Account'];
    	
    	this.setPricebookStandard();
        this.createPriceBook('VAYA', doInsert);
        this.createPhysician(doInsert, 'VAYA', this.physicianRecordType.Id);
        this.createPatient(doInsert, 'VAYA', this.patientRecordType.Id, this.physician.Id);
        this.createOpportunity(doInsert, this.patient.Id, this.pricebook.Id);
        this.createProduct(doInsert);
        this.createStandardPricebookEntry(doInsert, this.product.Id, this.standardId);
        this.createPriceBookEntry(doInsert, this.product.Id, this.pricebook.Id);
        this.createOpportunityLineItem(doInsert, this.opportunity.Id, this.pricebookEntry.Id);
    }
    
    public void setPricebookStandard(){   		
        this.standardId = Test.getStandardPricebookId();
    }
    
    public Pricebook2 CreatePriceBook(String name, Boolean doInsert){
    	this.pricebook = new Pricebook2(Name = name,
    									isActive = true );
    									
    	if (doInsert == true){
    		insert this.pricebook;
    	}
    	return this.pricebook;
    	
    }
    
    public Account createAccount(Boolean doInsert, String businessUnit){
        this.account = new Account(
        							Name = 'test Account',
        							Business_Unit__c = businessUnit
        							);
        if (doInsert == true){
                insert this.account;
        }
        
        return this.account;
    }
    
    public Account createPhysician(Boolean doInsert, String businessUnit, Id recordtypeId){
    	this.physician = new Account(
        							LastName = 'Physician1',
        							RecordTypeId = recordtypeId,
        							Business_Unit__c = businessUnit
        							);
        if (doInsert == true){
                insert this.physician;
        }
        
        return this.physician;
    }
    
    public Account createPatient(Boolean doInsert, String businessUnit, Id recordtypeId, Id physicianId){
    	this.patient = new Account(
        							LastName = 'Patient2',
        							RecordTypeId = recordtypeId,
        							Business_Unit__c = businessUnit,
        							Physician__c = physicianId
        							);
        if (doInsert == true){
                insert this.patient;
        }
        
        return this.patient;
    }
    
    public Opportunity createOpportunity(Boolean doInsert, Id accountId, Id pricebookId){
        this.opportunity = new Opportunity(
	                                		AccountId = accountId,
			                                Name = 'Test Opportunity',
			                                Pricebook2Id = pricebookId,
	                                        StageName = 'Prospecting',
	                                        CloseDate = Date.today()
                                   );
       if (doInsert == true){
               insert this.opportunity;
       }                                    
                                   
        return this.opportunity;
    }
    
    
    public Product2 createProduct(Boolean doInsert){
        this.product = new Product2(
                    Name = 'Test Product',
                    isActive = true
         );
                                                                
        if (doInsert == true){
                insert this.product;
        }                                                       
        
        return this.product;
    }
	
	public PricebookEntry createStandardPricebookEntry(Boolean doInsert, Id productId, Id pricebookId){
		
		this.standardPricebookEntry =  new PricebookEntry(
                        Pricebook2Id = pricebookId,
                        Product2Id = productId,
                        UnitPrice = 99,
                        isActive = true);
                        
		if (doInsert == true){
			insert this.standardPricebookEntry;
		} 
		return this.standardPricebookEntry;
	}
    
     public PricebookEntry createPriceBookEntry(Boolean doInsert, Id productId, Id pricebookId){
        this.pricebookEntry = new PricebookEntry(
                    Pricebook2Id = pricebookId,
                    Product2Id = productId,
                    UnitPrice = 99,
                    isActive = true);
                                                                                                
        if (doInsert == true){
             insert this.pricebookEntry;
        }                                                                                       
        
        return this.pricebookEntry;
    }
    
    public OpportunityLineItem createOpportunityLineItem(Boolean doInsert, Id opportunityId, Id pricebookEntryId){
            this.opportunityLineItem = new OpportunityLineItem(
											                    PriceBookEntryId = pricebookEntryId,
											                    OpportunityId = opportunityId,
											                    Quantity = 1,
											                    TotalPrice = 99
             													);
                                                                                                                    
            if (doInsert == true){
				insert this.opportunityLineItem;
            }
            
            return this.opportunityLineItem;
    }
}