public with sharing class PharmacyOpportunitiesController {

	public Opportunity[] opportunities {get;set;}
	
	
	
	public PharmacyOpportunitiesController() {
	
		opportunities = [SELECT Id, Name, StageName, Status__c, CloseDate FROM Opportunity WHERE Owner.UserRole.Name = 'Pharmacy'];
		
	
	}
	



}