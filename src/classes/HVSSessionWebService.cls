/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping='/HealthVault/Session/*')
global class HVSSessionWebService {

    @HttpGet
    global static SessionWebServiceResponse getHealthVaultUrl() {
    
        RestRequest req = RestContext.request;
        String accountId = req.params.get('accountid');
        String token = req.params.get('token');
        system.debug('request info: ' + accountId + ' : ' + token);
        try {
        	
            SessionWebServiceResponse result = new SessionWebServiceResponse(execute(accountId,token));
            return result;
        }
        catch (Exception e) {}
    
        return new SessionWebServiceResponse('false');
    } 
 
    public static String execute(String contactId,String token) {
    	
        HVSessionController controller = new HVSessionController(Id.valueOf(contactId));
        return controller.execute();
    }
 
    global class SessionWebServiceResponse {
    
        private String success;
        
        public SessionWebServiceResponse(String success) {
            this.success = success;
        }
    }

}