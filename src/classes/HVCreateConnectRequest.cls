/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVCreateConnectRequest extends HVRequest {

    public static final String SHELL = 'https://account.healthvault-ppe.com';
    
    public HVCreateConnectRequest() {
	    this.header = new HVRequestHeader('CreateConnectRequest','1');
	    this.info = new HVRequestInfo();
    } 

    public String getShellRedirectURL(String appToken) {
        
        String instanceName = '1';
        String targetqs = '?appid={APPID}&appCreationToken={TOKEN}&instanceName={INSTANCE}';
        
        String appId = Encodingutil.urlEncode(HVRequest.APP_ID, 'UTF-8');
        String token = Encodingutil.urlEncode(appToken, 'UTF-8');
        String instance = Encodingutil.urlEncode(instanceName, 'UTF-8');
        
        targetqs = targetqs.replace('{APPID}', appId).replace('{TOKEN}', token).replace('{INSTANCE}', instance);
        
        String result = '{SHELL}/redirect.aspx?target=CREATEAPPLICATION&targetqs=';
        result = result.replace('{SHELL}', SHELL);
        
        return result + Encodingutil.urlEncode(targetqs, 'UTF-8'); 
    }
    

}