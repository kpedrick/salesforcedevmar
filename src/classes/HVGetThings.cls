/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVGetThings extends HVRequest {

    private String recordId;
    private String personId;
    private String typeName;
    private String sharedSecret;
    private String token;
    private Boolean isFirstRun;
    
    public static final Map<String, Health_Vault_Types__c> TYPES = Health_Vault_Types__c.getAll(); 
    
    public HVGetThings(String recordId, String personId, String sharedSecret, String token, Boolean isFirstRun) {

        this.header = new HVRequestHeader('GetThings','3', null);
        this.info = new HVRequestInfo();
        
        this.sharedSecret = sharedSecret;
        this.token = token;        
        this.recordId = recordId;
        this.personId = personId;
        this.isFirstRun = isFirstRun;
    }    
    
    public HVGetThings(String recordId, String personId, String sharedSecret, String token) {

        this.header = new HVRequestHeader('GetThings','3', null);
        this.info = new HVRequestInfo();
        
        this.sharedSecret = sharedSecret;
        this.token = token;        
        this.recordId = recordId;
        this.personId = personId;
        this.isFirstRun = false;
    }
    
    public void setTypeName(String typeName) {
    
        this.typeName = typeName;
    }

    public override String getRequestBody() {
    
        String headerValue = header.getBasicHeader(getInfoHash(), getAuthSession());
    
        String result = '';
        result += REQUEST_BEGIN;
        result += getAuth(headerValue);
        result += headerValue;
        result += info.getInfo(getContent());
        result += REQUEST_END;
        
        return result;
    }
    
    private String getAuth(String headerValue) {
    
        String result = '';
        result += '<auth>';
        result += '<hmac-data algName="HMACSHA256">';
        result += getHMACSHA256Encrypt(headerValue);
        result += '</hmac-data>';
        result += '</auth>'; 
    
        return result;
    }
    
    private String getInfoHash() {
    
        String result = '';
        result += '<info-hash>';
        result += '<hash-data algName="SHA256">';
        result += getSHA256Hash(getInfoContent());
        result += '</hash-data>';
        result += '</info-hash>';
        
        return result;
    }
    
    private String getAuthSession() {
    
        String result = '';
        result += '<record-id>';
        result += recordId;
        result += '</record-id>';
        result += '<auth-session>';
        result += '<auth-token>';
        result += token;
        result += '</auth-token>';
        result += '<offline-person-info>';
        result += '<offline-person-id>';
        result += personId;
        result += '</offline-person-id>';
        result += '</offline-person-info>';
        result += '</auth-session>';
            
        return result;
    }
    
    private String getInfoContent() {
        return '<info>' + getContent() + '</info>';    
    }
    
    private String getContent() {
    	
    	String typeId = TYPES.get(typeName).Value__c;
    	
        String result = '';
        result += '<group>';
        result += '<filter>';
        result += '<type-id>';
        result += typeId;
        result += '</type-id>';
        result += '<thing-state>Active</thing-state>';
        
        if (!isFirstRun) {
	        result += '<updated-date-min>';
	        result += Datetime.now().format('yyyy-MM-dd') + 'T00:00:00';
	        result += '</updated-date-min>';
        }
        else {
            result += '<updated-date-min>';
            result += Datetime.now().addMonths(-6).format('yyyy-MM-dd') + 'T00:00:00';
            result += '</updated-date-min>';      	
        }
        
        result += '</filter>';
        result += '<format>';
        result += '<section>core</section>';
        result += '<xml />';
        result += '<type-version-format>';
        result += typeId;
        result += '</type-version-format>';
        result += '</format>';
        result += '</group>';
        
        return result;
    }    
    
    private String getSHA256Hash(String body) {
        
        String result = Encodingutil.base64Encode(Crypto.generateDigest('SHA-256', Blob.valueOf(body)));
        
        return result;
    }        
    
    private String getHMACSHA256Encrypt(String body) {
        
        Blob ss = Encodingutil.base64Decode(sharedSecret);
        String result = Encodingutil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(body), ss));
        
        return result;
    }  
    
    
}