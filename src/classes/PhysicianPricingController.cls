public with sharing class PhysicianPricingController {


public Account acc {get;set;}
public Pricebook2 pricebook {get;set;}
public Pricebook2 newPricebook {get;set;}
public List<PricebookEntry> listPbe {get;set;}
public List<PricebookEntry> newListPbe {get;set;}
public Map<Id,Double> mapStandardPrices {get;set;}
public Map<Id,Double> mapPhysicianRelatedPrices {get;set;}

public PhysicianPricingController(ApexPages.standardController stdCon){
	
	this.acc = (Account)stdCon.getRecord();
	this.acc = [Select Id,Name,Business_Unit__c From Account Where Id = : this.acc.Id];
	
}

public PageReference safeAction(){
	
	this.newPricebook = new Pricebook2(isActive=true,Physician__c = this.acc.Id);
	this.newListPbe = new List<PricebookEntry>();
    this.mapStandardPrices = new Map<Id,Double>();
    this.mapPhysicianRelatedPrices = new Map<Id,Double>();
    
	//Retrieve pricebook & pricebook entries in case of exists otherwise create new one 
	List<Pricebook2> pbe = [Select Id From Pricebook2 Where Physician__c = :this.acc.Id limit 1];
	this.pricebook = pbe.isEmpty() ? null : pbe.get(0) ;
	this.listPbe = this.pricebook != null ? [Select Id,Markup__c,UnitPrice,isActive,Product2.Name,Pricebook2.Name 
											From PricebookEntry 
											Where Pricebook2Id = :this.pricebook.Id AND isActive=true] : null;
  	
  	//Two list to retrieve standard pricebook values from Business unit (dricxAccess, Vaya)  or Standard pricebook
  	List<PricebookEntry> lstPbWithBusinessUnit = [Select Id,isActive,Product2Id,UnitPrice,Product2.Name From PricebookEntry Where Pricebook2.Name = :this.acc.Business_Unit__c];
  	List<PricebookEntry> lstPbWithStandardValues =  [Select Id,isActive,Product2Id,UnitPrice,Product2.Name From PricebookEntry Where Pricebook2.IsStandard = true];
  	
  	if(this.listPbe != null){
  		if(this.acc.Business_Unit__c != null){
  		 for(PricebookEntry p : lstPbWithBusinessUnit){
  		  this.mapStandardPrices.put(p.Product2Id,p.UnitPrice);	
  		  this.mapPhysicianRelatedPrices.put(p.Product2Id,p.UnitPrice);	
  		 }
  		}else{
  		 for(PricebookEntry p : lstPbWithStandardValues){
  		  this.mapStandardPrices.put(p.Product2Id,p.UnitPrice);
  		  this.mapPhysicianRelatedPrices.put(p.Product2Id,p.UnitPrice);	
  		 }	
  		}
  	}
  	
    //if pricebook is null we will create a record for each entry of Business Unit from Account 
    //Which must to match with a name of a Pricebook
    if(this.pricebook == null  ){
   	 for(PricebookEntry pEntry : lstPbWithBusinessUnit ){
   	 	this.mapStandardPrices.put(pEntry.Product2Id,pEntry.UnitPrice);
   		PricebookEntry p = new PricebookEntry(Product2Id = pEntry.Product2Id,UnitPrice = pEntry.UnitPrice);
   		newListPbe.add(pEntry);
   	 }
   	 //In case of no match with Business Unit or the field is empty we will use standard price book entries
   	 if(newListPbe.isEmpty()){
   	 	 for(PricebookEntry pEntry : lstPbWithStandardValues){
   	 	 	    this.mapStandardPrices.put(pEntry.Product2Id,pEntry.UnitPrice);
   	 	 		PricebookEntry p = new PricebookEntry(Product2Id = pEntry.Product2Id,UnitPrice = pEntry.UnitPrice);
   				newListPbe.add(pEntry);	
   	 	 }
   	 }
    }
	
	return null;
}





/**
* @descp: will save all values changed on Pricebook entries for an existing Pricebook
* Also, Will validate that a price won't be lower than standard pricebook
*
**/

public Pagereference save(){
	
	
	try{
		for(PricebookEntry pbe: this.listPbe){
			
			system.debug('pbe.Product2Id -> '+ pbe.Product2Id);
			system.debug('this.mapStandardPrices -> '+ this.mapStandardPrices);
			system.debug('this.mapStandardPrices.containsKey(pbe.Product2Id) -> '+this.mapStandardPrices.containsKey(pbe.Product2Id));
			
			system.debug('pbe.UnitPrice ->'+ pbe.UnitPrice);
			
			
			if(pbe.UnitPrice < this.mapStandardPrices.get(pbe.Product2Id) ){
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Price cannot be less than standard'));
			 return null;
			}else{
			 //Calculate % mark up on each pricebook
			 Double markUp = null; 
			 if(pbe.UnitPrice >= this.mapStandardPrices.get(pbe.Product2Id)){
			 	markup = pbe.UnitPrice * 100 / this.mapStandardPrices.get(pbe.Product2Id);
			    pbe.Markup__c = markup;	
			 }
			}									   
	   }
	   update this.listPbe;
	   ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Confirm,'Save success'));
	   return null;
	}catch(Exception e){
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,e.getMessage()));
		return null;
	}
	
	
}

/*
* @descp: This method will save new pricebook with all pricebook entries values. 
* Will validate if the price is not lower than the Standard 
*/
public PageReference saveNewPb(){
	
	try{
	
	//Insert new pricebook	
	insert this.newPricebook;
	
	//Insert pricebook entry related - Validate if the price is not lower
	List<PricebookEntry> listPbeToInsert = new List<PricebookEntry>();
	for(PricebookEntry pbe: this.newListPbe)
	{
		if(pbe.UnitPrice < this.mapStandardPrices.get(pbe.Product2Id) )
		{
		 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Price cannot be less than standard'));
		 return null;
		}else
		{
		 //Calculate % mark up on each pricebook
		 Double markUp = null; 
		 if(pbe.UnitPrice > this.mapStandardPrices.get(pbe.Product2Id))
		 	markup = pbe.UnitPrice * 100 / this.mapStandardPrices.get(pbe.Product2Id);
		 	
		 listPbeToInsert.add(new PricebookEntry(Pricebook2Id = this.newPricebook.Id,
											    Product2Id = pbe.Product2Id,
											    isActive= pbe.IsActive,
											    UnitPrice = pbe.UnitPrice,
											    Markup__c = markup));
		}									   
	}
	insert listPbeToInsert;
	
	//Send success message and refresh everything to see the pricebook with values
	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Confirm,'Save success'));
	this.pricebook = [Select Id From Pricebook2 Where Physician__c = :this.acc.Id];
	this.listPbe =   [Select Id,UnitPrice,Markup__c,isActive,Product2.Name,Pricebook2.Name 
												From PricebookEntry 
												Where Pricebook2Id = :this.pricebook.Id and isActive=true] ;
												
	this.safeAction();
	
	return null;
	}catch(Exception e){
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,e.getMessage()));
		return null;
	}
}

public PageReference back(){
	return new PageReference('/'+ this.acc.Id);
}
}