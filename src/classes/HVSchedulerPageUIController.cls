/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVSchedulerPageUIController {

    public DyrctAxess_Settings__c settings {get; set;}
    
    public HVSchedulerPageUIController() {}
    
    public void start() {
        settings = DyrctAxess_Settings__c.getOrgDefaults();
        if (settings.Id == null) {
            insert settings;
        }
    }
    
    public void save() {
        
        if (settings.Enable_Process__c) {
        
            try {
                
                Integer interval = (settings.Interval__c != null && settings.Interval__c > 1) 
                                       ? Integer.valueOf(settings.Interval__c) 
                                       : 2;
                                       
                settings.Apex_Job__c = System.scheduleBatch(new HVSPatientBatch(), 'Health Vault API Process', interval, 1);
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Process scheduled'));
            }
            catch (Exception e) {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Process failed to execute or it is already scheduled'));
            } 
            finally {
                update settings;
            }
        }
        else {
            
            if (settings.Apex_Job__c != null) {
                try {
                    System.abortJob(settings.Apex_Job__c);
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Process Terminated'));
                }
                catch (Exception e) {
                   Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Process Terminated'));
                } 
                finally {
                    settings.Apex_Job__c = null;
                    update settings;
                }
            }
        }
        
    }


}