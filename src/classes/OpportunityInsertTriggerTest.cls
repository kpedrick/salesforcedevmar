@isTest
public with sharing class OpportunityInsertTriggerTest {

    private static Account patient = null;
    private static Opportunity opportunity = null;

    static testMethod void test() {
    
        patient = TestUtils.createAccount('Patient');
        
        Test.startTest();
        
        opportunity = TestUtils.createPrescription(patient.Id);
        
        Test.stopTest();
    }
    
    private static void assertCaseCreation() {
    
        Case[] result = [SELECT Id FROM Case WHERE AccountId  = :patient.Id];
        System.assert(!result.isEmpty());
    }
    
}