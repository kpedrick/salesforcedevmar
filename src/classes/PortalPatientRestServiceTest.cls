/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@isTest(seeAllData=true)
public with sharing class PortalPatientRestServiceTest {
	
    static testMethod void testPatientRestServicePatient() {
 
        Account account = TestUtils.createAccount('physician');
        Portal_Credential__c credential = TestUtils.createCredential(account.Id);
        
        PortalPatientRestService.PortalPatientRestServiceData result = null;
        
        Test.startTest();
        
        RestContext.request = TestUtils.getPatientParamsMap(account.Id);
        result = PortalPatientRestService.doGet();
        
        Test.stopTest();
        
        System.assert(result.password != null);
        System.assert(result.Id != null);
        System.assertEquals(result.id, [SELECT Id, Account__c FROM Portal_Credential__c WHERE Id = :result.Id].Id);
    }	

}