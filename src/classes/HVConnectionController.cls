/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVConnectionController extends HVWebService {

    private Health_Vault_Credential__c credential = null; 
    
    public HVConnectionController(Id accountId) {
        super(accountId);
        system.debug('accountid: ' + accountId);
    }

    public override String execute() {
    
        String result = createHVContactApplication(HVSPatientBatchController.getInstance().getPatientById(accountId));
        return result;
    }
    
    private String createHVContactApplication(Account account) {
    
        String result = null;
    
        credential = createCredential(account);
    
        HVNewApplicationCreationInfo newApplicationCreationInfo = new HVNewApplicationCreationInfo();
        String pkg = newApplicationCreationInfo.getRequestBody();
        system.debug('application create: ' + pkg);
        HVResponse hvr = HVConnection.sendMethodRequest(pkg);
        system.debug('Connection response: ' + hvr);
        
        if (hvr != null) {

            HVCreateConnectRequest createConnectRequest = new HVCreateConnectRequest();
            system.debug('redirect app id: ' + hvr.getTagValue('app-token')); 
            String url = createConnectRequest.getShellRedirectURL(hvr.getTagValue('app-token'));        
            
            credential.Authorization_URL__c = url;  
            credential.Authorization_Shared_Secret__c = hvr.getTagValue('shared-secret');
            credential.Application_Id__c = hvr.getTagValue('app-id');
                      
            result = url;
            deletePreviousCredentials(account);        
            insert credential;              
        }
        
        return result;
    }
    
    private void deletePreviousCredentials(Account account) {
    
        Health_Vault_Credential__c[] credentials = HVSPatientBatchController.getInstance().getCredentialsByContactId(account.Id);
        
        if (!credentials.isEmpty()) {
            delete credentials;
        }
    }
    
    private Health_Vault_Credential__c createCredential(Account account) {
    
        Health_Vault_Credential__c result = new Health_Vault_Credential__c();
        result.Account__c = account.Id;
        return result;
    }

}