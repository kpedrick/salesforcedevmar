/*
* @author: mdiaz
* @descp: Inventory controller to manage creidts and debits for a product
**/
public with sharing class InventoryManagementController {

public Product2 product {get;set;}
public List<OpportunityLineItem> listOlis {get;set;}
public List<Inventory__c> listInv {get;set;}
public Boolean isInventorySum {get;set;}
public Inventory__c inventory {get;set;}


public InventoryManagementController(ApexPages.StandardController stdCon){
 this.product = (Product2)stdCon.getRecord();
 this.isInventorySum = false;
 this.inventory = new Inventory__c();
 
 this.listInv = [Select Id,
                        Product_Id__c,
                        Product_Id__r.Name,
                        Debit__c,
                        Credit__c,
                        Purchase_Date__c,
                        Status__c,
                        Prescription__c,
                        Type__c,
                        Dosage__c,
                        //days__c,
                        Physician__c,
                        Physician__r.Name,
                        Patient__r.Name
                From Inventory__c 
                Where Product_Id__c = :this.product.Id order by CreatedDate DESC];
 
 this.listOlis = [Select Id,OpportunityId,Shipping_Datetime__c 
                  From OpportunityLineItem 
                  Where Product2Id = :this.product.Id];
                  
                  
                  
//Account type 'Physician' has account 'Patient' as children                  
 
}

/**
* @author: mdiaz
* @descp: 
**/

public PageReference addInventory(){
    this.isInventorySum = true;
    return null;
}

/*
* @descp: Add inventory for a given product to the system (it will be debited)
* Also back to the table for the product 
**/

public PageReference saveInventory(){
    
    try{
    this.inventory.Product_ID__c = this.product.Id;
    this.inventory.Status__c = 'Complete';
    insert this.inventory;
    
    //Show success message and retrieve all records to see on the same page
    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Save success'));
    this.isInventorySum = false;
    this.listInv = [Select Id,
                        Product_Id__c,
                        Product_Id__r.Name,
                        Debit__c,
                        Credit__c,
                        Purchase_Date__c,
                        Status__c,
                        Prescription__c,
                        Type__c,
                        Dosage__c,
                        //days__c,
                        Physician__c,
                        Physician__r.Name,
                        Patient__r.Name
                From Inventory__c 
                Where Product_Id__c = :this.product.Id order by CreatedDate DESC];
    //this.product = [Select Id From Product2 Where Id = :this.product.Id];     
    this.isInventorySum = false;        
    return null;
    }catch(Exception e){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, e.getMessage()));
        return null;
    }
}

public PageReference back(){
    this.isInventorySum = false;
    return new PageReference('/apex/InventoryManagement?id='+this.product.Id);
}

public PageReference backToProduct(){
    return new PageReference('/'+ this.product.Id);
}

/*
* @descp: dosages per product. This code is re-used so it has to be in a helper class**
*
**/
public Selectoption[] getOptions() {
    Selectoption[] result = new Selectoption[] {};
    result.add(new Selectoption('', '- none -'));
    Dosage__c[] dosages = [SELECT Id,
                                 Name, 
                                 Product__c, 
                                 Value__c, 
                                 Unit__c,Display_Value__c 
                             FROM Dosage__c
                             WHERE Product__c = :this.product.Id];
      for (Dosage__c dosage :dosages) {
        String value = dosage.Display_Value__c;
        result.add(new Selectoption(value, value));
      }
    return result;
  }



}