@isTest
private class InventoryManagamentControllerTest {

    static testMethod void InventoryManagamentControllerTest() {
       
       Product2 prod = TestUtils.createProduct('prod');
       ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(prod);
       InventoryManagementController imc = new InventoryManagementController(stdCon);
       
       //get options dosage
       imc.getOptions();
       
       //back & backtoProduct
       imc.back();
       imc.backToProduct();
       
       //Save inventory 
       Inventory__c inv = imc.inventory ;
       inv.Debit__c = 2;
       inv.Product_ID__c = prod.Id;
       imc.saveInventory();
       system.assertEquals(true,[Select Id From Inventory__c Where Id = :inv.Id limit 1] != null);
       
    }
}