global class OpportunityRefillSchedulable implements Schedulable {
	
	public static String CRON_EXP = '0 0 2 * * ?';
	
	global void execute(SchedulableContext SC) {
	 	
		Database.executeBatch(new OpportunityRefillBatch(), 1);
	}

}