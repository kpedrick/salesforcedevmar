@IsTest
public class UniqueIdSynchronizerTest {
	static String salutation;
	static String firstname;
	static String lastname;
	@isTest
	static Account testNewAccount(){
		salutation = 'Mr.';
		firstname = 'Test';
		lastname = 'User';
		RecordType patientType  = [select id  
			                        from recordtype 
			                       where SobjectType = 'Account' 
			                        and  DeveloperName = 'Patient'];
		
		Account a =  new Account(
							recordtypeid = patientType.id,
							Salutation = salutation,
							firstname = firstname,
							lastname = lastname,
							Business_Unit__c = 'DyrctAxess'
							);
		insert a;
		a = [select Salutation,firstname, first_name__c,lastname,last_name__c, Unique_Identifier__c,Salutation__c from Account where id = :a.id ];
		system.assert(a.Unique_Identifier__c == a.lastName);
		system.assert(firstname == A.first_Name__c, a.firstName + ' != ' + a.first_Name__c);
		system.assert(lastname == A.Last_Name__c, a.LastName + ' != ' + a.Last_Name__c);
		system.assert(salutation == A.Salutation__c, a.Salutation + ' != ' + a.Salutation__c);
		return a;
	}
	
	@isTest
	static void testUpdateAccount(){
		Account a = testNewAccount();
		test.startTest();
		firstname = 'Blah';
		lastname = 'Blah';
		a.firstName = firstname;
		a.LastName = lastname;
		String identifier = a.Unique_Identifier__c;
		update a;
		a = [select Salutation,firstname, first_name__c, last_name__c,lastname, Unique_Identifier__c,Salutation__c from Account where id = :a.id ];
		system.assert(identifier == a.Unique_Identifier__c, a.Unique_Identifier__c + ' != ' +  identifier);
	}
}