global class AccountActiveStatusSchedulable implements Schedulable {
	
	global void execute(SchedulableContext  sc) {
		
		AccountActiveStatusBatch job = new AccountActiveStatusBatch();
		Id processId = Database.executeBatch(job, 100);
		
	}

}