/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVSessionController extends HVWebService {
    
    private Health_Vault_Credential__c credential; 
    
    public HVSessionController(Id accountId) {
        super(accountId);
    }

    public Health_Vault_Credential__c getCredential() {
    	return credential;
    }
    
    public override String execute() {
    
        return execute(true);
    }

    public String execute(Boolean authorizePeople) {
    
        Boolean result = createHVContactSession(HVSPatientBatchController.getInstance().getPatientById(accountId), authorizePeople);
        return String.valueOf(result);
    }
    
    private Boolean createHVContactSession(Account account, Boolean authorizePeople) {
    
        Boolean result = false;
    
        credential = (!account.Health_Vault_Credentials__r.isEmpty()) ? account.Health_Vault_Credentials__r[0] : null;

        System.debug('POKING CREDENTIAL ' + credential);

        if (credential != null) {
        
	        HVCreateAuthenticatedSessionToken createAuthenticatedSessionToken = new HVCreateAuthenticatedSessionToken(credential.Application_Id__c, credential.Authorization_Shared_Secret__c);        
	        HVResponse hvr = HVConnection.sendMethodRequest(createAuthenticatedSessionToken.getRequestBody());
	        
	        if (hvr != null) {
	
                credential.Application_Shared_Secret__c = hvr.getTagValue('shared-secret');
                credential.Application_Token__c = hvr.getTagValue('token');
                
                if (authorizePeople) {
                
			        HVGetAuthorizedPeople getAuthorizedPeople = new HVGetAuthorizedPeople(credential.Application_Id__c, credential.Application_Shared_Secret__c, credential.Application_Token__c);
			        hvr = HVConnection.sendMethodRequest(getAuthorizedPeople.getRequestBody());
	                
	                if (hvr != null) {
	                	credential.Person_Id__c = hvr.getTagValue('person-id');
	                	credential.Record_Id__c = hvr.getAttributeValue('record', 'id');
	                }
	                
	                update credential;
                }
                
                
                result = true;              
	        }
        }
        
        return result;
    }    
    
    
    private Health_Vault_Credential__c getCredential(Account account) {

        Health_Vault_Credential__c result = null;
        Health_Vault_Credential__c[] creds = HVSPatientBatchController.getInstance().getCredentialsByContactId(account.Id);

        if (!creds.isEmpty()) {
            result = creds[0];
        }

        return result;
    }    

}