@isTest
private class OpportunityRefillTest {

    static testMethod void testRefills() {
    	
    	OpportunityRefillTestFactory factory = new OpportunityRefillTestFactory();
    	factory.createAll(true);
    	
    	factory.opportunity.Status__c = 'Shipped';
    	
    	update factory.opportunity;
    	
    	factory.opportunity.Prescription_Shipped_Date__c = Date.today().addDays(-30);
    	
    	update factory.opportunity;
    	
    	Test.startTest();
    	
    	OpportunityRefillBatch batch = new OpportunityRefillBatch();
    	batch.execute(Database.query(OpportunityRefillBatch.QUERY_CONSTANT), Date.today());
  
    	Test.stopTest();
    	
    }
    
    static testMethod void testRefillsFuture() {
    	 OpportunityRefillTestFactory factory = new OpportunityRefillTestFactory();
    	factory.createAll(true);
    	
    	factory.opportunity.Status__c = 'Shipped';
    	
    	update factory.opportunity;
    	
    	factory.opportunity.Prescription_Shipped_Date__c = Date.today().addDays(-30);
    	
    	update factory.opportunity;
    	
    	Test.startTest();
    	
    	OpportunityRefillBatch batch = new OpportunityRefillBatch();
    	batch.execute(Database.query(OpportunityRefillBatch.QUERY_CONSTANT), Date.today().addDays(30));
    	
    	Test.stopTest();
    }
    
    static testMethod void testBatch() {
    	
    	OpportunityRefillTestFactory factory = new OpportunityRefillTestFactory();
    	factory.createAll(true);
    	
    	Id processId = Database.executeBatch(new OpportunityRefillBatch(), 1);

    }
    
    static testMethod void testScheduler() {
    	System.schedule('testBasicOpportunityRefillSchedulable', OpportunityRefillSchedulable.CRON_EXP, new OpportunityRefillSchedulable());
    }
    
}