public abstract class SObjectTrigger {
	
    public sObject[] sObjectOldList {get;set;}
    public sObject[] sObjectNewList {get;set;}
    
    public Map<Id, sObject> sObjectOldListMap = new Map<Id, sObject>();
    public Map<Id, sObject> sObjectNewListMap = new Map<Id, sObject>();
    
    public SObjectTrigger(sObject[] sObjectOldList, sObject[] sObjectNewList) {
        this.sObjectOldList = sObjectOldList == null ? new sObject[] {} : sObjectOldList;
        this.sObjectNewList = sObjectNewList == null ? new sObject[] {} : sObjectNewList;
        
        initializeMap(this.sObjectOldList, sObjectOldListMap);
        initializeMap(this.sObjectNewList, sObjectNewListMap);

    }
    
    public void initializeMap(sObject[] sObjectList, Map<Id, sObject> sObjectListMap) {
        for(sObject sObjectRecord : sObjectList) {
            if (sObjectRecord.Id != null) {
                sObjectListMap.put(sObjectRecord.Id, sObjectRecord);
            }
        }
    }
    
    public abstract Boolean executable(sObject sObjectOld, sObject sObjectNew);
    
    public virtual void execute() {
        
        sObject[] sObjectUpdateableList = new sObject[] {};

        if (Trigger.isDelete == true){
	        for(sObject sObjectOld : sObjectOldList) {
	            sObject sObjectNew = sObjectNewListMap.get(sObjectOld.Id);
	            sObjectNew = sObjectNew == null ? null : sObjectNew;
	            
	            if (executable(sObjectOld, sObjectNew)) {
	                sObjectUpdateableList.add(sObjectOld);
	            }
	        }
        }else{
        	for(sObject sObjectNew : sObjectNewList) {
            	sObject sObjectOld = sObjectOldListMap.get(sObjectNew.Id);
            	sObjectOld = sObjectOld == null ? null : sObjectOld;
            
            	if (executable(sObjectOld, sObjectNew)) {
                	sObjectUpdateableList.add(sObjectNew);
            	}
        	}
        }
        
        
        if (sObjectUpdateableList.size() != 0) {
            execute(sObjectUpdateableList, trigger.isAfter == true);
        }
    }
    
    public virtual void execute(sObject[] sObjectList, Boolean forceUpdate) {}
 
}