@isTest
public class OpportunityRefillTestFactory {
	
	
	public Id standardId;
	
	public Account account;
	
	public Account physician;

	public Account patient;
	
	public Opportunity opportunity;
	
	public Pricebook2 pricebook;
    public Product2 product;
    public PricebookEntry standardPricebookEntry;
    public PricebookEntry pricebookEntry;
	
	public OpportunityLineItem opportunityLineItem;
	
	public DyrctAxess_Settings__c dyrctAxessSettings;
	
	public OpportunityRefillTestFactory(){}
	
	public void createAll(Boolean doInsert){
		this.setPricebookStandard();
        this.createPriceBook();
        this.createDirectAccessSettings(doInsert);
        this.createAccount(doInsert);
        this.createOpportunity(doInsert);
        this.createProduct(doInsert);
        this.createPriceBookEntry(doInsert);
        this.createOpportunityLineItem(doInsert);
	}
	
	public DyrctAxess_Settings__c createDirectAccessSettings(Boolean doInsert){
		this.dyrctAxessSettings = new DyrctAxess_Settings__c(
																SetupOwnerId = UserInfo.getOrganizationId(),
																Notification_Email_Days__c = 10,
																Enable_Process__c = true,
																Interval__c = 3,
																Apex_Job__c = '08e4000000FMvuo',
																Portal_Base_URL__c = 'https://dyrctaxess.herokuapp.com',
																Application_Id__c = 'D831f505f-9bd7-4d96-a123-2e4c4a1e70e3',
																Health_Vault_Endpoint__c = 'https://platform.healthvault.com/platform/wildcat.ashx'
																);
		if (doInsert == true){													
			insert this.dyrctAxessSettings;
		}
		return this.dyrctAxessSettings;								
		
	}
	
	public void setPricebookStandard(){   		
            this.standardId = Test.getStandardPricebookId();
    }
    
    public void CreatePriceBook(){
    	this.pricebook = new Pricebook2(Name='VAYA', isActive=true );
    	insert pricebook;
    }
	
	public Account createAccount(Boolean doInsert){
		this.account = new Account(
									Name = '100'
								   );
		
		if (doInsert == true){
			insert this.account;
		}
		return this.account;
	}
	
	public Opportunity createOpportunity(Boolean doInsert){
		this.opportunity = new Opportunity(
											Name = 'test Opportunity',
											Type = 'New Prescription',
											StageName = 'New',
											CloseDate = Date.today().addDays(-30),
											Status__c = 'new',
											Prescription_Shipped_Date__c = Date.today().addDays(-30)
											);
		if (doInsert == true){
			insert this.opportunity;
		}
		return this.opportunity;
	}
	
	 public Product2 createProduct(Boolean doInsert){
            this.product = new Product2(
                        Name = 'Test Product',
                        isActive = true
             );
                                                                    
            if (doInsert == true){
                    insert this.product;
            }                                                       
            
            return this.product;
    }
    
     public PricebookEntry createPriceBookEntry(Boolean doInsert){
     		this.standardPricebookEntry =  new PricebookEntry(
                        Pricebook2Id = this.standardId,
                        Product2Id = this.product.Id,
                        UnitPrice = 99,
                        isActive = true);
            
            insert standardPricebookEntry;
            
            this.pricebookEntry = new PricebookEntry(
                        Pricebook2Id = this.pricebook.id,
                        Product2Id = this.product.Id,
                        UnitPrice = 99,
                        isActive = true);
                                                                                                    
            if (doInsert == true){
                 insert this.pricebookEntry;
            }                                                                                       
            
            return this.pricebookEntry;
    }
    
    
    //Product, Days, Dosage, Administration, Refills, Unit Price
    
    public OpportunityLineItem createOpportunityLineItem(Boolean doInsert){
    	
            this.opportunityLineItem = new OpportunityLineItem(
											                    PriceBookEntryId = this.standardPricebookEntry.Id,
											                    OpportunityId = this.opportunity.Id,
											                    Quantity = 1,
											                    Refills__c = '3',
                    											Shipping_Datetime__c = Date.today().addDays(-30),
                    											Days__c = '30',
                    											TotalPrice = 100
             													);
                                                                                                                    
            if (doInsert == true){
                    insert this.opportunityLineItem;
            }
            
            return this.opportunityLineItem;
    }
    
	
	

}