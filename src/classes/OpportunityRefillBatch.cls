/*
    To Run:
            Id processId = Database.executeBatch(new OpportunityRefillBatch(), 1);
*/
global class OpportunityRefillBatch implements Database.Batchable<SObject> {
    
    public class ApexBatchJobException extends Exception {}
    
    public static String QUERY_CONSTANT = 'SELECT Id, (SELECT Id FROM Refill_Opportunities__r) FROM Opportunity WHERE Type = \'New Prescription\' AND Status__c != \'Canceled-Pharmacy\' AND Status__c != \'Canceled-Call Center\'';
    private String query;
    
    public OpportunityRefillBatch(){
        this(QUERY_CONSTANT);
    }
    
    public OpportunityRefillBatch(String queryString) {
        
        this.query = queryString;
        
        //Check if the job is running already
        ApexClass ac = [SELECT Name FROM ApexClass WHERE Name = 'OpportunityRefillBatch'];
        
        List<AsyncApexJob> aajs = [SELECT Status FROM AsyncApexJob WHERE ApexClassId = :ac.Id AND 
                                    JobType = 'batchApex' AND Status in ('Queued', 'Processing')];
                                    
        if (!aajs.isEmpty()) {
            throw new ApexBatchJobException(aajs.size() + ' OpportunityRefillBatch job(s) already running or processing.');
        }
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        Database.QueryLocator qLocator = null;
        try {
            qLocator = Database.getQueryLocator(this.query);
        } catch (Exception e) {
            throw e;
        } 
        return qLocator;
    }
     
    global void execute(Database.BatchableContext ctx, List<SObject> sobjects){
        this.execute(sobjects, Date.today());
    }
    
    global void execute(List<SObject> sobjects, Date dateToday){
        
        Set<Id> opportunityIds = new Set<Id>();
        
        Set<String> opportunityOrderNumberSet = new Set<String>();
        
        List<OpportunityRefill> opportunityRefillList = new List<OpportunityRefill>();
        
        List<Opportunity> opptyInsertList = new List<Opportunity>();
        List<OpportunityLineItem> opptyLineInsertList = new List<OpportunityLineItem>();
        
        DyrctAxess_Settings__c dyrctAxessSettings = DyrctAxess_Settings__c.getOrgDefaults();
        
        for (Opportunity oppty :(List<Opportunity>)sobjects){
            opportunityIds.add(oppty.Id);
            
            for (Opportunity opptyRefill: oppty.Refill_Opportunities__r){
                opportunityIds.add(opptyRefill.Id);
            }
        }
        opportunityIds.remove(null);
        
        
        Map<Id,Opportunity> opptyMap = new Map<Id,Opportunity>([SELECT Id, 
                                                                       Name,
                                                                       Type,
                                                                       PriceBook2Id,
                                                                       RecordTypeId,
                                                                       AccountId,
                                                                       CloseDate,
                                                                       Status__c,
                                                                       StageName,
                                                                       Business_Unit__c,
                                                                       Prescription_Shipped_Date__c,
                                                                       (SELECT Id,
                                                                               Name,
                                                                               Quantity,
                                                                               UnitPrice,                              
                                                                               Product2Id,
                                                                               PriceBookEntryId,
                                                                               Administration__c,
                                                                               Total_Refills__c,
                                                                               Refills__c,
                                                                               Product_Unit__c,
                                                                               Days__c,
                                                                               Product_Dose__c,
                                                                               Dosage__c,
                                                                               Supply_Refill_Processing_Date__c
                                                                        FROM OpportunityLineItems
                                                                        WHERE Refills__c != null
                                                                        AND Product2Id != null
                                                                        AND Days__c != null
                                                                        ORDER BY CreatedDate),
                                                                        (SELECT Id, 
                                                                                Name,
                                                                                Type,
                                                                                Refill_Number__c,
                                                                                Prescription_Shipped_Date__c
                                                                        FROM Refill_Opportunities__r
                                                                        ORDER BY CreatedDate)  
                                                                   FROM Opportunity 
                                                                   WHERE Id IN :opportunityIds]);
                                                                   
            
        for (Opportunity oppty : opptyMap.values()){
            
            if (oppty.Type != 'New Prescription')
                continue;
                
            if (oppty.OpportunityLineItems.isEmpty() == true)
                continue;
            
            Integer refillNumber = 1;
            
            for (Opportunity ref : oppty.Refill_Opportunities__r){
                opportunityOrderNumberSet.add(oppty.Id +''+ ref.Refill_Number__c);
            }
            
            if (oppty.Refill_Opportunities__r.isEmpty() == true){
                
                if (oppty.Prescription_Shipped_Date__c == null)
                        continue;
                
                
                OpportunityRefill oppRefill = new OpportunityRefill(oppty, refillNumber, dyrctAxessSettings);
                oppRefill.cloneOpportunity(oppty);
                oppRefill.cloneLines(oppty.OpportunityLineItems, dateToday, oppty.Prescription_Shipped_Date__c);
                
                
                if (oppRefill.opptyLines.isEmpty() == true)
                    continue;
                
                opportunityRefillList.add(oppRefill);
                
            }else{
                
                for (Opportunity opptyRefill : oppty.Refill_Opportunities__r){
                    
                    refillNumber++;
                    
                    //skipp if already created
                    if (opportunityOrderNumberSet.contains(oppty.Id +''+ refillNumber) == true)
                        continue;
                        
                    if (opptyRefill.Prescription_Shipped_Date__c == null)
                        continue;
                
                    OpportunityRefill oppR = new OpportunityRefill(oppty, refillNumber, dyrctAxessSettings);
                    oppR.cloneOpportunity(oppty);
                    oppR.cloneLines(oppty.OpportunityLineItems, dateToday, opptyRefill.Prescription_Shipped_Date__c);
                    
                    if (oppR.opptyLines.isEmpty() == true)
                        continue;
                    
                    opportunityRefillList.add(oppR);
                }
            }
        }
        
        
        for (OpportunityRefill refillWrapper : opportunityRefillList){
            opptyInsertList.add(refillWrapper.oppty);
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            insert opptyInsertList;
        }catch(Exception e){
            Database.rollback(sp);
            system.debug(e);
            return;
        }
        
        
        for (OpportunityRefill refillWrapper : opportunityRefillList){
            refillWrapper.addOppId();
            opptyLineInsertList.addAll(refillWrapper.opptyLines);
        }
        
        try {
            insert opptyLineInsertList;
        }catch(Exception e){
            Database.rollback(sp);
            system.debug(e);
            return;
        }
        
    }
    
    
    global void finish(Database.BatchableContext ctx) {}

}