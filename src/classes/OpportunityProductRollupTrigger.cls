public class OpportunityProductRollupTrigger extends SObjectTrigger{

	public OpportunityProductRollupTrigger(sObject[] sObjectOldList, sObject[] sObjectNewList){
		super(sObjectOldList,sObjectNewList);
	}
    
    public override boolean executable(sObject sObjectOld, sObject sObjectNew){
		return true;
	}

	public override void execute(sObject[] sObjectList, Boolean forceUpdate){
		
		List<Opportunity> opptyUpdateList = new List<Opportunity>();
		Set<Id> opportunityIds = new Set<Id>();
		
		for (SObject obj : sObjectList){
			
			opportunityIds.add((Id)obj.get(''+OpportunityLineItem.OpportunityId));
		}
		opportunityIds.remove(null);
		
		
		system.debug('opportunityIds -> '+ opportunityIds);
		
		
		List<Opportunity> opptyList= [SELECT Id,
											 Product_Name_s__c,
											 (SELECT Id,
											 		 Product2.Name
											  FROM OpportunityLineItems)
									  FROM Opportunity
									  WHERE Id IN :opportunityIds];
									  
		
		for (Opportunity oppty : opptyList){
			
			String productNames = '';
			for (OpportunityLineItem opptyline : oppty.OpportunityLineItems){
				productNames += opptyLine.Product2.Name + ', ';
			}
			
			if (productNames != ''){
				productNames = productNames.subString(0, productNames.length() - 2);
			}
			if (productNames.length() > 255){
				productNames = productNames.subString(0, 255);
			}
			
			if (oppty.Product_Name_s__c == productNames)
				continue;
			
			oppty.Product_Name_s__c = productNames;
			opptyUpdateList.add(oppty);
		}
		
		if (opptyUpdateList.isEmpty() == false){
			update opptyUpdateList;
		}
	}
}