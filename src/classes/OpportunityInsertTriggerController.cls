public with sharing class OpportunityInsertTriggerController {

    public static void createOpportunityCases(Opportunity[] opportunities) {
    
	    Id[] accountIds = new Id[] {};
	    
	    for (Opportunity opportunity :opportunities) {
	        accountIds.add(opportunity.AccountId);
	    }
	    
	    Map<Id, Account> accounts = getAccountsMap(accountIds);
	    
	    CaseController caseController = new CaseController();
	    
	    Case[] cases = new Case[] {};
	    for (Opportunity opportunity :opportunities) {
	
	        Account account = accounts.get(opportunity.AccountId);
	        
	        if (!account.Is_Information_Complete__c) {
	           cases.add(caseController.createCase(account, opportunity.Id));
	        }
	    }    
	    
        insertCases(cases);
    }
    
    private static void insertCases(Case[] cases) {
    
        if (!cases.isEmpty()) {
            insert cases;
        }
    }
    
    private static Map<Id, Account> getAccountsMap(Id[] accountIds) {
        
        return new Map<Id, Account> ([SELECT Id, PersonContactId, Is_Information_Complete__c FROM Account WHERE Id IN :accountIds]);       
    }


}