@RestResource(urlMapping = '/portal/patientRegistration/*')
global class PortalPatientRegistrationRestService {
	
	
	public static Restrequest req = null;

    @httpGet
    global static PortalPatientRegistrationRestServiceData doGet() {
    
        req = RestContext.request;
        
        String response = PortalPatientRestServiceController.getInstance().createPatientRegistration(req.params);
        
        return new PortalPatientRegistrationRestServiceData(response);
    }
    

    global class PortalPatientRegistrationRestServiceData {
        
        public String id;
       
        public PortalPatientRegistrationRestServiceData(String id) {
           this.id = id == null ? '' : id;
        }
    }
	

}