/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public abstract class HVWebService {

    protected Id accountId;
    
    public HVWebService(Id accountId) {
        this.accountId = accountId;
    }
    
    public abstract String execute();
    
}