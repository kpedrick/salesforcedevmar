/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
public with sharing class HVNewApplicationCreationInfo extends HVRequest {
	
	public HVNewApplicationCreationInfo() {
		
        this.header = new HVRequestHeader('NewApplicationCreationInfo','1');
        this.info = new HVRequestInfo(); 
	}
	
}