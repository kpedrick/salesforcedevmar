/*
Name              : OpportunityAddProductsControllerTest
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by              Reference             
----------------------------------------------------------------------------------------
1. Fernando Rodriguez       08/07/2014          Peter Clifford            https://na5.salesforce.com/a0P7000000CzZM8EAN
*/
@isTest(seeAllData = true)
public with sharing class OpportunityAddProductsControllerTest {

  private static Account patient = null;
  private static Opportunity opportunity = null;

  static testMethod void test() {
  
    opportunity = [SELECT Id, Pricebook2Id, (SELECT Id, PricebookEntryId FROM OpportunityLineItems) 
                    FROM Opportunity 
                    WHERE Pricebook2Id != null 
                    LIMIT 1];
    
    Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(opportunity);
    OpportunityAddProductsController ctrl = null;
    PricebookEntry entry = [SELECT Id 
                              FROM PricebookEntry 
                              WHERE Pricebook2Id = :opportunity.Pricebook2Id 
                              LIMIT 1];
    
    Test.startTest();
    
    ctrl = new OpportunityAddProductsController(std);
    ctrl.saveItems();
    
    ctrl.searchBox = 'Aceon';
    ctrl.searchProducts();
    
    if (!opportunity.OpportunityLineItems.isEmpty()) {
    
	    Id entryId = opportunity.OpportunityLineItems[0].PricebookEntryId;
	    
	    ctrl.itemId = opportunity.OpportunityLineItems[0].Id;
	    ctrl.removeItem();
	    
	    ctrl.entryId = entryId;
	    ctrl.addItem();
    }
    Test.stopTest();     
    
    //System.assert(ctrl.items.size() > 0);
  }

}