/**************************************************
Author: Fernando Rodriguez <frodriguez@ioniasolutions.com>

Date: 02/04/2014
**************************************************/
@RestResource(urlMapping = '/portal/password/*')
global with sharing class PortalPasswordRestService {

    public static Restrequest req = null;

    @httpGet
    global static PortalPasswordRestServiceData doGet() {
    
        req = RestContext.request;
        String request = req.requestURI;
        
        PortalPasswordRestServiceData result = new PortalPasswordRestServiceData();
        
        if (request.contains('physician')) {
        
            String password = PortalPasswordRestServiceController.getInstance().changePhysicianPassword(req.params.get('username'));
            result.setPassword(password);
        }
        else {
            String password = PortalPasswordRestServiceController.getInstance().changePatientPassword(req.params.get('username'));
            result.setPassword(password);
        }
        
        return result;
    }

    global class PortalPasswordRestServiceData {
        
        public Boolean status = false;
        public String password;
        
        public void setPassword(String password) {
        
            this.password = password;
            if (password != null) {
                status = true;
            }
            
        
        }
        
    }

}