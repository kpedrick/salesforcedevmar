<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Support_Authorization_Received</fullName>
        <description>Support Authorization Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quotes_and_Invoices/QI_Support_Auth_Received</template>
    </alerts>
    <rules>
        <fullName>Support Authorization Received</fullName>
        <actions>
            <name>Support_Authorization_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Auth_Payment_Processed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification of support (Case) authorization from customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
