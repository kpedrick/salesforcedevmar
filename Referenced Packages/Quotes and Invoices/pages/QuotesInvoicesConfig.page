<apex:page controller="quot.QuotesInvoicesConfigController" tabstyle="pymt__Settings__c" title="Quotes and Invoices Configuration">
<apex:sectionHeader title="Linvio PaymentConnect" subtitle="Quotes and Invoices" help="{!URLFOR($Page.quot__Help)}" />
<apex:form id="mainForm">
<apex:pageMessages id="messages" />
<apex:pageBlock title="Quotes and Invoices Add-On Settings" mode="edit">
<apex:pageBlockSection columns="2">

<apex:pageBlockSectionItem helpText="Select the Sites website or Community portal that will be used to collect online payments for quotes and invoices.">
<apex:outputText value="Site/Community"/>
<apex:selectList size="1" value="{!settings.quot__Sites_Website__c}">
<apex:selectOptions value="{!SiteNameOptions}"/> 
</apex:selectList>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem />

<apex:inputField value="{!settings.quot__Force_com_Domain__c}" style="width:200px;"/>
<apex:pageBlockSectionItem />

<apex:inputField value="{!settings.quot__SiteQuoteApproval_Page__c}" style="width:150px;"/>
<apex:inputField value="{!settings.quot__PtlQuoteApproval_Page__c}" style="width:150px;"/>

<apex:inputField value="{!settings.quot__SiteInvoice_Page__c}"  style="width:150px;"/>
<apex:inputField value="{!settings.quot__PtlInvoice_Page__c}"  style="width:150px;"/>

<apex:inputField value="{!settings.quot__SiteSupportAuth_Page__c}"  style="width:150px;"/>
<apex:inputField value="{!settings.quot__PtlSupportAuth_Page__c}"  style="width:150px;"/>

<apex:inputField value="{!settings.quot__Support_Auth_I_Agree__c}"/>
<apex:pageBlockSectionItem />

<apex:inputField value="{!settings.quot__HideQuoteShippingAddr__c}"/>
<apex:pageBlockSectionItem helpText="Switches to the standard Salesforce rich text editor for terms and conditions on Opportunities">
<apex:outputText value="Disable HTML Editor on Opps"/>
<apex:inputField value="{!settings.quot__SF_Rich_Text_on_Opps__c}"/>
</apex:pageBlockSectionItem>

<apex:inputField value="{!settings.quot__HideInvoiceShippingAddr__c}"/>
<apex:pageBlockSectionItem helpText="Switches to the standard Salesforce rich text editor for SOWs on Cases">
<apex:outputText value="Disable HTML Editor on Cases"/>
<apex:inputField value="{!settings.quot__SF_Rich_Text_on_Cases__c}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputText value=""/>
<apex:commandButton value="Save Settings" action="{!saveSettings}"/>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem />

</apex:pageBlockSection>
</apex:pageBlock>

<apex:pageBlock title="Batch Update Support Auth Fields" mode="edit">
<p style="padding-left:20px;padding-right:20px;">Schedule or run this BatchUpdateSupportAuthFields script to search for payments or authorizations on support authorization requests (Cases).  When a payment or authorization is found that matches a published support authorization request for a Case, the script will set a flag on the case indicating that the customer has approved the request, and "unpublish" the request.</p>
<apex:pageBlockSection >
<apex:pageBlockSectionItem >
<apex:outputLabel value="Current Status"/>
<apex:outputText value="{!apexJob.Status}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputLabel value="Extended Status"/>
<apex:outputText value="{!apexJob.ExtendedStatus}"/>
</apex:pageBlockSectionItem>


<apex:pageBlockSectionItem >
<apex:outputLabel value="Total Job Items"/>
<apex:outputText value="{!apexJob.TotalJobItems}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputLabel value="Job Items Processed"/>
<apex:outputText value="{!apexJob.JobItemsProcessed}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputLabel value="Number of Errors"/>
<apex:outputText value="{!apexJob.NumberOfErrors}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputLabel value="Completed Date"/>
<apex:outputField value="{!apexJob.CompletedDate}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem rendered="{!NOT(isScheduled)}">
<apex:outputLabel value="Schedule batch job to run:"/>
<apex:selectList size="1" multiselect="false" value="{!selectedFreqHour}">
<apex:selectOption itemValue="*" itemLabel="Hourly"/>
<apex:selectOption itemValue="0/2" itemLabel="Every 2 Hours"/>
<apex:selectOption itemValue="0/4" itemLabel="Every 4 Hours"/>
<apex:selectOption itemValue="0/6" itemLabel="Every 6 Hours"/>
<apex:selectOption itemValue="0/12" itemLabel="Every 12 Hours"/>
<apex:selectOption itemValue="0" itemLabel="0:00"/>
<apex:selectOption itemValue="1" itemLabel="1:00"/>
<apex:selectOption itemValue="2" itemLabel="2:00"/> 
<apex:selectOption itemValue="3" itemLabel="3:00"/>
<apex:selectOption itemValue="4" itemLabel="4:00"/>
<apex:selectOption itemValue="5" itemLabel="5:00"/>
<apex:selectOption itemValue="6" itemLabel="6:00"/>
<apex:selectOption itemValue="7" itemLabel="7:00"/>
<apex:selectOption itemValue="8" itemLabel="8:00"/>
<apex:selectOption itemValue="9" itemLabel="9:00"/>
<apex:selectOption itemValue="10" itemLabel="10:00"/>
<apex:selectOption itemValue="11" itemLabel="11:00"/>
<apex:selectOption itemValue="12" itemLabel="12:00"/>
<apex:selectOption itemValue="13" itemLabel="13:00"/>
<apex:selectOption itemValue="14" itemLabel="14:00"/>
<apex:selectOption itemValue="14" itemLabel="14:00"/>
<apex:selectOption itemValue="15" itemLabel="15:00"/>
<apex:selectOption itemValue="16" itemLabel="16:00"/>
<apex:selectOption itemValue="17" itemLabel="17:00"/>
<apex:selectOption itemValue="18" itemLabel="18:00"/>
<apex:selectOption itemValue="19" itemLabel="19:00"/>
<apex:selectOption itemValue="20" itemLabel="20:00"/>
<apex:selectOption itemValue="21" itemLabel="21:00"/>
<apex:selectOption itemValue="22" itemLabel="22:00"/>
<apex:selectOption itemValue="23" itemLabel="23:00"/>
</apex:selectList>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem rendered="{!NOT(isScheduled)}">
<apex:outputLabel value="Day of the week:"/>
<apex:selectList size="1" multiselect="false" value="{!selectedFreqDay}">
<apex:selectOption itemValue="E" itemLabel="Every Day"/>
<apex:selectOption itemValue="1" itemLabel="Sun"/>
<apex:selectOption itemValue="2" itemLabel="Mon"/>
<apex:selectOption itemValue="3" itemLabel="Tue"/>
<apex:selectOption itemValue="4" itemLabel="Wed"/>
<apex:selectOption itemValue="5" itemLabel="Thu"/>
<apex:selectOption itemValue="6" itemLabel="Fri"/>
<apex:selectOption itemValue="7" itemLabel="Sat"/>
</apex:selectList>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem rendered="{!(isScheduled)}">
<apex:outputLabel value="Scheduled Batch Job Status:" />
<apex:outputText value="Scheduled"/>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem rendered="{!(isScheduled)}">
<apex:outputLabel value="Next Scheduled Run:" />
<apex:outputText value="{!job.nextFireTime}"/>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
<apex:outputText value=""/>
<apex:outputText value=""/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputText value=""/>
<apex:outputText value=""/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputText value=""/>
<apex:outputPanel >
<apex:commandButton value="Run Batch Script Once" action="{!runScriptOnce}"/>
<apex:commandButton value="Schedule Batch Job" action="{!scheduleBatchJob}" rendered="{!NOT(isScheduled)}"/>
<apex:commandButton value="Cancel Scheduled Batch Job" action="{!cancelBatchJob}" rendered="{!(isScheduled)}"/>
</apex:outputPanel>
</apex:pageBlockSectionItem>

</apex:pageBlockSection>

</apex:pageBlock>
</apex:form>
</apex:page>