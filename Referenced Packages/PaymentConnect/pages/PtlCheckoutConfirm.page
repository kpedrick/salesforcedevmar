<apex:page id="ptlCheckoutConfirm" controller="pymt.SiteCheckoutConfirmController" sidebar="false" tabstyle="pymt__PaymentX__c" cache="false" >
<link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.pymt__PaymentConnect,'styles/ptl_basic.css')}" />
<head>
<title>Checkout Complete</title>
</head>
 
  <div class="pc_page_content_top"></div>
  <div class="pc_page_content"  id="pc_ptlscheduledpayments_page">
    <div class="pc_page_content_inner"> 

    <apex:form id="checkoutConfirmForm"  >
        <apex:outputPanel id="checkoutConfirmFormPanel" >

            <apex:pageMessages id="messages"/>
            <apex:outputPanel rendered="{!showPageContents}"> 
                <apex:sectionHeader subtitle="{!IF(payment.pymt__Status__c == 'Authorized',$Label.SiteCheckout_AuthCompletedHeading,$Label.SiteCheckout_ChargeCompletedHeading)}"/>

                <apex:outputPanel rendered="{!NOT(payment.pymt__Status__c == 'Authorized')}">
                    <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_ChargeCompletedMessage == '-')}">
                    <p>{!$Label.SiteCheckout_ChargeCompletedMessage}</p> 
                    </apex:outputPanel>
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!(payment.pymt__Status__c == 'Authorized')}">
                    <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_AuthCompletedMessage == '-')}">
                    <p>{!$Label.SiteCheckout_AuthCompletedMessage}</p> 
                    </apex:outputPanel>
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_ReceiptDetailsHeading == '-')}">            
                <h2>{!$Label.SiteCheckout_ReceiptDetailsHeading}</h2>
                </apex:outputPanel>

                <apex:outputText value="{!$Label.Label_Date}"/>:&nbsp;<apex:outputField value="{!payment.pymt__Date__c}"/>

                <apex:outputPanel rendered="{!hasCartItems}" >
                    <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_ItemListHeading == '-')}">            
                    <h3>{!$Label.SiteCheckout_ItemListHeading}</h3>
                    </apex:outputPanel>
                    <apex:dataTable styleClass="pc_cart_item_table"  id="cartItemTable2" value="{!attachedCartItems}" var="item" columnClasses="itemDescriptionColumn,itemQuantityColumn,currency_column,currency_column" rowClasses="evenRow,oddRow">
                        <apex:column headerClass="itemDescriptionColumn">
                            <apex:facet name="header">{!$Label.Label_ItemName}</apex:facet>
                            <apex:outputText value="{!item.name}" />&nbsp;<apex:outputText styleClass="product_code" value="({!item.pymt__Product_Code__c})" rendered="{!NOT(ISNULL(item.pymt__Product_Code__c))}" />
                        </apex:column>
                        <apex:column headerClass="itemQuantityColumn">
                            <apex:facet name="header">{!$Label.Label_Quantity}</apex:facet>
                            <apex:outputText value="{0,number,0.00}" >
                                <apex:param value="{!item.pymt__Quantity__c}" />
                            </apex:outputText>
                        </apex:column>
                        <apex:column headerClass="currency_column" >
                            <apex:facet name="header">{!$Label.Label_UnitPrice}</apex:facet>
                            <apex:outputText value="{!currencyShortFormatExpression}">
                            <apex:param value="{!item.pymt__Unit_Price__c}" />
                            </apex:outputText>
                        </apex:column>
                        <apex:column headerClass="currency_column">
                            <apex:facet name="header">{!$Label.Label_TotalAmount}</apex:facet>
                            <apex:outputText value="{!currencyShortFormatExpression}">
                            <apex:param value="{!item.pymt__Total__c}" />
                            </apex:outputText>
                        </apex:column>
                    </apex:dataTable>
                </apex:outputPanel>
            
                <apex:outputPanel rendered="{!NOT(hasCartItems)}" >
                    <table class="pc_cart_item_table">
                    <tr>
                        <th class="itemDescriptionColumn">{!$Label.Label_Description}</th>
                        <th class="currency_column">{!$Label.Label_Amount}</th>
                    </tr>
                    <tr>
                        <td class="itemDescriptionColumn"><apex:outputText value="{!payment.name}" /></td>
                        <td class="currency_column"><apex:outputText value="{!currencyFormatExpression}"><apex:param value="{!subtotal}"/></apex:outputText></td>            
                    </tr>
                    </table>
                </apex:outputPanel>
            
                <div class="pc_checkout_totals" >
            <apex:panelGrid columns="2" columnClasses="totalsLabelColumn,currency_column">
                <apex:outputLabel value="{!$Label.Label_DiscountAmount}" rendered="{!NOT(OR(ISNULL(payment.pymt__Discount__c),payment.pymt__Discount__c==0))}"/>
                <apex:outputText styleClass="discountAmount" rendered="{!NOT(OR(ISNULL(payment.pymt__Discount__c),payment.pymt__Discount__c==0))}"
                    value="{!currencyFormatExpression}" >
                   <apex:param value="{!payment.pymt__Discount__c}"/>
                </apex:outputText> 
                
                <apex:outputLabel value="{!$Label.Label_TaxAmount}" rendered="{!NOT(OR(ISNULL(payment.pymt__Tax__c),payment.pymt__Tax__c==0))}"/>
                <apex:outputText styleClass="taxAmount" rendered="{!NOT(OR(ISNULL(payment.pymt__Tax__c),payment.pymt__Tax__c==0))}"
                    value="{!currencyFormatExpression}" >
                   <apex:param value="{!payment.pymt__Tax__c}"/>
                </apex:outputText> 
                
                <apex:outputLabel value="{!$Label.Label_ShippingAmount}" rendered="{!NOT(OR(ISNULL(payment.pymt__Shipping__c),payment.pymt__Shipping__c==0))}"/>
                <apex:outputText styleClass="shippingAmount" rendered="{!NOT(OR(ISNULL(payment.pymt__Shipping__c),payment.pymt__Shipping__c==0))}"
                    value="{!currencyFormatExpression}" >
                   <apex:param value="{!payment.pymt__Shipping__c}"/>
                </apex:outputText> 
                
                <apex:outputLabel value="{!$Label.Label_TotalAmount}"/>
                <apex:outputText styleClass="totalAmount" 
                    value="{!currencyFormatExpression}" >
                   <apex:param value="{!payment.pymt__Amount__c}"/>
                </apex:outputText> 
                                
                
            </apex:panelGrid>
    
                </div>
            
                <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_ReceivedFromHeading == '-')}">
                <h2>{!$Label.SiteCheckout_ReceivedFromHeading}</h2>
                </apex:outputPanel>
                <p>
                {!payment.Billing_First_Name__c}&nbsp;{!payment.Billing_Last_Name__c}<br/>
                {!payment.Billing_Street__c}<br/>
                {!payment.Billing_City__c}, {!payment.Billing_State__c}&nbsp;{!payment.Billing_Postal_Code__c}<br/>
                {!payment.Billing_Country__c}<br/></p>
                <apex:outputPanel rendered="{!NOT($Label.SiteCheckout_PaidToHeading == '-')}">
                <h2>{!$Label.SiteCheckout_PaidToHeading}</h2>
                </apex:outputPanel>
                <p>
                {!$Organization.Name}<br/>
                {!$Organization.Street}<br/>
                {!$Organization.City}, {!$Organization.State}&nbsp;{!$Organization.PostalCode}<br/>
                {!$Organization.Country}<br/>
                {!$Organization.Phone}<br/></p>
                <br/>
                <apex:commandButton id="finish" value="{!$Label.SiteCheckout_FinishedButton}" onclick="" action="{!finishTransaction}" rendered="{!finishURL <> null}"/> 
 
            </apex:outputPanel>
        </apex:outputPanel>
    </apex:form>
    </div>
    </div>
</apex:page>